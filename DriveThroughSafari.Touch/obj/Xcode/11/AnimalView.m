// WARNING
// This file has been generated automatically by Xamarin Studio to
// mirror C# types. Changes in this file made by drag-connecting
// from the UI designer will be synchronized back to C#, but
// more complex manual changes may not transfer correctly.


#import "AnimalView.h"

@implementation AnimalView

@synthesize btnShowMarkers = _btnShowMarkers;
@synthesize lblDescription = _lblDescription;
@synthesize lblFact = _lblFact;
@synthesize lblName = _lblName;
@synthesize lblOrigin = _lblOrigin;
@synthesize mainImage = _mainImage;

@end
