// WARNING
// This file has been generated automatically by Xamarin Studio to
// mirror C# types. Changes in this file made by drag-connecting
// from the UI designer will be synchronized back to C#, but
// more complex manual changes may not transfer correctly.


#import <Foundation/Foundation.h>


@interface AnimalView : UIViewController {
	UIButton *_btnShowMarkers;
	UILabel *_lblDescription;
	UILabel *_lblFact;
	UILabel *_lblName;
	UILabel *_lblOrigin;
	UIImageView *_mainImage;
}

@property (nonatomic, retain) IBOutlet UIButton *btnShowMarkers;

@property (nonatomic, retain) IBOutlet UILabel *lblDescription;

@property (nonatomic, retain) IBOutlet UILabel *lblFact;

@property (nonatomic, retain) IBOutlet UILabel *lblName;

@property (nonatomic, retain) IBOutlet UILabel *lblOrigin;

@property (nonatomic, retain) IBOutlet UIImageView *mainImage;

@end
