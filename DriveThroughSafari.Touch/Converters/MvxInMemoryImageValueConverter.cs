﻿using System;
using Cirrious.CrossCore.Converters;
using MonoTouch.UIKit;
using MonoTouch.Foundation;

namespace DriveThroughSafari.Touch
{
	public class MvxInMemoryImageValueConverter : MvxValueConverter<byte[], UIImage>
	{
		protected override UIImage Convert(byte[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			if (value == null)
				return null;

			var imageData = NSData.FromArray(value);
			return UIImage.LoadFromData(imageData);
		}
	}
}