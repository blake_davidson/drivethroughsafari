using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using CrossUI.Touch.Dialog.Elements;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace DriveThroughSafari.Touch.Elements
{
    public class TransparentMultilineElement : StyledMultilineElement
    {
        public TransparentMultilineElement()
        {
           
        }

        protected override UITableViewCell GetCellImpl(MonoTouch.UIKit.UITableView tv)
        {
            var cell = base.GetCellImpl(tv);

            cell.SelectionStyle = UITableViewCellSelectionStyle.None;
            cell.BackgroundView = new UIView(RectangleF.Empty);

            return cell;
        }
    }
}