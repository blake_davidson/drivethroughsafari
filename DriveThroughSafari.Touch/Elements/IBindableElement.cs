using Cirrious.MvvmCross.Binding.BindingContext;

namespace DriveThroughSafari.Touch.Elements
{
    public interface IBindableElement
        : IMvxBindingContextOwner
    {
        object DataContext { get; set; }
    }
}