﻿using System;
using CrossUI.Touch.Dialog.Elements;
using System.Drawing;
using MonoTouch.UIKit;

namespace DriveThroughSafari.Touch
{
    public class SafariStyledStringElement : StyledStringElement
    {

        public SizeF ShadowOffset { get; set; }

        public UIColor ShadowColor { get; set; }

        public SafariStyledStringElement(string text)
            : base(text)
        {
        }

        protected override MonoTouch.UIKit.UITableViewCell GetCellImpl(MonoTouch.UIKit.UITableView tv)
        {
            var cell = base.GetCellImpl(tv);
            cell.TextLabel.ShadowOffset = ShadowOffset;
            cell.TextLabel.ShadowColor = ShadowColor;

            return cell;
        }
    }
}

