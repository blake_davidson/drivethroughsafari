using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cirrious.MvvmCross.Binding.BindingContext;
using CrossUI.Touch.Dialog.Elements;
using DriveThroughSafari.Core.ViewModels;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using DriveThroughSafari.Core;
using DriveThroughSafari.Core.Models;

namespace DriveThroughSafari.Touch.Elements
{
    public class AnimalElement : StyledStringElement, IBindableElement, IElementSizing
    {
        public IMvxBindingContext BindingContext { get; set; }

        public AnimalElement()
        {
            ShouldDeselectAfterTouch = true;
            Accessory = UITableViewCellAccessory.DisclosureIndicator;         
            this.CreateBindingContext();
            this.DelayBind(() =>
            {
					var set = this.CreateBindingSet<AnimalElement, Animal>();
                set.Bind().For(me => me.Caption).To(p => p.Name);
                set.Apply();
            });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                BindingContext.ClearAllBindings();
            }
            base.Dispose(disposing);
        }

        public virtual object DataContext
        {
            get { return BindingContext.DataContext; }
            set { BindingContext.DataContext = value; }
        }

        public float GetHeight(UITableView tableView, MonoTouch.Foundation.NSIndexPath indexPath)
        {
            return 75;
        }
    }
}