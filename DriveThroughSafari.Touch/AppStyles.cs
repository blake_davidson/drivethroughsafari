﻿using System;
using MonoTouch.UIKit;
using System.Drawing;

namespace DriveThroughSafari.Touch
{
    public static class AppStyles
    {
        public static UIFont Langdon = UIFont.FromName("Langdon", 30);
        public static UIFont Fabrica = UIFont.FromName("Fabrica", 20);
        public static string BackButtonPath = "Images/Navigation/Back";
        public static string ZebraBackground = "Images/zebrabckgrd";

        public static UIColor TintColor = UIColor.FromRGB(172, 7, 7);


        public static bool IsTallScreen()
        {
            return UIScreen.MainScreen.Bounds.Height > 321f;
        }

        public static UIImage GetImage(string imageName)
        {
            return IsTallScreen() ? UIImage.FromFile(imageName + "-568h@2x.png") : UIImage.FromBundle(imageName);
        }
           
    }
}

