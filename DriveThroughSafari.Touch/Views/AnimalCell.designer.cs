// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;
using Cirrious.MvvmCross.Binding.Touch.Views;
using MonoTouch.UIKit;

namespace DriveThroughSafari.Touch
{
    [Register("AnimalCell")]
    partial class AnimalCell
    {
        [Outlet]
        UIImageView imgMain { get; set; }

        [Outlet]
        MonoTouch.UIKit.UILabel lblName { get; set; }

        void ReleaseDesignerOutlets()
        {
            if (imgMain != null)
            {
                imgMain.Dispose();
                imgMain = null;
            }

            if (lblName != null)
            {
                lblName.Dispose();
                lblName = null;
            }
        }
    }
}
