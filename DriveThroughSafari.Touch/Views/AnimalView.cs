﻿using System;
using Cirrious.MvvmCross.Touch.Views;
using MonoTouch.UIKit;
using System.Drawing;
using Cirrious.MvvmCross.Binding.BindingContext;
using DriveThroughSafari.Core.ViewModels;
using MonoTouch.Foundation;
using System.Collections.Generic;
using MonoTouch.CoreImage;
using SDWebImage;

using Monotouch.GPUImage;

namespace DriveThroughSafari.Touch
{
    [DisableMenuGesture]
    public class AnimalView : MvxViewController
    {
        public AnimalView()
        {
        }

        public GPUImageView AnimalImageView;
        public GPUImagePicture AnimalImage;
        public UITableView AnimalTable;
        public UIView AnimalOverlayImage;
        public GPUImageiOSBlurFilter Filter;
        public UIImageView FullImageView;
        // public UIImage FullImage;

        public new AnimalViewModel ViewModel
        {
            get { return (AnimalViewModel)base.ViewModel; }
            set { base.ViewModel = value; }
        }

        private UIButton _backButton;

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            View.BackgroundColor = UIColor.FromPatternImage(AppStyles.GetImage(AppStyles.ZebraBackground));
            //View.BackgroundColor = UIColor.FromRGB(245, 245, 245);
            CreateBackButton();
            NavigationItem.LeftBarButtonItem = new UIBarButtonItem(_backButton);

            AnimalImageView = new GPUImageView(new RectangleF(0, NavigationController.NavigationBar.Frame.Size.Height, 320, 213));

            FullImageView = new UIImageView(new RectangleF(0, NavigationController.NavigationBar.Frame.Size.Height, 320, 213));
            AnimalTable = new UITableView(new RectangleF(0, FullImageView.Frame.Height + NavigationController.NavigationBar.Bounds.Height + 10, UIScreen.MainScreen.Bounds.Width, UIScreen.MainScreen.Bounds.Height - FullImageView.Bounds.Height - NavigationController.NavigationBar.Bounds.Height - 30), UITableViewStyle.Plain);

            var img = new UIImageView(FullImageView.Frame);
            img.SetImage(NSUrl.FromString(ViewModel.Animal.ImageUrl));
            AnimalImage = new GPUImagePicture(img.Image.CGImage);

            FullImageView.SetImage(NSUrl.FromString(ViewModel.Animal.ImageUrl));
            FullImageView.Alpha = 0f;

            Filter = new GPUImageiOSBlurFilter();
            Filter.BlurRadiusInPixels = 4.0f;
            Filter.ForceProcessingAtSize(AnimalImageView.Frame.Size);

            AnimalImage.AddTarget(Filter);
            Filter.AddTarget(AnimalImageView);

            AnimalImage.ProcessImage();

            AnimalTable.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            AnimalTable.BackgroundView = null;
            AnimalTable.BackgroundColor = UIColor.FromRGBA(245, 245, 245, .75f);
            AnimalTable.ScrollEnabled = true;
            AnimalTable.ShowsHorizontalScrollIndicator = false;
            AnimalTable.ShowsVerticalScrollIndicator = false;

            var set = this.CreateBindingSet<AnimalView,AnimalViewModel>();
            var source = new AnimalTableSource(this);
            AnimalTable.Source = source;

            CreateAnimalOverlayImage();

            Add(FullImageView);
            Add(AnimalImageView);
            Add(AnimalOverlayImage);
            Add(AnimalTable);

        }

        public void CreateBackButton()
        {
            var image = UIImage.FromBundle(AppStyles.BackButtonPath);
            _backButton = UIButton.FromType(UIButtonType.Custom);
            _backButton.SetBackgroundImage(image, UIControlState.Normal);
            _backButton.Frame = new RectangleF(0, 0, image.Size.Width, image.Size.Height);
            _backButton.TouchUpInside += (sender, e) =>
            ViewModel.GoBack.Execute(null);

        }

        private void CreateAnimalOverlayImage()
        {
            var circleImage = new UIImageView(FullImageView.Bounds);
            circleImage.SetImage(NSUrl.FromString(ViewModel.Animal.ImageUrl));
            circleImage.Bounds = new RectangleF(FullImageView.Bounds.X, FullImageView.Bounds.Top, FullImageView.Bounds.Height, FullImageView.Bounds.Height);
            circleImage.CreateCircleImage();

            AnimalOverlayImage = new UIView(FullImageView.Frame);
            AnimalOverlayImage.BackgroundColor = UIColor.Clear;

            AnimalOverlayImage.AddSubview(circleImage);

        }
    }


    public class AnimalTableSource
    : UITableViewSource
    {
        private UITableView _tableView;
        private AnimalViewModel _viewModel;
        private Dictionary<int,float> _heights;
        private Dictionary<int,AnimalInfoCell> _cells;
        private AnimalView _view;
        private float startingY;

        public AnimalTableSource(AnimalView view)
        {
            _tableView = view.AnimalTable;
            _viewModel = view.ViewModel;
            _view = view;
            _heights = new Dictionary<int, float>();
            _cells = new Dictionary<int, AnimalInfoCell>();
            CreateCell();

            startingY = _tableView.ContentOffset.Y;
        }

        public override void Scrolled(UIScrollView scrollView)
        {
            float y = _tableView.ContentOffset.Y;
            float h = _cells[0].GetHeight() + _cells[1].GetHeight();

            if (y < h)
            {
                var diff = h - y;

                var newAlpha = diff / h;
                _view.AnimalOverlayImage.Alpha = newAlpha;
                _view.AnimalImageView.Alpha = newAlpha;
                _view.FullImageView.Alpha = y / h;

            }
            else
            {
                _view.AnimalOverlayImage.Alpha = 0;
                _view.AnimalImageView.Alpha = 0;
                _view.FullImageView.Alpha = 1.0f;
            }
        }

        public override int NumberOfSections(UITableView tableView)
        {
            return 1;
        }

        void CreateCell()
        {
            for (int i = 0; i < 6; i++)
            {
                switch (i)
                {
                    case 0:
                        var cell0 = new AnimalInfoCell("Name");
                        cell0.Init("NAME", _viewModel.Animal.Name);
                        _heights.Add(0, cell0.GetHeight());
                        _cells.Add(0, cell0);
                        break;

                    case 1:
                        var cell1 = new AnimalInfoCell("Length");
                        cell1.Init("LENGTH", _viewModel.Animal.Length);
                        _heights.Add(1, cell1.GetHeight());
                        _cells.Add(1, cell1);
                        break;

                    case 2:
                        var cell2 = new AnimalInfoCell("Weight");
                        cell2.Init("WEIGHT", _viewModel.Animal.Weight);
                        _heights.Add(2, cell2.GetHeight());
                        _cells.Add(2, cell2);
                        break;

                    case 3:
                        var cell3 = new AnimalInfoCell("Origin");
                        cell3.Init("ORIGIN", _viewModel.Animal.Origin);
                        _heights.Add(3, cell3.GetHeight());
                        _cells.Add(3, cell3);
                        break;

                    case 4:
                        var cell4 = new AnimalInfoCell("Fact");
                        cell4.Init("FACT", _viewModel.Animal.Description);
                        _heights.Add(4, cell4.GetHeight());
                        _cells.Add(4, cell4);
                        break;

                    case 5:
                        if (_viewModel.Animal.Markers != null && _viewModel.Animal.Markers.Count > 0)
                        {
                            var cell5 = new AnimalInfoCell("Map");
                            cell5.Init(_viewModel.Animal.Markers);
                            _heights.Add(5, cell5.GetHeight());
                            _cells.Add(5, cell5);

                        }
                        break;
                }
            }
        }

        public override int RowsInSection(UITableView tableView, int section)
        {
            if (_viewModel.Animal.Markers != null && _viewModel.Animal.Markers.Count > 0)
                return 6;

            return 5;
        }


        public override UITableViewCell GetCell(UITableView tableView, MonoTouch.Foundation.NSIndexPath indexPath)
        {
        
            switch (indexPath.Item)
            {
                case 0:
                    var cell0 = tableView.DequeueReusableCell(_cells[0].ReuseIdentifier);
                    if (cell0 == null)
                        return _cells[0];

                    return cell0;

                case 1:
                    var cell1 = tableView.DequeueReusableCell(_cells[1].ReuseIdentifier);
                    if (cell1 == null)
                        return _cells[1];

                    return cell1;

                case 2:
                    var cell2 = tableView.DequeueReusableCell(_cells[2].ReuseIdentifier);
                    if (cell2 == null)
                        return _cells[2];

                    return cell2;
                case 3:
                    var cell3 = tableView.DequeueReusableCell(_cells[3].ReuseIdentifier);
                    if (cell3 == null)
                        return _cells[3];

                    return cell3;
                case 4:
                    var cell4 = tableView.DequeueReusableCell(_cells[4].ReuseIdentifier);
                    if (cell4 == null)
                        return _cells[4];

                    return cell4;

                case 5:
                    var cell5 = tableView.DequeueReusableCell(_cells[5].ReuseIdentifier);
                    if (cell5 == null)
                        return _cells[5];

                    return cell5;

            }
        
        
            return new UITableViewCell();
        }

        public override float GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            if (_heights.ContainsKey(indexPath.Item))
            {
                return _heights[indexPath.Item];
            }
        
            return 100;
        }
    }
}


