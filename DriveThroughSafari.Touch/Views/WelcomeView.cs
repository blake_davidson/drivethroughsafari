﻿using Cirrious.MvvmCross.Touch.Views;
using MonoTouch.Foundation;
using MonoTouch.MediaPlayer;
using MonoTouch.UIKit;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Touch.Views.Presenters;
using System.Drawing;
using DriveThroughSafari.Core.ViewModels;

namespace DriveThroughSafari.Touch.Views
{
    public partial class WelcomeView : MvxViewController
    {
        public WelcomeView()
            : base("WelcomeView", null)
        {
        }

        MPMoviePlayerController moviePlayer;

        public new WelcomeViewModel ViewModel
        {
            get { return (WelcomeViewModel)base.ViewModel; }
            set { base.ViewModel = value; }
        }

        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();
			
            // Release any cached data, images, etc that aren't in use.
        }


        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
			
            // Perform any additional setup after loading the view, typically from a nib.
            btnGetStarted.TouchUpInside += (object sender, System.EventArgs e) =>
            {
                var presenter = (SafariPresenter)Mvx.Resolve<IMvxTouchViewPresenter>();
                presenter.ToggleNavigation();
            };

            //NavigationController.SetNavigationBarHidden (true, false);
            moviePlayer = new MPMoviePlayerController(NSUrl.FromFilename("Safari.m4v"));
            View.AddSubview(moviePlayer.View);

            moviePlayer.ControlStyle = MPMovieControlStyle.None;
            moviePlayer.View.Frame = UIScreen.MainScreen.Bounds;
            moviePlayer.RepeatMode = MPMovieRepeatMode.One;
            moviePlayer.ScalingMode = MPMovieScalingMode.Fill;
            moviePlayer.Play();

            btnGetStarted.SetTitle("", UIControlState.Normal);
            var image = AppStyles.GetImage("Images/Welcome/WWDTS_wbutton");
            btnGetStarted.SetBackgroundImage(image, UIControlState.Normal);
            btnGetStarted.Frame = new RectangleF((View.Bounds.Width / 2) - (image.Size.Width / 2), UIScreen.MainScreen.Bounds.Height - 100, image.Size.Width, image.Size.Height);
            //View.AddSubview(new UIImageView(AppStyles.GetImage("Images/Welcome/WWDTS_blackfade")));

            View.BringSubviewToFront(btnGetStarted);

            if (!ViewModel.Settings.HasSeenTutorial)
            {
                ShowTutorial();
            }
        }


        public override void ViewDidDisappear(bool animated)
        {
            moviePlayer.Stop();
            moviePlayer.Dispose();

            base.ViewDidDisappear(animated);
        }

        void ShowTutorial()
        {
            var btnImage = new UIButton(UIScreen.MainScreen.Bounds);
            btnImage.SetBackgroundImage(AppStyles.GetImage("Images/Welcome/WWDTS_tutorial"), UIControlState.Normal);
            View.AddSubview(btnImage);
            btnImage.TouchUpInside += (sender, e) =>
            {
                ViewModel.DismissTutorial.Execute(null);
                btnImage.RemoveFromSuperview();
            };
        }

        public void PlayVideoLoop()
        {
            moviePlayer.Play();
        }

        public void PauseVideoLoop()
        {
            moviePlayer.Pause();
        }
    }
}

