using System;
using System.Collections.Generic;
using System.Drawing;
using Cirrious.MvvmCross.Binding.BindingContext;
using Cirrious.MvvmCross.Touch.Views;
using DriveThroughSafari.Core.ViewModels;
using MonoTouch.CoreGraphics;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using SlidingPanels.Lib;
using SlidingPanels.Lib.PanelContainers;

namespace DriveThroughSafari.Touch.Views
{
    public partial class HomeView : MvxViewController
    {
        public new HomeViewModel ViewModel
        {
            get
            {
                return base.ViewModel as HomeViewModel;
            }
        }

        static bool UserInterfaceIdiomIsPhone
        {
            get
            {
                return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone;
            }
        }

        private UIBarButtonItem CreateSliderButton(string imageName, PanelType panelType)
        {
            UIButton button = new UIButton(new RectangleF(0, 0, 40f, 40f));
            button.SetBackgroundImage(UIImage.FromBundle(imageName), UIControlState.Normal);
            button.TouchUpInside += delegate
            {
                SlidingPanelsNavigationViewController navController = NavigationController as SlidingPanelsNavigationViewController;
                navController.TogglePanel(panelType);
            };

            return new UIBarButtonItem(button);
        }

        public HomeView()
            : base()
        {
            NavigationItem.LeftBarButtonItem = CreateSliderButton("Images/SlideRight40.png", PanelType.LeftPanel);
            NavigationItem.RightBarButtonItem = CreateSliderButton("Images/SlideLeft40.png", PanelType.RightPanel);
        }

        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            // Perform any additional setup after loading the view, typically from a nib.
         //   LeftArrowImage.Image = UIImage.FromBundle("Images/LeftArrow.png");
         //   UpArrowImage.Image = UIImage.FromBundle("Images/UpArrow.png");
         //   RightArrowImage.Image = UIImage.FromBundle("Images/RightArrow.png");


            this.AddBindings(
                new Dictionary<object, string>()
			    {
					{this, "Title DisplayName"},
				//	{CenterText, "Text CenterText"},
				//	{NavigateText, "Text DoNextLabel"},
				//	{NavigateButton, "TouchUpInside DoNextCommand"}
				});

            NavigationController.NavigationBar.TintColor = UIColor.Black;
        }



        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

        }
    }
}