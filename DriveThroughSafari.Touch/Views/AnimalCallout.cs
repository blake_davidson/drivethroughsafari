﻿using System;
using MonoTouch.UIKit;
using System.Drawing;
using MonoTouch.Foundation;

namespace DriveThroughSafari.Touch
{
    public sealed class AnimalCallout : UIView
    {
        private readonly UIImageView _mainImage;
        private readonly UILabel _label;

        public AnimalCallout(byte[] animalImage, string title, string description)
            : base()
        {
            this.Frame = new RectangleF(0, 0, 100, 50);
            BackgroundColor = UIColor.FromRGB(245, 245, 245);

            _mainImage = new UIImageView(new RectangleF(0, 0, 20, 20));
            _mainImage.Image = UIImage.LoadFromData(NSData.FromArray(animalImage));
            _mainImage.ContentMode = UIViewContentMode.ScaleAspectFit;
            _mainImage.CreateCircleImage();

            AddSubview(_mainImage);

            _label = new UILabel(new RectangleF(20, 0, 80, 50));
            _label.Text = title;

            AddSubview(_label);
        }
    }
}

