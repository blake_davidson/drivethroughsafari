using System;
using System.Drawing;
using System.Linq;
using Cirrious.MvvmCross.Binding.BindingContext;
using Cirrious.MvvmCross.Binding.Touch.Views;
using Cirrious.MvvmCross.Touch.Views;
using DriveThroughSafari.Core.ViewModels;
using DriveThroughSafari.Touch.Controls;
using DriveThroughSafari.Touch.Views.PhotoBooth;
using MonoTouch.CoreGraphics;
using MonoTouch.CoreLocation;
using MonoTouch.Foundation;
using MonoTouch.ObjCRuntime;
using MonoTouch.UIKit;
using MonoTouch.MapKit;
using SDWebImage;

namespace DriveThroughSafari.Touch.Views
{

    [Register("TourView")]
    [DisableMenuGesture]
    public class TourView : MvxViewController
    {

        public MKMapView Map;
        private UICollectionView _animals;
        private UIButton _backButton;
        public string Id = "CustomMapAnnotation";
        private UIView _outsideOfSafariView;

        public new TourViewModel ViewModel
        {
            get { return (TourViewModel)base.ViewModel; }
            set { base.ViewModel = value; }
        }

        public TourView()
        {
            Map = new MKMapView
            {
                MapType = MKMapType.Standard,
                ShowsUserLocation = true,
                ZoomEnabled = true,
                ScrollEnabled = true,
                UserTrackingMode = MKUserTrackingMode.Follow
            };
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            var region = MKCoordinateRegion.FromDistance(Map.UserLocation.Coordinate, 50, 50);
            Map.SetRegion(region, true);

            Map.GetViewForAnnotation += GetViewForAnnotation;

            var layout = new UICollectionViewFlowLayout();
            layout.ScrollDirection = UICollectionViewScrollDirection.Horizontal;
            layout.ItemSize = new SizeF(100, 100);

            _animals = new UICollectionView(new RectangleF(0, 0, View.Bounds.Width, 100), layout)
            {
                BackgroundColor = UIColor.Black,
                Alpha = .75f
            };

            _animals.RegisterNibForCell(AnimalCutoutCell.Nib, AnimalCutoutCell.Key);

            var source = new MvxCollectionViewSource(_animals, AnimalCutoutCell.Key);

            _animals.Source = source;
            CreateBackButton();
            NavigationItem.LeftBarButtonItem = new UIBarButtonItem(_backButton);

            SetupOutsideofSafariView();

            var set = this.CreateBindingSet<TourView, TourViewModel>();
            set.Bind(source).To(vm => vm.AnimalsAtLocation);
            set.Bind(_animals).For(a => a.Hidden).To(vm => vm.HideAnimalsView);
            set.Bind(source).For(s => s.SelectionChangedCommand).To(vm => vm.ItemSelectedCommand);
            set.Bind(_backButton).To(vm => vm.GoHome);
            set.Bind(_outsideOfSafariView).For(a => a.Hidden).To(vm => vm.HideWarning);
            set.Apply();

            View = Map;
            View.AddSubview(_outsideOfSafariView);
            View.AddSubview(_animals);

            AddMarkers();
            AddHeadingButton();

            _animals.ReloadData();
        }

        public void AddMarkers()
        {
            if (ViewModel.Markers != null)
            {
                var markers = ViewModel.Markers;

                foreach (var mapMarker in ViewModel.Markers.Select(marker => new BasicMapAnnotation(new CLLocationCoordinate2D(marker.Location.Lat, marker.Location.Lng), marker.Title, marker.Message, marker.Animal)))
                {
                    Map.AddAnnotation(mapMarker);
                }
            }
        }

        public void CreateBackButton()
        {
            var image = UIImage.FromBundle(AppStyles.BackButtonPath);
            _backButton = UIButton.FromType(UIButtonType.Custom);
            _backButton.SetBackgroundImage(image, UIControlState.Normal);
            _backButton.Frame = new RectangleF(0, 0, image.Size.Width, image.Size.Height);

        }


        private void AddHeadingButton()
        {

            //this.NavigationItem.SetRightBarButtonItem(
            //    new UIBarButtonItem(UIImage.FromFile(@"Images/LocationBlue.png")
            //                        , UIBarButtonItemStyle.Plain
            //                        , (sender, args) =>
            //                            {
            //                                // button was clicked
            //                                Map.UserTrackingMode = Map.UserTrackingMode == MKUserTrackingMode.None
            //                                                           ? MKUserTrackingMode.Follow
            //                                                           : MKUserTrackingMode.None;
            //                            })
            //    , true);

        }

        void SetupOutsideofSafariView()
        {
            _outsideOfSafariView = new UIView(View.Frame);
            _outsideOfSafariView.BackgroundColor = UIColor.White;
            _outsideOfSafariView.Alpha = .75f;
            var lbl = new UILabel(_outsideOfSafariView.Frame);
            lbl.LineBreakMode = UILineBreakMode.CharacterWrap;
            lbl.Lines = 0;
            lbl.TextColor = UIColor.Black;
            lbl.Text = "YOU CURRENTLY ARE NOT AT THE SAFARI. PLEASE COME VISIT US SOON!";
            lbl.Font = AppStyles.Langdon;
            lbl.TextAlignment = UITextAlignment.Center;
            _outsideOfSafariView.Add(lbl);
            _outsideOfSafariView.Hidden = true;
        }

        private void PictureChosen(object sender, PictureChosenEventArgs e)
        {
            // e.Animal.ShowAnimal();
        }

        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);

            ViewModel.StopService.Execute(null);
            Map.SetUserTrackingMode(MKUserTrackingMode.None, false);
        }

        MKAnnotationView GetViewForAnnotation(MKMapView mapView, NSObject annotation)
        {
            if (annotation is MKUserLocation)
                return null; 

            var view = mapView.DequeueReusableAnnotation(Id);
            if (view == null)
            {
                view = new MKAnnotationView(annotation, Id);
            }
            else
            {
                view.Annotation = annotation;
            }

            view.CanShowCallout = true;
            // (view as MKAnnotationView).AnimatesDrop = true;

            var backgroungImage = new UIImageView(UIImage.FromBundle("Images/SafariMapPin"));

            var circleImage = new UIImageView(new RectangleF(9, 6, backgroungImage.Frame.Size.Width / 2, backgroungImage.Frame.Size.Height / 2));
            circleImage.SetImage(NSUrl.FromString((annotation as BasicMapAnnotation).Animal.ImageUrl), UIImage.FromBundle("Images/Tabs/animals"), SDWebImageOptions.ContinueInBackground);
            circleImage.CreateCircleImage(1);

            var v = new UIView(backgroungImage.Frame);
            v.AddSubview(backgroungImage);
            v.AddSubview(circleImage);

            UIGraphics.BeginImageContext(v.Frame.Size);
            var ctx = UIGraphics.GetCurrentContext();
            if (ctx != null)
            {
                v.Layer.RenderInContext(ctx);
                view.Image = UIGraphics.GetImageFromCurrentImageContext();
                UIGraphics.EndImageContext();
            }

            return view;
        }


        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

            ViewModel.StartService.Execute(null);
            Map.SetUserTrackingMode(MKUserTrackingMode.Follow, false);
        }

        class MapDelegate : MKMapViewDelegate
        {
            //Override OverLayRenderer to draw Polyline returned from directions
            public override MKOverlayRenderer OverlayRenderer(MKMapView mapView, IMKOverlay overlay)
            {
                if (overlay is MKPolyline)
                {
                    var route = (MKPolyline)overlay;
                    var renderer = new MKPolylineRenderer(route) { StrokeColor = UIColor.Blue };
                    return renderer;
                }
                return null;
            }
        }

    }
}