﻿using System;
using MonoTouch.UIKit;
using System.Drawing;
using MonoTouch.Foundation;
using DriveThroughSafari.Core.ViewModels;

namespace DriveThroughSafari.Touch
{
    public enum FlashState
    {
        Auto,
        On,
        Off
    }

    public sealed class Overlayview : UIView
    {
		
        private FlashState _flashState;
        private bool _frontCamera;

        public UIButton BtnTakePicture { get; set; }

        public UIButton BtnToggleFlash { get; set; }

        public UIButton BtnSwitchCamera { get; set; }

        public UIButton BtnBack { get; set; }

        public UIImageView Overlay { get; set; }

        public Overlayview(UIImage overlay, PhotoBoothCameraView camera, PhotoBoothViewModel viewmodel)
            : base(new RectangleF(0, 0, UIScreen.MainScreen.Bounds.Width, UIScreen.MainScreen.Bounds.Height))
        {
            BtnSwitchCamera = null;
            BtnTakePicture = null;
            BtnToggleFlash = null;
            Overlay = new UIImageView(overlay);

            AddSubview(Overlay);

            var photoImage = UIImage.FromBundle("Images/Camera/takepicture");
            BtnTakePicture = new UIButton(new RectangleF((Bounds.Width - photoImage.Size.Width) / 2, Bounds.Height - photoImage.Size.Height - 10, photoImage.Size.Width, photoImage.Size.Height));
            BtnTakePicture.SetBackgroundImage(photoImage, UIControlState.Normal);
            BtnTakePicture.TouchUpInside += (object sender, EventArgs e) =>
            {
                camera.TakePicture();
                viewmodel.IsBusy = true;
            };
            AddSubview(BtnTakePicture);

            if (UIImagePickerController.IsFlashAvailableForCameraDevice(UIImagePickerControllerCameraDevice.Rear) ||
                UIImagePickerController.IsFlashAvailableForCameraDevice(UIImagePickerControllerCameraDevice.Front))
            {
                _flashState = FlashState.Auto;

                var photoFlash = UIImage.FromBundle("Images/Camera/auto");
                BtnToggleFlash = new UIButton(new RectangleF(Bounds.Width - photoFlash.Size.Width - 10, Bounds.Height - photoFlash.Size.Height - 15, photoFlash.Size.Width, photoFlash.Size.Height));
                BtnToggleFlash.SetBackgroundImage(photoFlash, UIControlState.Normal);
                BtnToggleFlash.TouchUpInside += (object sender, EventArgs e) =>
                {
                    try
                    {
                        switch (_flashState)
                        {
                            case FlashState.Auto:
                                BtnToggleFlash.SetImage(UIImage.FromBundle("Images/Camera/on"), UIControlState.Normal);
                                _flashState = FlashState.On;
                                break;
                            case FlashState.On:
                                BtnToggleFlash.SetImage(UIImage.FromBundle("Images/Camera/off"), UIControlState.Normal);
                                _flashState = FlashState.Off;
                                break;
                            case FlashState.Off:
                                BtnToggleFlash.SetImage(UIImage.FromBundle("Images/Camera/auto"), UIControlState.Normal);
                                _flashState = FlashState.Auto;
                                break;
                        }

                    }
                    catch
                    {
                    }
                };
                AddSubview(BtnToggleFlash);
				
            }


            if (UIImagePickerController.IsCameraDeviceAvailable(UIImagePickerControllerCameraDevice.Front))
            {
                _frontCamera = false;

                var photoFlipCamera = UIImage.FromBundle("Images/Camera/switch");
                BtnSwitchCamera = new UIButton(new RectangleF(photoFlipCamera.Size.Width - 10, photoFlipCamera.Size.Height + 5, photoFlipCamera.Size.Width, photoFlipCamera.Size.Height));
                BtnSwitchCamera.SetImage(photoFlipCamera, UIControlState.Normal);
                BtnSwitchCamera.TouchUpInside += (object sender, EventArgs e) =>
                {
                    if (_frontCamera)
                    {
                        camera.SetCameraDevice(UIImagePickerControllerCameraDevice.Rear);
                    }
                    else
                    {
                        camera.SetCameraDevice(UIImagePickerControllerCameraDevice.Front);
                    }

                    _frontCamera = !_frontCamera;
                };
                AddSubview(BtnSwitchCamera);
            }

            var img = UIImage.FromBundle(AppStyles.BackButtonPath);
            BtnBack = new UIButton(new RectangleF(10, 10, img.Size.Width, img.Size.Height));
            BtnBack.SetImage(img, UIControlState.Normal);
            BtnBack.TouchUpInside += delegate
            {
                camera.Stop();
                camera.DismissViewController(true, null);
            };
            AddSubview(BtnBack);
        }
    }
}

