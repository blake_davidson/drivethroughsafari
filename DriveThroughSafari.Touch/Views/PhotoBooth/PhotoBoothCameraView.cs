﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.AVFoundation;
using MonoTouch.CoreVideo;
using MonoTouch.CoreMedia;
using MonoTouch.CoreGraphics;
using MonoTouch.CoreFoundation;
using System.Runtime.InteropServices;
using System.Threading;

namespace DriveThroughSafari.Touch
{
    public class PhotoBoothCameraView : UIViewController
    {
        public PhotoBoothCameraView(Action<UIImage> pictureTaken)
        {
            PictureTaken = pictureTaken;
        }

        public Action<UIImage> PictureTaken;

        public UIImageView ImageView;
        public Overlayview OverlayView;
        private bool _frontCamera;

        UIView MainView;

        AVCaptureSession session;
        OutputRecorder outputRecorder;
        DispatchQueue queue;
        AVCaptureDevice captureDevice;

        public AVCaptureFlashMode FlashMode { get; set; }

        public Boolean Capture;

        public void SetupView()
        {
            MainView = new UIView(new RectangleF(0, 0, UIScreen.MainScreen.Bounds.Width, UIScreen.MainScreen.Bounds.Height));
            this.View = MainView;
           
            ImageView = new UIImageView(new RectangleF(0, 0, UIScreen.MainScreen.Bounds.Width, UIScreen.MainScreen.Bounds.Height));
            ImageView.ContentMode = UIViewContentMode.ScaleAspectFill;

            captureDevice = null;

            FlashMode = AVCaptureFlashMode.Auto;

            MainView.Add(ImageView);
            MainView.Add(OverlayView);

            SetupCaptureSession();
        }

        bool SetupCaptureSession()
        {
            // configure the capture session for low resolution, change this if your code
            // can cope with more data or volume
            session = new AVCaptureSession()
            {
                SessionPreset = AVCaptureSession.PresetHigh
            };

            captureDevice = null;

            // create a device input and attach it to the session
            var devices = AVCaptureDevice.DevicesWithMediaType(AVMediaType.Video);
            foreach (var device in devices)
            {
                captureDevice = device;
                if (_frontCamera && device.Position == AVCaptureDevicePosition.Front)
                    break;

                if (!_frontCamera && device.Position == AVCaptureDevicePosition.Back)
                    break;
            }


            if (captureDevice == null)
            {
                Console.WriteLine("No captureDevice - this won't work on the simulator, try a physical device");
                return false;
            }

            //Configure for 15 FPS. Note use of LockForConigfuration()/UnlockForConfiguration()
            NSError error = null;
            captureDevice.LockForConfiguration(out error);
            if (error != null)
            {
                Console.WriteLine(error);
                captureDevice.UnlockForConfiguration();
                return false;
            }

            if (captureDevice.HasFlash && captureDevice.IsFlashModeSupported(FlashMode))
            {
                captureDevice.FlashMode = FlashMode;
            }

            if (UIDevice.CurrentDevice.CheckSystemVersion(7, 0))
                captureDevice.ActiveVideoMinFrameDuration = new CMTime(1, 15);
            captureDevice.UnlockForConfiguration();


            var input = AVCaptureDeviceInput.FromDevice(captureDevice);
            if (input == null)
            {
                Console.WriteLine("No input - this won't work on the simulator, try a physical device");
                return false;
            }
            session.AddInput(input);

            // create a VideoDataOutput and add it to the sesion
            var output = new AVCaptureVideoDataOutput()
            {
                VideoSettings = new AVVideoSettings(CVPixelFormatType.CV32BGRA),
            };


            // configure the output
            queue = new MonoTouch.CoreFoundation.DispatchQueue("myQueue");
            outputRecorder = new OutputRecorder(this);
            output.SetSampleBufferDelegate(outputRecorder, queue);
            session.AddOutput(output);

            session.StartRunning();
            return true;
        }

        public void TakePicture()
        {
            Capture = true;
        }

        public void Stop()
        {
            if (outputRecorder != null)
                outputRecorder.CancelTokenSource.Cancel();

            //Try removing all existing outputs prior to closing the session
            try
            {
                while (session.Outputs.Length > 0)
                    session.RemoveOutput(session.Outputs[0]);
            }
            catch
            {
            }

            //Try to remove all existing inputs prior to closing the session
            try
            {
                while (session.Inputs.Length > 0)
                    session.RemoveInput(session.Inputs[0]);
            }
            catch
            {
            }

            if (session.Running)
                session.StopRunning();
        }

        public void SetCameraDevice(UIImagePickerControllerCameraDevice cameraDevice)
        {
            _frontCamera = cameraDevice == UIImagePickerControllerCameraDevice.Front;
            Stop();
            SetupCaptureSession();
        }
    }

    public class OutputRecorder : AVCaptureVideoDataOutputSampleBufferDelegate
    {
        private bool capturing = false;

        PhotoBoothCameraView CameraView;

        public CancellationTokenSource CancelTokenSource = new CancellationTokenSource();

        public OutputRecorder(PhotoBoothCameraView cameraView)
        {
            CameraView = cameraView;
        }

        public override void DidOutputSampleBuffer(AVCaptureOutput captureOutput, CMSampleBuffer sampleBuffer, AVCaptureConnection connection)
        {

            if (capturing || CancelTokenSource.IsCancellationRequested)
            {
                if (sampleBuffer != null)
                {
                    sampleBuffer.Dispose();
                    sampleBuffer = null;
                }
                return;
            }

            try
            {
                var image = ImageFromSampleBuffer(sampleBuffer);

                // Do something with the image, we just stuff it in our main view.
                CameraView.ImageView.BeginInvokeOnMainThread(delegate
                    {
                        if (!capturing)
                        {
                            CameraView.ImageView.Image = image;
                            CameraView.ImageView.Transform = CGAffineTransform.MakeRotation((float)Math.PI / 2);

                            if (CameraView.Capture)
                            {
                                capturing = true;
                                var view = new UIView(new RectangleF(0, 0, UIScreen.MainScreen.Bounds.Width, UIScreen.MainScreen.Bounds.Height));
                                view.Add(CameraView.ImageView);
                                view.Add(CameraView.OverlayView.Overlay);

                                UIGraphics.BeginImageContext(CameraView.OverlayView.Bounds.Size);
                                view.Layer.RenderInContext(UIGraphics.GetCurrentContext());
                                var img = UIGraphics.GetImageFromCurrentImageContext();
                                UIGraphics.EndImageContext();
                                CameraView.Capture = false;

                                capturing = false;
                                CameraView.PictureTaken(img);
                                sampleBuffer.Dispose();
                                sampleBuffer = null;
                            }
                        }
                    });


                //
                // Although this looks innocent "Oh, he is just optimizing this case away"
                // this is incredibly important to call on this callback, because the AVFoundation
                // has a fixed number of buffers and if it runs out of free buffers, it will stop
                // delivering frames. 
                //  
                sampleBuffer.Dispose();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        UIImage ImageFromSampleBuffer(CMSampleBuffer sampleBuffer)
        {
            // Get the CoreVideo image
            using (var pixelBuffer = sampleBuffer.GetImageBuffer() as CVPixelBuffer)
            {
                // Lock the base address
                pixelBuffer.Lock(0);
                // Get the number of bytes per row for the pixel buffer
                var baseAddress = pixelBuffer.BaseAddress;
                int bytesPerRow = pixelBuffer.BytesPerRow;
                int width = pixelBuffer.Width;
                int height = pixelBuffer.Height;
                var flags = CGBitmapFlags.PremultipliedFirst | CGBitmapFlags.ByteOrder32Little;
                // Create a CGImage on the RGB colorspace from the configured parameter above
                using (var cs = CGColorSpace.CreateDeviceRGB())
                using (var context = new CGBitmapContext(baseAddress, width, height, 8, bytesPerRow, cs, (CGImageAlphaInfo)flags))
                using (var cgImage = context.ToImage())
                {
                    pixelBuffer.Unlock(0);
                    return UIImage.FromImage(cgImage);
                }
            }
        }
    }
}

