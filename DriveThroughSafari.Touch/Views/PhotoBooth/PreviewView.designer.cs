// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace DriveThroughSafari.Touch
{
	[Register ("PreviewView")]
	partial class PreviewView
	{
		[Outlet]
		MonoTouch.UIKit.UIImageView imgMain { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (imgMain != null) {
				imgMain.Dispose ();
				imgMain = null;
			}
		}
	}
}
