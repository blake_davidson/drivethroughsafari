﻿using System;
using Cirrious.MvvmCross.Binding.Touch.Views;
using DriveThroughSafari.Core.ViewModels;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using DriveThroughSafari.Core;
using DriveThroughSafari.Core.Models;

namespace DriveThroughSafari.Touch.Views.PhotoBooth
{
    public class PhotoBoothItemSource
        : MvxCollectionViewSource
    {
        public delegate void PictureChosenEventHandler(object sender,PictureChosenEventArgs e);

        public PhotoBoothItemSource(UICollectionView collectionView, NSString defaultCellId)
            : base(collectionView, defaultCellId)
        {
        }

        public event PictureChosenEventHandler PictureChosen;

        public override int NumberOfSections(UICollectionView collectionView)
        {
            return 1;
        }

        public override bool ShouldHighlightItem(UICollectionView collectionView, NSIndexPath indexPath)
        {
            return true;
        }

        public override void ItemHighlighted(UICollectionView collectionView, NSIndexPath indexPath)
        {
            var cell = collectionView.CellForItem(indexPath);
            cell.ContentView.BackgroundColor = UIColor.LightGray;
        }

        public override void ItemUnhighlighted(UICollectionView collectionView, NSIndexPath indexPath)
        {
            var cell = collectionView.CellForItem(indexPath);
            cell.ContentView.BackgroundColor = UIColor.Clear;
        }

        public override void ItemSelected(UICollectionView collectionView, NSIndexPath indexPath)
        {
            var cell = (AnimalCutoutCell)collectionView.CellForItem(indexPath);
       
            var args = new PictureChosenEventArgs { PictureChosen = cell.MainImage, Item = (PhotoboothItem)cell.DataContext };
            PictureChosen(this, args);
        }
            
            
    }

    public class PictureChosenEventArgs : EventArgs
    {
        public UIImageView PictureChosen { get; set; }

        public PhotoboothItem Item { get; set; }
    }
}