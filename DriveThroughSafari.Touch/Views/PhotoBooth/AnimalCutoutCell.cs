using System;
using Cirrious.MvvmCross.Binding.BindingContext;
using Cirrious.MvvmCross.Binding.Touch.Views;
using DriveThroughSafari.Core.ViewModels;
using MonoTouch.CoreGraphics;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using DriveThroughSafari.Core;
using DriveThroughSafari.Core.Models;

namespace DriveThroughSafari.Touch.Views.PhotoBooth
{
    public sealed partial class AnimalCutoutCell : MvxCollectionViewCell
    {
        public static readonly UINib Nib = UINib.FromName("AnimalCutoutCell", NSBundle.MainBundle);
        public static readonly NSString Key = new NSString("AnimalCutoutCell");

        public AnimalCutoutCell(IntPtr handle)
            : base(handle)
        {
            BackgroundView = new UIView { BackgroundColor = UIColor.Clear };
            SelectedBackgroundView = new UIView { BackgroundColor = UIColor.Clear };

            ContentView.Layer.BorderColor = UIColor.Clear.CGColor;
            ContentView.Layer.BorderWidth = 0f;
            ContentView.BackgroundColor = UIColor.Clear;
            ContentView.Layer.BackgroundColor = UIColor.Clear.CGColor;
            ContentView.Transform = CGAffineTransform.MakeScale(0.8f, 0.8f);
           

            MainImage = new UIImageView();
            MainImage.BackgroundColor = UIColor.Clear;
            MainImage.Opaque = false;
            MainImage.Layer.BackgroundColor = UIColor.Clear.CGColor;

            this.DelayBind(() =>
                {
                    MvxFluentBindingDescriptionSet<AnimalCutoutCell, Animal> set =
                        this.CreateBindingSet<AnimalCutoutCell, Animal>();
                    //  set.Bind(_loader).To(animal => animal.ImageUrl);
                    set.Bind(MainImage).For("SDWebImage").To(vm => vm.ImageUrl);
                    set.Apply();
                });

            MainImage.Center = ContentView.Center;
            MainImage.Transform = CGAffineTransform.MakeScale(0.7f, 0.7f);
        }

        public static AnimalCutoutCell Create()
        {
            return (AnimalCutoutCell)Nib.Instantiate(null, null)[0];
        }
    }
}