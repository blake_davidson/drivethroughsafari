﻿using System;
using Cirrious.MvvmCross.Touch.Views;
using MonoTouch.UIKit;
using DriveThroughSafari.Core.ViewModels;
using DriveThroughSafari.Touch.Views.PhotoBooth;
using Cirrious.MvvmCross.Binding.BindingContext;
using System.Drawing;
using MonoTouch.CoreGraphics;
using MonoTouch.CoreAnimation;
using MonoTouch.Foundation;

namespace DriveThroughSafari.Touch
{
    public class PhotoBoothView
        : MvxCollectionViewController
    {
        private readonly bool _isInitialized;
        private UIButton _backButton;
        private PhotoBoothCameraView _camera;

        public PhotoBoothView()
            : base(new LineLayout
                { HeaderReferenceSize = new SizeF(160, 100),
                    ScrollDirection = UICollectionViewScrollDirection.Horizontal
                })
        {
            _isInitialized = true;
            CollectionView.ContentInset = new UIEdgeInsets(50, 0, 0, 0);
            ViewDidLoad();

        }

        public new PhotoBoothViewModel ViewModel
        {
            get { return (PhotoBoothViewModel)base.ViewModel; }
            set { base.ViewModel = value; }
        }

        public override sealed void ViewDidLoad()
        {
            if (!_isInitialized)
                return;

            base.ViewDidLoad();

            CollectionView.RegisterNibForCell(AnimalCutoutCell.Nib, AnimalCutoutCell.Key);

            //Get Source
            var source = new PhotoBoothItemSource(CollectionView, AnimalCutoutCell.Key);

            //Wire up chosen event
            source.PictureChosen += PictureChosen;

            //Set Source
            CollectionView.Source = source;

            CollectionView.BackgroundColor = UIColor.Clear;
            CollectionView.BackgroundView = null;
            View.BackgroundColor = UIColor.FromPatternImage(AppStyles.GetImage(AppStyles.ZebraBackground));

            MvxFluentBindingDescriptionSet<PhotoBoothView, PhotoBoothViewModel> set = this.CreateBindingSet<PhotoBoothView, PhotoBoothViewModel>();
            set.Bind(source).To(vm => vm.PhotoBoothItems);
            ViewModel.BindLoadingMessage(View, vm => vm.IsBusy, "Loading...");
            CreateBackButton();
            NavigationItem.LeftBarButtonItem = new UIBarButtonItem(_backButton);
            set.Apply();

            CollectionView.ReloadData();
           
        }

        public void CreateBackButton()
        {
            var image = UIImage.FromBundle(AppStyles.BackButtonPath);
            _backButton = UIButton.FromType(UIButtonType.Custom);
            _backButton.SetBackgroundImage(image, UIControlState.Normal);
            _backButton.Frame = new RectangleF(0, 0, image.Size.Width, image.Size.Height);
            _backButton.TouchUpInside += delegate
            {
                ViewModel.Home().Execute(null);
            };

        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            NavigationController.NavigationBar.TintColor = UIColor.Red;
        }


        private void PictureChosen(object sender, PictureChosenEventArgs e)
        {
            _camera = new PhotoBoothCameraView(PictureTaken);

            //Use Chosen Picture as Overlay
            var overlay = new Overlayview(e.PictureChosen.Image, _camera, ViewModel);
            _camera.OverlayView = overlay;


            if (overlay.BtnTakePicture != null)
                overlay.BringSubviewToFront(overlay.BtnTakePicture);

            _camera.SetupView();

            NavigationController.PresentViewController(_camera, true, null);

        }

        private void PictureTaken(UIImage image)
        {
            _camera.DismissViewController(false, null);

            using (NSData imageData = image.AsPNG())
            {
                Byte[] myByteArray = new Byte[imageData.Length];
                System.Runtime.InteropServices.Marshal.Copy(imageData.Bytes, myByteArray, 0, Convert.ToInt32(imageData.Length));
                ViewModel.Image = myByteArray;
                myByteArray = null;
            }

            ViewModel.IsBusy = false;
            ViewModel.ShowPreview.Execute(null);
        }

    }


    public class LineLayout : UICollectionViewFlowLayout
    {
        public const float ITEM_SIZE = 200.0f;
        public const int ACTIVE_DISTANCE = 200;
        public const float ZOOM_FACTOR = 0.3f;

        public LineLayout()
        {
            ItemSize = new SizeF(ITEM_SIZE, ITEM_SIZE);
            ScrollDirection = UICollectionViewScrollDirection.Horizontal;
            SectionInset = new UIEdgeInsets(200, 0.0f, 200, 0.0f);
            MinimumLineSpacing = 50.0f;
        }

        public override bool ShouldInvalidateLayoutForBoundsChange(RectangleF newBounds)
        {
            return true;
        }


        public override UICollectionViewLayoutAttributes[] LayoutAttributesForElementsInRect(RectangleF rect)
        {
            var array = base.LayoutAttributesForElementsInRect(rect);
            var visibleRect = new RectangleF(CollectionView.ContentOffset, CollectionView.Bounds.Size);

            foreach (var attributes in array)
            {
                if (attributes.Frame.IntersectsWith(rect))
                {
                    float distance = visibleRect.GetMidX() - attributes.Center.X;
                    float normalizedDistance = distance / ACTIVE_DISTANCE;
                    if (Math.Abs(distance) < ACTIVE_DISTANCE)
                    {
                        float zoom = 1 + ZOOM_FACTOR * (1 - Math.Abs(normalizedDistance));
                        attributes.Transform3D = CATransform3D.MakeScale(zoom, zoom, 1.0f);
                        attributes.ZIndex = 1;
                    }
                }
            }

            return array;
        }

        public override PointF TargetContentOffset(PointF proposedContentOffset, PointF scrollingVelocity)
        {
            float offSetAdjustment = float.MaxValue;
            float horizontalCenter = (float)(proposedContentOffset.X + (this.CollectionView.Bounds.Size.Width / 2.0));
            RectangleF targetRect = new RectangleF(proposedContentOffset.X, 0.0f, this.CollectionView.Bounds.Size.Width, this.CollectionView.Bounds.Size.Height);
            var array = base.LayoutAttributesForElementsInRect(targetRect);
            foreach (var layoutAttributes in array)
            {
                float itemHorizontalCenter = layoutAttributes.Center.X;
                if (Math.Abs(itemHorizontalCenter - horizontalCenter) < Math.Abs(offSetAdjustment))
                {
                    offSetAdjustment = itemHorizontalCenter - horizontalCenter;
                }
            }
            return new PointF(proposedContentOffset.X + offSetAdjustment, proposedContentOffset.Y);
        }
    }


    public class PictureDelegate : UIImagePickerControllerDelegate
    {
        public UIImage SelectedImage { get; set; }

        private PhotoBoothViewModel _viewModel;

        public PictureDelegate(PhotoBoothViewModel viewmodel)
        {
            _viewModel = viewmodel;
        }


        public override void FinishedPickingMedia(UIImagePickerController picker, NSDictionary info)
        {
            var bottomImage = (info[UIImagePickerController.OriginalImage] as UIImage).ScaleAndRotateImage((int)UIScreen.MainScreen.Bounds.Height);
            var bottomImageView = new UIImageView(new RectangleF(0, 0, UIScreen.MainScreen.Bounds.Width, UIScreen.MainScreen.Bounds.Height));
            bottomImageView.Image = bottomImage;
            bottomImageView.ContentMode = UIViewContentMode.ScaleAspectFill;
            bottomImageView.ClipsToBounds = true;
            var topImageView = new UIImageView(new RectangleF(0, 0, UIScreen.MainScreen.Bounds.Width, UIScreen.MainScreen.Bounds.Height));
            SelectedImage.ScaleAndRotateImage((int)UIScreen.MainScreen.Bounds.Height);
            topImageView.Image = SelectedImage;

            topImageView.ContentMode = UIViewContentMode.ScaleAspectFill;
            topImageView.ClipsToBounds = true;

            var combineView = new UIView(new RectangleF(0, 0, UIScreen.MainScreen.Bounds.Width, UIScreen.MainScreen.Bounds.Height));

            picker.DismissViewController(false, null);


            combineView.Add(bottomImageView);
            combineView.Add(topImageView);

            UIGraphics.BeginImageContextWithOptions(combineView.Bounds.Size, false, 0.0f);
            combineView.Layer.RenderInContext(UIGraphics.GetCurrentContext());
            var newImage = UIGraphics.GetImageFromCurrentImageContext();
            UIGraphics.EndImageContext();


//            var rotatedImage = ScaleImage(bottomImage, (int)bottomImage.Size.Height);
//            var imageSize = bottomImage.Size;
//            var overlaySize = SelectedImage.Size;
//
//            var xScaleFactor = bottomImage.Size.Width / UIScreen.MainScreen.Bounds.Width;
//            var yScaleFactor = bottomImage.Size.Height / UIScreen.MainScreen.Bounds.Height;
//
//            if (AppStyles.IsTallScreen())
//            {
//                if (rotatedImage.Size.Height == 1280f)
//                {
//                    yScaleFactor += 0.15f;
//                    xScaleFactor -= 1.0f;
//                }
//                else
//                {
//                    yScaleFactor += 1.05f;
//                }
//            }
//
//            UIGraphics.BeginImageContext(rotatedImage.Size);
//
//            rotatedImage.Draw(new RectangleF(0, 0, imageSize.Width, imageSize.Height));
//            SelectedImage.Draw(new RectangleF(0, 0, overlaySize.Width * xScaleFactor, overlaySize.Height * yScaleFactor));
//            var newImage = UIGraphics.GetImageFromCurrentImageContext();
//
//            UIGraphics.EndImageContext();

            using (NSData imageData = newImage.AsPNG())
            {
                Byte[] myByteArray = new Byte[imageData.Length];
                System.Runtime.InteropServices.Marshal.Copy(imageData.Bytes, myByteArray, 0, Convert.ToInt32(imageData.Length));
                _viewModel.Image = myByteArray;
                myByteArray = null;
            }

            newImage.SaveToPhotosAlbum(null);

            _viewModel.IsBusy = false;
            _viewModel.ShowPreview.Execute(null);

        }

        public override void Canceled(UIImagePickerController picker)
        {
            picker.DismissViewController(true, null);
        }



        public static UIImage ScaleImage(UIImage image, int maxSize)
        {

            UIImage res;

            using (CGImage imageRef = image.CGImage)
            {
                CGImageAlphaInfo alphaInfo = imageRef.AlphaInfo;
                CGColorSpace colorSpaceInfo = CGColorSpace.CreateDeviceRGB();
                if (alphaInfo == CGImageAlphaInfo.None)
                {
                    alphaInfo = CGImageAlphaInfo.NoneSkipLast;
                }

                int width, height;

                width = imageRef.Width;
                height = imageRef.Height;


                if (height >= width)
                {
                    width = (int)Math.Floor((double)width * ((double)maxSize / (double)height));
                    height = maxSize;
                }
                else
                {
                    height = (int)Math.Floor((double)height * ((double)maxSize / (double)width));
                    width = maxSize;
                }


                CGBitmapContext bitmap;

                if (image.Orientation == UIImageOrientation.Up || image.Orientation == UIImageOrientation.Down)
                {
                    bitmap = new CGBitmapContext(IntPtr.Zero, width, height, imageRef.BitsPerComponent, imageRef.BytesPerRow, colorSpaceInfo, alphaInfo);
                }
                else
                {
                    bitmap = new CGBitmapContext(IntPtr.Zero, height, width, imageRef.BitsPerComponent, imageRef.BytesPerRow, colorSpaceInfo, alphaInfo);
                }

                switch (image.Orientation)
                {
                    case UIImageOrientation.Left:
                        bitmap.RotateCTM((float)Math.PI / 2);
                        bitmap.TranslateCTM(0, -height);
                        break;
                    case UIImageOrientation.Right:
                        bitmap.RotateCTM(-((float)Math.PI / 2));
                        bitmap.TranslateCTM(-width, 0);
                        break;
                    case UIImageOrientation.Up:
                        break;
                    case UIImageOrientation.Down:
                        bitmap.TranslateCTM(width, height);
                        bitmap.RotateCTM(-(float)Math.PI);
                        break;
                }

                bitmap.DrawImage(new Rectangle(0, 0, width, height), imageRef);


                res = UIImage.FromImage(bitmap.ToImage());
                bitmap = null;

            }


            return res;
        }



    }
}

