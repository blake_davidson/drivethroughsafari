// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the Xcode designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//

using MonoTouch.Foundation;

namespace DriveThroughSafari.Touch.Views.PhotoBooth
{
    [Register("AnimalCutoutCell")]
	sealed partial class AnimalCutoutCell
	{
		[Outlet]
		public MonoTouch.UIKit.UIImageView MainImage { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (MainImage != null) {
				MainImage.Dispose ();
				MainImage = null;
			}
		}
	}
}
