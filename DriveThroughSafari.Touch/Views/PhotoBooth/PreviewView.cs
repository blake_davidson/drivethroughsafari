﻿using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Cirrious.MvvmCross.Touch.Views;
using Cirrious.MvvmCross.Binding.BindingContext;
using DriveThroughSafari.Core;
using MonoTouch.MessageUI;
using MonoTouch.Social;
using MonoTouch.Accounts;

namespace DriveThroughSafari.Touch
{
    public partial class PreviewView : MvxViewController
    {
        public PreviewView()
            : base("PreviewView", null)
        {
        }

        public new PreviewViewModel ViewModel
        {
            get
            {
                return base.ViewModel as PreviewViewModel;
            }
        }

        private UIButton _backButton;

        SLComposeViewController slComposer;

        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();
			
            // Release any cached data, images, etc that aren't in use.
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
			
            CreateBackButton();
            NavigationItem.LeftBarButtonItem = new UIBarButtonItem(_backButton);

            // Perform any additional setup after loading the view, typically from a nib.
            var set = this.CreateBindingSet<PreviewView, PreviewViewModel>();
            set.Bind(imgMain).For(i => i.Image).To(vm => vm.Image).WithConversion("InMemoryImage");
            set.Bind(_backButton).To(vm => vm.GoBack);
            ViewModel.BindLoadingMessage(View, vm => vm.IsBusy, "Loading...");

            this.NavigationItem.SetRightBarButtonItem(
                new UIBarButtonItem(UIBarButtonSystemItem.Action, (sender, args) =>
                    {
                        var	actionSheet = new UIActionSheet("Share Picture");
                        actionSheet.AddButton("Mail");
                        actionSheet.AddButton("Message");
                        actionSheet.AddButton("Facebook");
                        actionSheet.AddButton("Twitter");
                        actionSheet.AddButton("Cancel");

                        actionSheet.Clicked += (s, e) =>
                        {
                            ViewModel.IsBusy = true;
                            switch (e.ButtonIndex)
                            {
                                case 0:
                                    SendEmail();
                                    break;
                                case 1:
                                    SendSMS();
                                    break;
                                case 2:
                                    PostToFacebook();
                                    break;
                                case 3:
                                    Tweet();
                                    break;
                                case 4:
                                    ViewModel.IsBusy = false;
                                    break;
                            }
                        };

                        actionSheet.ShowFromToolbar(this.NavigationController.Toolbar);

                    })
				, true);

            this.SetToolbarItems(new []
                {
                    new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace) { Width = 10 }
                    , new UIBarButtonItem(UIBarButtonSystemItem.Save, (s, e) => imgMain.Image.SaveToPhotosAlbum(delegate
                            {
                                new UIAlertView("Saved", "Image Saved", null, "OK", null).Show();
                            }))
                }, false);

            this.NavigationController.ToolbarHidden = false;
           

            set.Apply();
        }

        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);

            NavigationController.SetToolbarHidden(true, false);
        }

        public void CreateBackButton()
        {
            var image = UIImage.FromBundle(AppStyles.BackButtonPath);
            _backButton = UIButton.FromType(UIButtonType.Custom);
            _backButton.SetBackgroundImage(image, UIControlState.Normal);
            _backButton.Frame = new RectangleF(0, 0, image.Size.Width, image.Size.Height);

        }

        void PostToFacebook()
        {
            if (SLComposeViewController.IsAvailable(SLServiceKind.Facebook))
            {				

                slComposer = SLComposeViewController.FromService(SLServiceType.Facebook);

                slComposer.SetInitialText("Having fun at Wild Wilderness Drive Through Safari. #GentryZoo ");
                slComposer.AddImage(imgMain.Image);

                slComposer.CompletionHandler += (result) => InvokeOnMainThread(() => DismissViewController(true, null));

                ViewModel.IsBusy = false;

                PresentViewController(slComposer, true, () => ViewModel.IsBusy = false);
            }
            else
            {
                ViewModel.IsBusy = false;
                new UIAlertView("Facebook Account not added", "Please add a facebook account to Settings -> Facebook ", null, "OK", null).Show();
            }
        }

        void SendEmail()
        {
            if (MFMailComposeViewController.CanSendMail)
            {
                MFMailComposeViewController _mailController;

                _mailController = new MFMailComposeViewController();
                _mailController.SetSubject("Wild Wilderness Drive Through Safari");
                _mailController.SetMessageBody("Having fun at the Wild Wilderness Drive Through Safari in Gentry, Ar.", false);
                NSData imgData = imgMain.Image.AsPNG();
                _mailController.AddAttachmentData(imgData, "image/png", "image.png");
                _mailController.Finished += (sender, e) => (DismissViewController(true, null));

                this.PresentViewController(_mailController, true, () => ViewModel.IsBusy = false);
            }
            else
            {
                ViewModel.IsBusy = false;
            }
        }

        void SendSMS()
        {
            var messageController = new MFMessageComposeViewController();

            if (MFMessageComposeViewController.CanSendText)
            {
                messageController.Body = "Having fun at the Wild Wilderness Drive Through Safari in Gentry, Ar.";
                messageController.AddAttachment(imgMain.Image.AsPNG(), "kUTTypePNG", "image.png");
                messageController.Finished += (sender, e) => (DismissViewController(true, null));	

                var img = UIImage.FromFile("chimp.png").AsPNG();

                this.PresentViewController(messageController, true, () => ViewModel.IsBusy = false);
				
            }
            else
            {
                ViewModel.IsBusy = false;
            }
        }

        public void Tweet()
        {
            if (SLComposeViewController.IsAvailable(SLServiceKind.Twitter))
            {

                ViewModel.IsBusy = true;

                slComposer = SLComposeViewController.FromService(SLServiceType.Twitter);

                slComposer.SetInitialText("Having fun at Wild Wilderness Drive Through Safari. #GentryZoo ");
                slComposer.AddImage(imgMain.Image);

                slComposer.CompletionHandler += (result) => InvokeOnMainThread(() => DismissViewController(true, null));

                PresentViewController(slComposer, true, () => ViewModel.IsBusy = false);
            }
            else
            {
                ViewModel.IsBusy = false;
                new UIAlertView("Twitter Account not added", "Please add a twitter account to Settings -> Twitter ", null, "OK", null).Show();
            }
        }
    }
}

