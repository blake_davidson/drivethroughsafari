﻿using System;
using MonoTouch.UIKit;
using Cirrious.MvvmCross.Binding.BindingContext;
using System.Linq.Expressions;
using CrossUI.Touch.Dialog.Elements;
using DriveThroughSafari.Core.ViewModels;
using DriveThroughSafari.Core.Models;
using Cirrious.MvvmCross.Binding.ValueConverters;
using System.Collections.Generic;
using System.Drawing;

namespace DriveThroughSafari.Touch.Views
{
    public class GroupAnimalsView : DialogViewControllerBase
    {
        public GroupAnimalsView()
            : base(UITableViewStyle.Plain)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            var frame = View.Frame;
            frame.Width = 195;
            View.Frame = frame;
            TableView.BackgroundView = null;
            TableView.BackgroundColor = UIColor.Clear;
            TableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            TableView.ScrollEnabled = true;
            View.BackgroundColor = UIColor.FromPatternImage(AppStyles.GetImage("Images/WWDTS_RightMenuBackground"));
            TableView.RowHeight = TableView.RowHeight + 20;

            var binder = this.CreateBindingSet<GroupAnimalsView, GroupAnimalsViewModel>();

            Func<string, Expression<Func<GroupAnimalsViewModel, object>>, StyledStringElement> createElement = (text, property) =>
            {
                var element = new StyledStringElement(text)
                {
                    Accessory = UITableViewCellAccessory.DisclosureIndicator,
                    ShouldDeselectAfterTouch = true,
                    TextColor = UIColor.White,
                    BackgroundColor = UIColor.Clear,
                    Font = AppStyles.Langdon
                };

                binder.Bind(element)
                        .For(el => el.SelectedCommand)
                        .To(property);

                return element;
            };

            Func<string, Category, Expression<Func<GroupAnimalsViewModel, object>>, StyledStringElement> createElementWithParameter = (text, category, property) =>
            {
                var element = new StyledStringElement(text)
                {
                    Accessory = UITableViewCellAccessory.DisclosureIndicator,
                    ShouldDeselectAfterTouch = true,
                    TextColor = UIColor.White,
                    BackgroundColor = UIColor.Clear,
                    Font = AppStyles.Langdon
                };

                binder.Bind(element)
                        .For(el => el.SelectedCommand)
                        .To(property)
                        .WithConversion(new MvxCommandParameterValueConverter(), category);

                return element;
            };

            var rootElement = new RootElement();
            var section = new Section("Group By:");
            section.Add(createElement("A-Z", vm => vm.AtoZSort));
            section.Add(createElement("Z-A", vm => vm.ZtoASort));
            section.Add(createElement("CATEGORY", vm => vm.CategorySort));


            var section2 = new Section("Filter By Category");
            section2.Add(createElementWithParameter("BIRDS", Category.Bird, vm => vm.FilterByCategory));
            section2.Add(createElementWithParameter("CATS", Category.Cat, vm => vm.FilterByCategory));
            section2.Add(createElementWithParameter("DEEAR AND ANTELOPE", Category.DeerAndAntelope, vm => vm.FilterByCategory));
            section2.Add(createElementWithParameter("FARM ANIMALS", Category.FarmAnimal, vm => vm.FilterByCategory));
            section2.Add(createElementWithParameter("HOOFSTOCK", Category.Hoofstock, vm => vm.FilterByCategory));
            section2.Add(createElementWithParameter("CANINES", Category.K9, vm => vm.FilterByCategory));
            section2.Add(createElementWithParameter("OTHER MAMMALS", Category.OtherMammal, vm => vm.FilterByCategory));
            section2.Add(createElementWithParameter("PRIMATES", Category.Primates, vm => vm.FilterByCategory));
            section2.Add(createElementWithParameter("REPTILES", Category.Reptile, vm => vm.FilterByCategory));
            section2.Add(createElementWithParameter("RODENTS", Category.Rodent, vm => vm.FilterByCategory));

            rootElement.Add(section);
            rootElement.Add(section2);
            Root = rootElement;
           
            binder.Apply();

        }
    }
}