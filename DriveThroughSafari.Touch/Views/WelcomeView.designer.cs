// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace DriveThroughSafari.Touch.Views
{
	[Register ("WelcomeView")]
	partial class WelcomeView
	{
		[Outlet]
		MonoTouch.UIKit.UIButton btnGetStarted { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (btnGetStarted != null) {
				btnGetStarted.Dispose ();
				btnGetStarted = null;
			}
		}
	}
}
