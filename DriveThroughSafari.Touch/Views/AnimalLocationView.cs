using Cirrious.MvvmCross.Touch.Views;
using DriveThroughSafari.Core.ViewModels;
using MonoTouch.CoreLocation;
using MonoTouch.Foundation;

using MonoTouch.MapKit;

namespace DriveThroughSafari.Touch.Views
{
    [Register("AnimalLocationView")]
    public class AnimalLocationView : MvxViewController
    {
        private MKMapView _map;

        public new AnimalLocationsViewModel ViewModel
        {
            get { return (AnimalLocationsViewModel)base.ViewModel; }
            set { base.ViewModel = value; }
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            _map = new MKMapView
            {
                MapType = MKMapType.Hybrid,
                ZoomEnabled = true,
                ScrollEnabled = true,
            };

            var region = MKCoordinateRegion.FromDistance(new CLLocationCoordinate2D(36.298231, -94.495919), 1200, 1200);
            _map.SetRegion(region, true);

            AddMarkers();

            View = _map;

        }

        public void AddMarkers()
        {
            if (ViewModel.Markers != null)
            {

                foreach (var marker in ViewModel.Markers)
                {
                    var mapMarker = new BasicMapAnnotation(new CLLocationCoordinate2D(marker.Location.Lat, marker.Location.Lng), marker.Title, marker.Message, marker.Animal);
                    _map.AddAnnotation(mapMarker);
                }
            }
        }

    }
}