﻿using System;
using Cirrious.MvvmCross.Touch.Views;
using System.Drawing;
using MonoTouch.UIKit;
using System.Collections;
using MonoTouch.Foundation;
using System.Collections.Generic;
using System.Linq;
using DriveThroughSafari.Core.ViewModels;
using Cirrious.MvvmCross.Binding.Touch.Views;
using SDWebImage;
using Cirrious.MvvmCross.Binding.BindingContext;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.Messenger;
using DriveThroughSafari.Core.Messages;
using MonoTouch.MediaPlayer;

namespace DriveThroughSafari.Touch
{
    public class MediaAlbumView : MvxViewController
    {

        public MediaAlbumView()
        {
            IsFullScreen = false;
        }

        const float kImageDistance = 5;

        public new MediaAlbumViewModel ViewModel
        {
            get { return (MediaAlbumViewModel)base.ViewModel; }
            set { base.ViewModel = value; }
        }

        // public List<UIImage> Images { get; set; }

        public List<UIImageView> ImageViews { get; set; }

        public UIScrollView ContentView { get; set; }

        MvxSubscriptionToken _token;

        UIButton _backButton
        {
            get;
            set;
        }

        private SizeF SizeResizeToHeight(SizeF size, float height)
        {
            size.Width *= height / size.Height;
            size.Height = height;
            return size;
        }

        public override UIInterfaceOrientationMask GetSupportedInterfaceOrientations()
        {
            return base.GetSupportedInterfaceOrientations();
        }

        public override void ViewDidLoad()
        {
            ImageViews = new List<UIImageView>();

            base.ViewDidLoad();

            MonoTouch.ObjCRuntime.Class.ThrowOnInitFailure = false;

            var messenger = Mvx.Resolve<IMvxMessenger>();
            _token = messenger.SubscribeOnMainThread<LoadingChangedMessage>(LoadingMessageReceived);

            var set = this.CreateBindingSet<MediaAlbumView, MediaAlbumViewModel>();
            ViewModel.BindLoadingMessage(View, vm => vm.IsBusy, "Loading...");
            //View.BackgroundColor = UIColor.FromPatternImage(AppStyles.GetImage(AppStyles.ZebraBackground));
            View.BackgroundColor = UIColor.White;

            var refreshControl = new MvxUIRefreshControl();
            refreshControl.TintColor = AppStyles.TintColor;

            set.Bind(refreshControl).For(r => r.IsRefreshing).To(vm => vm.IsBusy);
            set.Bind(refreshControl).For(r => r.RefreshCommand).To(vm => vm.ReloadCommand);
            set.Bind(refreshControl).For(r => r.Message).To(vm => vm.BusyMessage);

            CreateBackButton();
            NavigationItem.LeftBarButtonItem = new UIBarButtonItem(_backButton);
            NavigationController.NavigationBar.TintColor = AppStyles.TintColor;

            var scrollview = new UIScrollView(this.View.Bounds);
            scrollview.AddSubview(refreshControl);
            scrollview.AutoresizingMask = UIViewAutoresizing.FlexibleHeight | UIViewAutoresizing.FlexibleWidth;
            scrollview.Bounces = true;
            scrollview.AlwaysBounceVertical = true;
            scrollview.ShowsVerticalScrollIndicator = false;

            this.ContentView = scrollview;

            this.ContentView.AutoresizingMask = UIViewAutoresizing.FlexibleHeight | UIViewAutoresizing.FlexibleWidth;

            this.View.AddSubview(scrollview);

            var imageViewIndex = 0;               

            foreach (var mediaItem in ViewModel.MediaItems)
            {
                var imageView = new UIImageView();

                using (var url = NSUrl.FromString(mediaItem.ImageUrl))
                {
                    imageView.SetImage(url, UIImage.FromBundle("Images/Tabs/animals"), CompletedHandler);
                }

                if (mediaItem.IsVideo)
                {
                    var playButton = UIImage.FromBundle("/Images/MediaGallery/playButton");
                    var playView = new UIImageView(playButton);
                    imageView.AddSubview(playView);
                }
                imageView.Tag = imageViewIndex;
                this.ImageViews.Add(imageView);
                imageViewIndex++;
               
            }

            set.Apply();
            PlaceImages();
    
        }

        void CompletedHandler(UIImage image, NSError error, SDImageCacheType cacheType)
        {
            InvokeOnMainThread(PlaceImages);
        }


        public void CreateBackButton()
        {
            var image = UIImage.FromBundle(AppStyles.BackButtonPath);
            _backButton = UIButton.FromType(UIButtonType.Custom);
            _backButton.SetBackgroundImage(image, UIControlState.Normal);
            _backButton.Frame = new RectangleF(0, 0, image.Size.Width, image.Size.Height);
            _backButton.TouchUpInside += (object sender, EventArgs e) => ViewModel.GoHome.Execute(null);

        }

        private void LoadingMessageReceived(LoadingChangedMessage message)
        {
            if (message.Sender != ViewModel)
                return;

            PlaceImages();

        }

        public void PlaceImages()
        {
            if (ImageViews.Count < 1)
                return;
                
            var newSize = this.SetFramesToImageViewsToFitSize(this.ImageViews, this.ContentView.Frame.Size);
            this.ContentView.ContentSize = newSize;
            int i = 0;
            foreach (UIImageView imageView in this.ImageViews)
            {
                this.ContentView.AddSubview(imageView);
                imageView.UserInteractionEnabled = true;
                imageView.Tag = i;
                i++;
                UITapGestureRecognizer tapped = new UITapGestureRecognizer(ImgTouchUp);
                tapped.NumberOfTapsRequired = 1;
                imageView.AddGestureRecognizer(tapped);

                if (imageView.Subviews.Any())
                {
                    (imageView.Subviews[0] as UIImageView).Center = imageView.Center;
                }

            }
        }

        void ImgTouchUp(object sender)
        {
            UITapGestureRecognizer gesture = (UITapGestureRecognizer)sender;
            Console.WriteLine("Taped Image tag is {0}", gesture.View.Tag);
            UIImageView imageView = this.ImageViews[gesture.View.Tag];
            var mediItem = ViewModel.MediaItems[gesture.View.Tag];

            if (mediItem.IsVideo)
            {
                PlayVideo(mediItem);
            }
            else
            {
                this.ContentView.BringSubviewToFront(imageView);
                ImgToFullScreen(imageView);
            }

        }

        void PlayVideo(DriveThroughSafari.Core.Models.MediaItem mediItem)
        {
            var player = new MPMoviePlayerViewController();
            player.MoviePlayer.SourceType = MPMovieSourceType.File;
            player.MoviePlayer.ControlStyle = MPMovieControlStyle.Fullscreen;
            player.MoviePlayer.ContentUrl = NSUrl.FromString(mediItem.VideoUrl);

            player.MoviePlayer.Play();
            player.MoviePlayer.Fullscreen = true;

            this.NavigationController.PresentMoviePlayerViewController(player);
        }

        private RectangleF prevFrame;
        public bool IsFullScreen;

        void ImgToFullScreen(UIImageView imageView)
        {
            if (!IsFullScreen)
            {
                UIView.Animate(0.5, 0, 0, () =>
                    {
                        this.NavigationController.NavigationBar.Hidden = true;
                        prevFrame = imageView.Frame;
                        imageView.Frame = UIScreen.MainScreen.Bounds;
                        imageView.ContentMode = UIViewContentMode.ScaleAspectFit;
                    }, () => IsFullScreen = true);
                return;
            }
            else
            {
                UIView.Animate(0.5, 0, 0, () =>
                    {
                        imageView.Frame = prevFrame;
                        NavigationController.NavigationBar.Hidden = false;
                    }, () =>
                    {
                        IsFullScreen = false;
                        PlaceImages();
                    });
                return;
            }

        }


        public SizeF SetFramesToImageViewsToFitSize(List<UIImageView> imageViews, SizeF frameSize)
        {
            int N = imageViews.Count;
            var newFrames = new RectangleF[N];
            float ideal_height = Math.Max(frameSize.Height, frameSize.Width) / 4;
            float[] seq = new float[N];
            float total_width = 0;
            for (int i = 0; i < imageViews.Count(); i++)
            {
                UIImage image = imageViews[i].Image;
                SizeF newSize = SizeResizeToHeight(image.Size, ideal_height);
                newFrames[i] = new RectangleF(new PointF(0, 0), newSize);
                seq[i] = newSize.Width;
                total_width += seq[i];
            }

            int K = (int)(float)Math.Round(total_width / frameSize.Width);
            float[,] M = new float[N, K];
            float[,] D = new float[N, K];
            for (int i = 0; i < N; i++)
                for (int j = 0; j < K; j++)
                    D[i, j] = 0;

            for (int i = 0; i < K; i++)
                M[0, i] = seq[0];

            for (int i = 0; i < N; i++)
            {
                if (i > 0)
                {
                    M[i, 0] = seq[i] + M[i - 1, 0];
                }
                else
                {
                    M[i, 0] = seq[i] + 0;
                }
            }

            float cost;
            for (int i = 1; i < N; i++)
            {
                for (int j = 1; j < K; j++)
                {
                    M[i, j] = 2147483647;
                    for (int k = 0; k < i; k++)
                    {
                        cost = Math.Max(M[k, j - 1], M[i, 0] - M[k, 0]);
                        if (M[i, j] > cost)
                        {
                            M[i, j] = cost;
                            D[i, j] = k;
                        }

                    }

                }

            }

            int k1 = K - 1;
            int n1 = N - 1;
            int[,] ranges = new int[N, 2];
            while (k1 >= 0)
            {
                ranges[k1, 0] = (int)(D[n1, k1] + 1);
                ranges[k1, 1] = n1;
                n1 = (int)D[n1, k1];
                k1--;
            }

            ranges[0, 0] = 0;
            float cellDistance = 5;
            float heightOffset = cellDistance, widthOffset;
            float frameWidth;
            for (int i = 0; i < K; i++)
            {
                float rowWidth = 0;
                frameWidth = frameSize.Width - ((ranges[i, 1] - ranges[i, 0]) + 2) * cellDistance;
                for (int j = ranges[i, 0]; j <= ranges[i, 1]; j++)
                {
                    rowWidth += newFrames[j].Size.Width;
                }

                float ratio = frameWidth / rowWidth;
                widthOffset = 0;
                for (int j = ranges[i, 0]; j <= ranges[i, 1]; j++)
                {
                    var newSize = new SizeF(newFrames[j].Size.Width * ratio, newFrames[j].Size.Height * ratio);
                    newFrames[j].Size = newSize;

                    newFrames[j].X = widthOffset + (j - ranges[i, 0] + 1) * cellDistance;
                    newFrames[j].Y = heightOffset;
                    widthOffset += newFrames[j].Size.Width;
                }

                heightOffset += newFrames[ranges[i, 0]].Size.Height + cellDistance;
            }

            for (int i = 0; i < N; i++)
            {
                UIImageView imgView = imageViews[i];
                imgView.AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight;
                imgView.ContentMode = UIViewContentMode.ScaleAspectFit;
                imgView.BackgroundColor = UIColor.FromRGBA(255, 255, 255, 170);
                imgView.Frame = newFrames[i];
                this.ContentView.AddSubview(imgView);
            }

            return new SizeF(frameSize.Width, heightOffset);
        }
    }
}

