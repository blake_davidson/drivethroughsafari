using System;
using System.Linq.Expressions;
using Cirrious.MvvmCross.Binding.BindingContext;
using CrossUI.Touch.Dialog.Elements;
using DriveThroughSafari.Core.ViewModels;
using MonoTouch.UIKit;
using System.Drawing;
using MonoTouch.CoreGraphics;
using Cirrious.MvvmCross.Binding.Touch.Views;

namespace DriveThroughSafari.Touch.Views
{
    public class MenuView : DialogViewControllerBase
    {
        public MenuView()
            : base(UITableViewStyle.Plain)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            var frame = View.Frame;
            frame.Width = 290;
            View.Frame = frame;
            TableView.BackgroundView = null;
            TableView.BackgroundColor = UIColor.Clear;
            TableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            TableView.ScrollEnabled = true;
            View.BackgroundColor = UIColor.FromPatternImage(AppStyles.GetImage("Images/WWDTS_MenuBackground"));
            TableView.RowHeight = TableView.RowHeight + 20;

            var binder = this.CreateBindingSet<MenuView, MenuViewModel>();

            Func<string, string, Expression<Func<MenuViewModel, object>>, SafariStyledStringElement> createElement = (text, imageFileName, property) =>
            {
                var element = new SafariStyledStringElement(text)
                {
                    Accessory = UITableViewCellAccessory.DisclosureIndicator,
                    ShouldDeselectAfterTouch = true,
                    Image = UIImage.FromBundle(imageFileName),
                    TextColor = UIColor.White,
                    BackgroundColor = UIColor.Clear,
                    Font = AppStyles.Langdon,
                    ShadowColor = UIColor.Black,
                    ShadowOffset = new SizeF(2, 2)
                    
                };

                binder.Bind(element)
                      .For(el => el.SelectedCommand)
                      .To(property);

                return element;
            };

            Root = new RootElement("")
            {
                new Section
                {
                    createElement("ANIMALS", "Images/Tabs/animals", vm => vm.ShowAnimals),
                    createElement("TOUR", "Images/Tabs/tour", vm => vm.ShowTour),
                    createElement("PHOTO BOOTH", "Images/Tabs/pictures", vm => vm.ShowPhotoBooth),
                    createElement("MEDIA GALLERY", "Images/Tabs/gallery", vm => vm.ShowPhotoGallery),
                }
            };

            var headerView = new UIView(new RectangleF(0, 0, View.Frame.Width, 120));

            TableView.TableHeaderView = headerView;

            var footer = new FooterView(new RectangleF(0, UIScreen.MainScreen.Bounds.Height - 75, View.Frame.Width, 75));

            Add(footer);

            binder.Bind(footer.BtnFacebook).To(vm => vm.GoToFacebook);
            binder.Bind(footer.BtnMail).To(vm => vm.SendEmail);
            binder.Bind(footer.BtnPhone).To(vm => vm.MakePhoneCall);
            binder.Bind(footer.BtnTripAdvisor).To(vm => vm.GoToTripAdvisor);
            binder.Apply();
        }


        public override UIView GetViewForFooter(UITableView tableView, int section)
        {
            return base.GetViewForFooter(tableView, section);
        }

        private new MenuViewModel ViewModel
        {
            get { return (MenuViewModel)base.ViewModel; }
        }
			
    }

    public class FooterView : UIView
    {
		
        public UIButton BtnPhone { get; set; }

        public UIButton BtnMail { get; set; }

        public UIButton BtnFacebook { get; set; }

        public UIButton BtnTripAdvisor { get; set; }

        public FooterView(RectangleF frame)
            : base(frame)
        {
            InitButtons();
        }

        private void InitButtons()
        {
            BtnPhone = new UIButton(new RectangleF(40, 0, 50, 50));
            BtnPhone.SetImage(UIImage.FromBundle("Images/Tabs/phone"), UIControlState.Normal);
            Add(BtnPhone);

            BtnMail = new UIButton(new RectangleF(95, 0, 50, 50));
            BtnMail.SetImage(UIImage.FromBundle("Images/Tabs/mail"), UIControlState.Normal);
            Add(BtnMail);

            BtnFacebook = new UIButton(new RectangleF(150, 0, 50, 50));
            BtnFacebook.SetImage(UIImage.FromBundle("Images/Tabs/facebook"), UIControlState.Normal);
            Add(BtnFacebook);

            BtnTripAdvisor = new UIButton(new RectangleF(205, 0, 50, 50));
            BtnTripAdvisor.SetImage(UIImage.FromBundle("Images/Tabs/tripadvisor"), UIControlState.Normal);
            Add(BtnTripAdvisor);
        }
    }

    public class MenuBackgroundView : UIView
    {
        public MenuBackgroundView(RectangleF frame)
            : base(frame)
        {
        }

        public override void Draw(RectangleF rect)
        {
            base.Draw(rect);

            var colorSpace = CGColorSpace.CreateDeviceRGB();
            var context = UIGraphics.GetCurrentContext();


            UIColor fillColor = UIColor.FromRGBA(0.537f, 0.000f, 0.000f, 1.000f);
            UIColor gradientColor = UIColor.FromRGBA(0.833f, 0.008f, 0.008f, 1.000f);
            UIColor strokeColor = UIColor.FromRGBA(0.000f, 0.000f, 0.000f, 1.000f);

            var gradientColors = new CGColor [] { fillColor.CGColor, UIColor.FromRGBA(0.685f, 0.004f, 0.004f, 1.000f).CGColor, gradientColor.CGColor };
            var gradientLocations = new float [] { 0, 0.81f, 1 };
            var gradient = new CGGradient(colorSpace, gradientColors, gradientLocations);

            var bgRectPath = UIBezierPath.FromRect(new RectangleF(1.5f, -0.5f, 200, 480));
            context.SaveState();
            bgRectPath.AddClip();
            context.DrawLinearGradient(gradient, new PointF(101.5f, -0.5f), new PointF(101.5f, 479.5f), 0);
            context.RestoreState();
            strokeColor.SetStroke();
            bgRectPath.LineWidth = 1;
            bgRectPath.Stroke();
        }
    }
}