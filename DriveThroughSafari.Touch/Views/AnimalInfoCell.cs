﻿using System;
using MonoTouch.UIKit;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.MapKit;
using DriveThroughSafari.Core.Models;
using System.Collections.Generic;
using System.Linq;
using MonoTouch.CoreLocation;
using SDWebImage;

namespace DriveThroughSafari.Touch
{
    public sealed class AnimalInfoCell : UITableViewCell
    {
        private UILabel _label;
        public MKMapView Map;
        public string Id = "CustomMapAnnotation";

        public AnimalInfoCell(string cellName)
            : base(UITableViewCellStyle.Default, cellName)
        {

        }

        public void Init(List<MapMarker> markers)
        {
            const double MAP_PADDING = 1.1;
            const double MINIMUM_VISIBLE_LATITUDE = 0.01;

            BackgroundView = null;
            BackgroundColor = UIColor.Clear;

            Map = new MKMapView
            {
                MapType = MKMapType.Standard,
                ShowsUserLocation = true,
                ZoomEnabled = true,
                ScrollEnabled = true,
                Frame = new RectangleF(0, 0, 320, 260)
            };

            Map.GetViewForAnnotation += GetViewForAnnotation;
                      
            var lats = new List<Double>();
            var lngs = new List<Double>();

            foreach (var mapMarker in markers.Select(marker => new BasicMapAnnotation(new CLLocationCoordinate2D(marker.Location.Lat, marker.Location.Lng), marker.Title, marker.Message, marker.Animal)))
            {
                lats.Add(mapMarker.Coordinate.Latitude);
                lngs.Add(mapMarker.Coordinate.Longitude);
     
                Map.AddAnnotation(mapMarker);
            }

            MKCoordinateRegion region;
            var minLatitude = lats.Min();
            var minLongitude = lngs.Min();
            var maxLatitude = lats.Max();
            var maxLongitude = lngs.Max();

            region.Center.Latitude = (minLatitude + maxLatitude) / 2;
            region.Center.Longitude = (minLongitude + maxLongitude) / 2;

            region.Span = new MKCoordinateSpan();

            region.Span.LatitudeDelta = (maxLatitude - minLatitude) * MAP_PADDING;

            region.Span.LatitudeDelta = (region.Span.LatitudeDelta < MINIMUM_VISIBLE_LATITUDE)
                ? MINIMUM_VISIBLE_LATITUDE 
                : region.Span.LatitudeDelta;

            region.Span.LongitudeDelta = (maxLongitude - minLongitude) * MAP_PADDING;

            MKCoordinateRegion scaledRegion = Map.RegionThatFits(region);
            Map.SetRegion(scaledRegion, false);

            ContentView.AddSubview(Map);
            this.Frame = Map.Frame;

        }

        MKAnnotationView GetViewForAnnotation(MKMapView mapView, NSObject annotation)
        {
            if (annotation is MKUserLocation)
                return null; 

            var view = mapView.DequeueReusableAnnotation(Id);
            if (view == null)
            {
                view = new MKAnnotationView(annotation, Id);
            }
            else
            {
                view.Annotation = annotation;
            }

            view.CanShowCallout = true;
            // (view as MKAnnotationView).AnimatesDrop = true;

            var backgroungImage = new UIImageView(UIImage.FromBundle("Images/SafariMapPin"));

            var circleImage = new UIImageView(new RectangleF(9, 6, backgroungImage.Frame.Size.Width / 2, backgroungImage.Frame.Size.Height / 2));
            circleImage.SetImage(NSUrl.FromString((annotation as BasicMapAnnotation).Animal.ImageUrl));
            circleImage.CreateCircleImage(1);
           
            var v = new UIView(backgroungImage.Frame);
            v.AddSubview(backgroungImage);
            v.AddSubview(circleImage);

            UIGraphics.BeginImageContext(v.Frame.Size);
            var ctx = UIGraphics.GetCurrentContext();
            if (ctx != null)
            {
                v.Layer.RenderInContext(ctx);
                view.Image = UIGraphics.GetImageFromCurrentImageContext();
                UIGraphics.EndImageContext();
            }
                
            return view;
        }

        public void Init(string label, string description)
        {
            Map = null;

            var boldAttr = new UIStringAttributes();
            boldAttr.Font = AppStyles.Langdon;

            var otherAttr = new UIStringAttributes();
            otherAttr.Font = AppStyles.Fabrica;

            var paragraphStyle1 = new NSMutableParagraphStyle();
            paragraphStyle1.LineSpacing = (float)3.5;

            var paragraphStyle2 = new NSMutableParagraphStyle();
            paragraphStyle2.LineSpacing = (float)3.5;

            otherAttr.ParagraphStyle = paragraphStyle1;
            boldAttr.ParagraphStyle = paragraphStyle2;

            _label = new UILabel(new RectangleF(30, 0, 260, 100));
            
            _label.Lines = 0;
            _label.LineBreakMode = UILineBreakMode.WordWrap;
            _label.TextColor = UIColor.DarkGray;
            
            BackgroundView = null;
            BackgroundColor = UIColor.Clear;
            SelectionStyle = UITableViewCellSelectionStyle.None;

            var descString = new NSMutableAttributedString(string.Format("{0}: {1}", label, description));
            
            var len = label.Length + 1;
            
            descString.SetAttributes(boldAttr.Dictionary, new NSRange(0, len));
            descString.SetAttributes(otherAttr.Dictionary, new NSRange(len, descString.Length - len));
            // descString.AddAttribute( , new NSRange(0, label.Length + descString.Length));
            
            _label.AttributedText = descString;
            ContentView.AddSubview(_label);
            
            _label.SizeToFit();
            
            this.Frame = _label.Frame;
        }

        public float GetHeight()
        {
            if (Map != null)
            {
                return 260;
            }

            if (_label.Bounds.Height > 40)
            {
                return _label.Frame.Height + 30;
            }

            return _label.Frame.Height + 18;
        }
            
    }
}