﻿using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Cirrious.MvvmCross.Touch.Views;
using Cirrious.MvvmCross.Binding.Touch.Views;
using Cirrious.MvvmCross.Binding.BindingContext;
using DriveThroughSafari.Core.ViewModels;
using System.Collections.Generic;
using DriveThroughSafari.Core.Models;
using System.Linq;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Touch.Views.Presenters;
using Mono;

namespace DriveThroughSafari.Touch
{
    public partial class AnimalsView : MvxTableViewController
    {

        private UIButton _backButton;

        public AnimalsView()
        {
        }

        public new AnimalsViewModel ViewModel
        {
            get { return (AnimalsViewModel)base.ViewModel; }
            set { base.ViewModel = value; }
        }

        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();
            
            // Release any cached data, images, etc that aren't in use.
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            //NavigationController.NavigationBar.SetBackgroundImage(UIImage.FromBundle("Images/Navigation/TopBarAnimals"), UIBarMetrics.Default);
            
            // Perform any additional setup after loading the view, typically from a nib.
            var source = new AnimalsTableSource(TableView);
            var binder = this.CreateBindingSet<AnimalsView, AnimalsViewModel>();
            var refreshControl = new MvxUIRefreshControl();
            refreshControl.TintColor = AppStyles.TintColor;

            this.RefreshControl = refreshControl;

            binder.Bind(source).To(vm => vm.GroupedList);
            binder.Bind(refreshControl).For(r => r.IsRefreshing).To(vm => vm.IsBusy);
            binder.Bind(refreshControl).For(r => r.RefreshCommand).To(vm => vm.ReloadCommand);
            binder.Bind(refreshControl).For(r => r.Message).To(vm => vm.BusyMessage);
            binder.Bind(source).For(x => x.SelectionChangedCommand).To(vm => vm.ItemSelectedCommand);
            binder.Apply();
            TableView.Source = source;
            this.NavigationItem.SetRightBarButtonItem(new UIBarButtonItem("Sort", UIBarButtonItemStyle.Plain, (object sender, EventArgs e) =>
                    {
                        var presenter = (SafariPresenter)Mvx.Resolve<IMvxTouchViewPresenter>();
                        presenter.ShowSortPanel();
                    }), false);

            CreateBackButton();
            NavigationItem.LeftBarButtonItem = new UIBarButtonItem(_backButton);
            NavigationController.NavigationBar.TintColor = AppStyles.TintColor;

            TableView.BackgroundColor = UIColor.Clear;
            TableView.BackgroundView = null;
            View.BackgroundColor = UIColor.FromPatternImage(AppStyles.GetImage(AppStyles.ZebraBackground));

            TableView.ReloadData();
        }

        public void CreateBackButton()
        {
            var image = UIImage.FromBundle(AppStyles.BackButtonPath);
            _backButton = UIButton.FromType(UIButtonType.Custom);
            _backButton.SetBackgroundImage(image, UIControlState.Normal);
            _backButton.Frame = new RectangleF(0, 0, image.Size.Width, image.Size.Height);
            _backButton.TouchUpInside += (object sender, EventArgs e) => ViewModel.GoHome.Execute(null);

        }
    }

    public  class AnimalsTableSource : MvxTableViewSource
    {

        public AnimalsTableSource(UITableView tableView)
            : base(tableView)
        {
            tableView.RegisterNibForCellReuse(AnimalCell.Nib, AnimalCell.Key);
        }

        private Dictionary<String, List<Animal>> _itemSource;

        public override System.Collections.IEnumerable ItemsSource
        {
            get
            {
                return _itemSource;
            }
            set
            {
                base.ItemsSource = value;
                _itemSource = (Dictionary<string, List<Animal>>)value;
                ReloadTableData();
            }
        }

        public override string TitleForHeader(UITableView tableView, int section)
        {
            if (_itemSource == null)
                return string.Empty;

            return _itemSource.Keys.ElementAt(section);
        }

        public override int NumberOfSections(UITableView tableView)
        {
            return _itemSource.Keys.Count;
        }

        public override int RowsInSection(UITableView tableview, int section)
        {
            var key = _itemSource.Keys.ElementAt(section);
            return _itemSource[key].Count;
        }

        public override float GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            return 200f;
        }

        #region implemented abstract members of MvxBaseTableViewSource

        protected override UITableViewCell GetOrCreateCellFor(UITableView tableView, NSIndexPath indexPath, object item)
        {
            // Reuse a cell if one exists
            var cell = tableView.DequeueReusableCell(AnimalCell.Key) as AnimalCell ?? AnimalCell.Create();
            var left = true;

            if (indexPath.Section == 0)
            {
                if (indexPath.Row % 2 != 0)
                {
                    left = false;
                }

            }
            else
            {
                var cnt = indexPath.Row;
                var key = _itemSource.Keys.ElementAt(indexPath.Section - 1);
                if (_itemSource[key].Count % 2 != 0)
                {
                    cnt += 1;
                }

                if (cnt % 2 != 0)
                {
                    left = false;
                }
            }

            cell.UpdateCell(left);
                    
            return cell;

        }

        #endregion

        protected override object GetItemAt(NSIndexPath indexPath)
        {
            if (_itemSource == null || _itemSource.Count == 0)
                return null;

            var key = _itemSource.Keys.ElementAt(indexPath.Section);
            var item = _itemSource[key][indexPath.Row];
            item.Name = item.Name.ToUpper();

            return item;
        }
            
    }
}

