using MonoTouch.UIKit;
using Cirrious.MvvmCross.Dialog.Touch;

namespace DriveThroughSafari.Touch.Views
{
    public abstract class DialogViewControllerBase : MvxDialogViewController
    {
        protected DialogViewControllerBase(UITableViewStyle style)
            : base(style, pushing: true)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            TableView.TableFooterView = GetFooterView();
            TableView.BackgroundView = null;
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            this.OnViewWillAppear(ViewModel, OnLoadingComplete);
        }

        protected virtual void OnLoadingComplete() { }

        protected virtual UIView GetFooterView()
        {
            return new UIView();
        }
    }
}