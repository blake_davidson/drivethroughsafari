﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Cirrious.MvvmCross.Binding.Touch.Views;
using Cirrious.MvvmCross.Binding.BindingContext;
using DriveThroughSafari.Core.Models;
using MonoTouch.CoreGraphics;
using SDWebImage;

namespace DriveThroughSafari.Touch
{
    public partial class AnimalCell : MvxTableViewCell
    {
        public static readonly UINib Nib = UINib.FromName("AnimalCell", NSBundle.MainBundle);
        public static readonly NSString Key = new NSString("AnimalCell");
        private Animal _animal;

        public AnimalCell(IntPtr handle)
            : base(handle)
        {
            this.DelayBind(() =>
                {
                    var set = this.CreateBindingSet<AnimalCell, Animal>();
                    set.Bind(lblName).To(item => item.Name);
                    //set.Bind(imgMain).For(i => i.Image).To(vm => vm.ImageData).WithConversion("InMemoryImage");
                    set.Bind(imgMain).For("SDWebImage").To(vm => vm.ImageUrl);
                    set.Apply();

                    lblName.Font = AppStyles.Langdon;
                    lblName.TextColor = UIColor.White;
                    lblName.BackgroundColor = UIColor.Clear;
                    lblName.ShadowColor = UIColor.Black;
                    lblName.ShadowOffset = new SizeF(2, 2);
                    //lblName.Layer.MasksToBounds = false;
                    //   var bg = new LabelBackground(lblName.Frame);
                    // AddSubview(bg);
                    BringSubviewToFront(lblName);
                    // SendSubviewToBack(bg);
                });


        }

        public static AnimalCell Create()
        {
            var cell = (AnimalCell)Nib.Instantiate(null, null)[0];

            return cell;
        }

        public void UpdateCell(bool left)
        {
            if (left)
            {
                lblName.TextAlignment = UITextAlignment.Left;
            }
            else
            {
                lblName.TextAlignment = UITextAlignment.Right;
            }
        }
    }

    public sealed class LabelBackground : UIView
    {
        public LabelBackground(RectangleF frame)
            : base(frame)
        {
            BackgroundColor = UIColor.Clear;
        }

        public override void Draw(RectangleF rect)
        {
            //// General Declarations
            var colorSpace = CGColorSpace.CreateDeviceRGB();
            var context = UIGraphics.GetCurrentContext();

            //// Color Declarations
            UIColor gradientColor = UIColor.FromRGBA(0.000f, 0.000f, 0.000f, 1.000f);
            UIColor gradientColor2 = UIColor.FromRGBA(0.000f, 0.000f, 0.000f, 0.296f);

            //// Gradient Declarations
            var gradientColors = new CGColor [] { gradientColor.CGColor, UIColor.FromRGBA(0.000f, 0.000f, 0.000f, 0.648f).CGColor, gradientColor2.CGColor };
            var gradientLocations = new float [] { 0, 0.52f, 1 };
            var gradient = new CGGradient(colorSpace, gradientColors, gradientLocations);

            //// Rectangle Drawing
            var rectanglePath = UIBezierPath.FromRect(new RectangleF(0, rect.Bottom - 46.5f, 320, 46.5f));
            context.SaveState();
            rectanglePath.AddClip();
            context.DrawLinearGradient(gradient, new PointF(160, 46.5f), new PointF(160, -0), 0);
            context.RestoreState();


        }
    }
}