﻿using System.Drawing;
using Cirrious.MvvmCross.Binding.Touch.Views;
using DriveThroughSafari.Touch.Views.PhotoBooth;
using MonoTouch.UIKit;

namespace DriveThroughSafari.Touch.Controls
{
    public sealed class SingleRowCollectionView : MvxView
    {
        public UICollectionView Animals;
        public MvxCollectionViewSource Source;

        public SingleRowCollectionView(RectangleF bounds)
            :base(bounds)
        {
            var layout = new UICollectionViewFlowLayout();
            BackgroundColor = UIColor.Black;
            layout.ScrollDirection = UICollectionViewScrollDirection.Horizontal;
            layout.ItemSize = new SizeF(100, 100);
            Animals = new UICollectionView(bounds, layout) {BackgroundColor = UIColor.White, Alpha = .75f};
            Alpha = .75f;
            Animals.RegisterNibForCell(AnimalCutoutCell.Nib, AnimalCutoutCell.Key);
            Source = new MvxCollectionViewSource(Animals, AnimalCutoutCell.Key);

            Animals.Source = Source;
        }

    }
}
