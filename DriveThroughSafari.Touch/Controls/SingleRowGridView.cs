using System.Collections.Generic;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace DriveThroughSafari.Touch.Controls
{
    [Register("SingleRowGridView")]
    public class SingleRowGridView : UIView
    {
       
        private readonly List<SingleRowData> _data;

        public SingleRowGridView()
        {
            Initialize();
        }

        public SingleRowGridView(RectangleF bounds)
            : base(bounds)
        {
            Initialize();
        }

        public SingleRowGridView(RectangleF bounds, List<SingleRowData> data)
            : base(bounds)
        {
            _data = data;
        }

        void Initialize()
        {

            this.BackgroundColor = UIColor.Black;

        }
    }

    [Register("SingleRowData")]
    public class SingleRowData : NSObject
    {

        public SingleRowData(string image)
        {
            Image = image;
        }

        public SingleRowData()
        {
            // TODO: Complete member initialization
        }

        [Export("Image")]
        public string Image { get; set; }
    }
}