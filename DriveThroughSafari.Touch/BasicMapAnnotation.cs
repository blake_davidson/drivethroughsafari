﻿using MonoTouch.CoreLocation;
using MonoTouch.MapKit;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using DriveThroughSafari.Core.Models;

namespace DriveThroughSafari.Touch
{
    public class BasicMapAnnotation : MKAnnotation
    {
        public override sealed CLLocationCoordinate2D Coordinate { get; set; }

        readonly string _title;
        readonly string _subtitle;
        readonly Animal _animal;

        public override string Title { get { return _title; } }

        public override string Subtitle { get { return _subtitle; } }

        public Animal Animal { get { return _animal; } }

        public BasicMapAnnotation(CLLocationCoordinate2D coordinate, string title, string subtitle, Animal animal)
        {
            Coordinate = coordinate;
            _title = title;
            _subtitle = subtitle;
            _animal = animal;
        }

    }
}
