﻿using System;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Touch.Views;
using Cirrious.MvvmCross.Touch.Views.Presenters;
using Cirrious.MvvmCross.ViewModels;
using DriveThroughSafari.Core;
using DriveThroughSafari.Core.ViewModels;
using MonoTouch.UIKit;
using SlidingPanels.Lib;
using SlidingPanels.Lib.PanelContainers;
using DriveThroughSafari.Touch.Views;
using System.Drawing;

namespace DriveThroughSafari.Touch
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class DisableMenuGestureAttribute : Attribute
    {
    }

    public class SafariPresenter : MvxTouchViewPresenter
    {
        private readonly UIWindow _window;
        private LeftPanelContainer _menuPanelContainer;
        private RightPanelContainer _sortAnimalsContainer;
        private UIViewController _rootViewController;
        private SlidingPanelsNavigationViewController _slidingPanelsController;
        private IMvxTouchViewCreator _viewCreator;
        private UINavigationController _navController;

        public UINavigationController NavController { get { return _navController; } }

        protected IMvxTouchViewCreator ViewCreator
        {
            get { return _viewCreator ?? (_viewCreator = Mvx.Resolve<IMvxTouchViewCreator>()); }
        }

        public SafariPresenter(UIApplicationDelegate applicationDelegate, UIWindow window)
            : base(applicationDelegate, window)
        {
            _window = window;
        }

        protected override void ShowFirstView(UIViewController viewController)
        {
            base.ShowFirstView(viewController);

            _slidingPanelsController = (SlidingPanelsNavigationViewController)MasterNavigationController;

            _rootViewController.AddChildViewController(_slidingPanelsController);
            _rootViewController.View.AddSubview(_slidingPanelsController.View);

            var menuRequest = new MvxViewModelRequest<MenuViewModel>(new MvxBundle(null), null, MvxRequestedBy.UserAction);
            var menuView = (UIViewController)ViewCreator.CreateView(menuRequest);
            _menuPanelContainer = new LeftPanelContainer(menuView) { EdgeTolerance = 100f };
            _slidingPanelsController.InsertPanel(_menuPanelContainer);

            var rightMenuRequest = new MvxViewModelRequest<GroupAnimalsViewModel>(new MvxBundle(null), null, MvxRequestedBy.UserAction);
            var rightMenuView = (UIViewController)ViewCreator.CreateView(rightMenuRequest);
            _sortAnimalsContainer = new RightPanelContainer(rightMenuView);
        }

        protected override UINavigationController CreateNavigationController(UIViewController viewController)
        {
            var navController = new SafariViewController(viewController);
            navController.CanSwipeToShowPanel = delegate(UITouch x)
            {
                var attributes = navController.TopViewController.GetType().GetCustomAttributes(false);
                var canSwipe = true;
                if (attributes.Length > 0)
                {
                    foreach (var attr in attributes)
                    {
                        if (attr.GetType() == typeof(DisableMenuGestureAttribute))
                        {
                            canSwipe = false;
                        }
                    }
                }
                return canSwipe;
            };

            navController.View.Layer.ShadowRadius = 0;

            _rootViewController = new UIViewController();

            _navController = navController;

            // _navController.NavigationBar.SetBackgroundImage(UIImage.FromBundle("Images/Navigation/TopBar"), UIBarMetrics.Default);
            //_navController.NavigationBar.ShadowImage = new UIImage();
            //_navController.NavigationBar.BackgroundColor = UIColor.Clear;
            _navController.NavigationBar.Translucent = true;
            //_navController.NavigationBar.SetBackgroundImage(new UIImage(), UIBarMetrics.Default);
            _navController.NavigationBar.BarStyle = UIBarStyle.Default;
            //_navController.NavigationBar.Subviews[0].Alpha = 0.7f;
            var img = new UIImageView(UIImage.FromBundle("Images/Navigation/TopBar"));
            _navController.NavigationBar.Layer.InsertSublayer(img.Layer, 2);
            _navController.SetNavigationBarHidden(true, false);

            return navController;
        }

        protected override void SetWindowRootViewController(UIViewController controller)
        {
            _window.AddSubview(_rootViewController.View);
            _window.RootViewController = _rootViewController;
        }

        public override void Show(MvxViewModelRequest request)
        {
            if (_menuPanelContainer != null && _menuPanelContainer.IsVisible)
                _slidingPanelsController.TogglePanel(PanelType.LeftPanel);

            if (request.ViewModelType == typeof(WelcomeViewModel) && _navController != null)
            {
                _navController.SetNavigationBarHidden(true, true);
            }
            else if (_navController != null)
            {
                _navController.SetNavigationBarHidden(false, false);
            }

            if (request.ViewModelType == typeof(AnimalsViewModel) && _navController != null)
            {
                _slidingPanelsController.InsertPanel(_sortAnimalsContainer);

            }
            else if (MasterNavigationController != null && MasterNavigationController.TopViewController.GetType() == typeof(AnimalsView))
            {
                _slidingPanelsController.RemovePanel(_sortAnimalsContainer);
            }

            if (request.PresentationValues != null)
            {
                if (request.PresentationValues.ContainsKey(PresentationBundleFlagKeys.ClearStack))
                {
                    ClearStackAndNavigate(request);

                    return;
                }
            }

            base.Show(request);
        }

        private void ClearStackAndNavigate(MvxViewModelRequest request)
        {
            var nextViewController = (UIViewController)ViewCreator.CreateView(request);

            if (MasterNavigationController.TopViewController.GetType() != nextViewController.GetType())
            {
                MasterNavigationController.PopToRootViewController(false);
                MasterNavigationController.ViewControllers = new UIViewController[] { nextViewController };
            }
        }

        public void ToggleNavigation()
        {
            _slidingPanelsController.TogglePanel(PanelType.LeftPanel);
        }

        public void ShowSortPanel()
        {
            _slidingPanelsController.TogglePanel(PanelType.RightPanel);
        }

        public void HideSortPanel()
        {
            _slidingPanelsController.TogglePanel(PanelType.RightPanel);
        }
    }

    public class SafariViewController
        : SlidingPanelsNavigationViewController
    {

        public SafariViewController(UIViewController viewController)
            : base(viewController)
        {

        }

        public override bool ShouldAutorotate()
        {
            return TopViewController.ShouldAutorotate();
        }

        public override UIInterfaceOrientationMask GetSupportedInterfaceOrientations()
        {
            return TopViewController.GetSupportedInterfaceOrientations();
        }

    }
}

