using System.Collections.Generic;
using System.Reflection;
using Cirrious.MvvmCross.Binding.Bindings.Target.Construction;
using Cirrious.MvvmCross.Localization;
using Cirrious.MvvmCross.Touch.Platform;
using Cirrious.MvvmCross.Touch.Views.Presenters;
using Cirrious.MvvmCross.ViewModels;
using DriveThroughSafari.Core;
using DriveThroughSafari.Touch.Controls;
using DriveThroughSafari.Touch.Views;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace DriveThroughSafari.Touch
{
    public class Setup
        : MvxTouchSetup
    {
        public Setup(MvxApplicationDelegate applicationDelegate, IMvxTouchViewPresenter presenter)
            : base(applicationDelegate, presenter)
        {
        }

        protected override List<Assembly> ValueConverterAssemblies
        {
            get
            {
                List<Assembly> toReturn = base.ValueConverterAssemblies;
                toReturn.Add(typeof(MvxLanguageConverter).Assembly);
                toReturn.Add(typeof(MvxInMemoryImageValueConverter).Assembly);
                return toReturn;
            }
        }

        protected override IMvxApplication CreateApp()
        {
            var app = new NoSplashScreenSafariApp();
            return app;
        }

        protected override void InitializeLastChance()
        {
            var errorDisplayer = new ErrorDisplayer();

            base.InitializeLastChance();
        }

        protected override void FillTargetFactories(IMvxTargetBindingFactoryRegistry registry)
        {
            registry.RegisterCustomBindingFactory<UIImageView>("SDWebImage", (source) => new SDTargetBinding(source));
            //  registry.RegisterCustomBindingFactory <SingleRowGridView>("SingleRowData", (source) => new SingleRowDataSourceTargetBinding(source));
            //  registry.RegisterCustomBindingFactory<AnimalsView>("AnimalsListData", (source) => new AnimalsListDataSourceTargetBinding(source));
            //  registry.RegisterCustomBindingFactory<MapView>("MapLocation", (source) => new MapCameraTargetBinding(source));
         
            base.FillTargetFactories(registry);
        }
    }
}