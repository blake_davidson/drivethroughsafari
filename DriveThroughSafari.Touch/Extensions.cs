﻿using System;
using System.Drawing;
using System.Linq.Expressions;
using System.Reflection;
using BigTed;
using Cirrious.MvvmCross.Binding.BindingContext;
using DriveThroughSafari.Core.ViewModels;
using MonoTouch.CoreGraphics;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using SlidingPanels.Lib;
using SlidingPanels.Lib.PanelContainers;
using SDWebImage;

namespace DriveThroughSafari.Touch
{
    public static class Extensions
    {
        public static void BindLoadingMessage<TViewModel>(this TViewModel viewModel, UIView view, Expression<Func<TViewModel, bool>> property, string message)
           where TViewModel : BaseViewModel
        {
            var expression = property.Body as MemberExpression;

            if (expression == null)
                return;

            var propertyInfo = expression.Member as PropertyInfo;

            if (propertyInfo == null)
                return;

            Action<bool> setMessageVisibility = visible =>
            {
                if (visible)
                {
                    BTProgressHUD.Show(message, maskType: ProgressHUD.MaskType.Black);
                }
                else
                {
                    BTProgressHUD.Dismiss();
                }
            };

            setMessageVisibility((bool)propertyInfo.GetValue(viewModel, null));

            viewModel.PropertyChanged += (sender, e) =>
            {
                if (e.PropertyName == propertyInfo.Name)
                    setMessageVisibility((bool)propertyInfo.GetValue(viewModel, null));
            };
        }

        public static void OnViewWillAppear<TController>(this TController controller, object viewModel, Action loadedCallback)
            where TController : UIViewController, IMvxBindingContextOwner
        {
            var appViewModel = (BaseViewModel)viewModel;

            controller.InvokeOnMainThread(() => loadedCallback());


            if (controller.NavigationController != null && controller.NavigationController.ViewControllers.Length == 1 && controller.NavigationItem.LeftBarButtonItem == null)
            {
                var menuButton = new UIButton(new RectangleF(0, 0, 40f, 40f));
                menuButton.SetBackgroundImage(UIImage.FromFile("SlideLeft40.png"), UIControlState.Normal);
                menuButton.TouchUpInside += delegate
                {
                    ((SlidingPanelsNavigationViewController)controller.NavigationController).TogglePanel(PanelType.LeftPanel);
                };

                controller.NavigationItem.LeftBarButtonItem = new UIBarButtonItem(menuButton);
            }
        }

        public static UIImage ImageFromUrl(this string url)
        {
            var nsUrl = new NSUrl(url);
            var data = NSData.FromUrl(nsUrl);
            return new UIImage(data);
        }


        public static void CreateCircleImage(this UIImageView image)
        {
            image.CreateCircleImage(2, 5);
        }

        public static void CreateCircleImage(this UIImageView image, float radius)
        {
            image.CreateCircleImage(radius, 5);
        }

        public static void CreateCircleImage(this UIImageView image, int borderWidth)
        {
            image.CreateCircleImage(2, borderWidth);
        }

        public static void CreateCircleImage(this UIImageView image, float radius, int borderWidth)
        {
            image.Layer.CornerRadius = image.Bounds.Width / radius;
            image.Layer.BorderWidth = borderWidth;
            image.Layer.BorderColor = UIColor.White.CGColor;
            image.Layer.MasksToBounds = true;
        }

        public static void CreateImageAndPlaceholder(this UIImageView view, string url)
        {
            var nsurl = NSUrl.FromString(url);
            const string monkeyImage = "/Images/Tabs/animals";
            if (nsurl == null)
            {
                view.Image = UIImage.FromBundle(monkeyImage);
            }
            else
            {
                view.SetImage(nsurl, UIImage.FromBundle(monkeyImage));
            }
        }
    }
}
