using Cirrious.CrossCore;
using Cirrious.MvvmCross.Touch.Platform;
using Cirrious.MvvmCross.ViewModels;
using MonoTouch.Foundation;
using MonoTouch.ObjCRuntime;
using MonoTouch.UIKit;
using Cirrious.MvvmCross.Touch.Views.Presenters;
using DriveThroughSafari.Touch.Views;
using MonoTouch.MediaPlayer;

namespace DriveThroughSafari.Touch
{
    [Register("AppDelegate")]
    public class AppDelegate
        : MvxApplicationDelegate
    {

        public static readonly NSString NotificationWillChangeStatusBarOrientation =
            new NSString("UIApplicationWillChangeStatusBarOrientationNotification");

        public static readonly NSString NotificationDidChangeStatusBarOrientation =
            new NSString("UIApplicationDidChangeStatusBarOrientationNotification");

        public static readonly NSString NotificationOrientationDidChange =
            new NSString("UIDeviceOrientationDidChangeNotification");

        private UIWindow _window;

        public static bool IsPhone
        {
            get { return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone; }
        }

        public static bool IsPad
        {
            get { return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad; }
        }

        public static bool HasRetina
        {
            get
            {
                if (UIScreen.MainScreen.RespondsToSelector(new Selector("scale")))
                    return (UIScreen.MainScreen.Scale == 2.0);
                else
                    return false;
            }
        }

        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            _window = new UIWindow(UIScreen.MainScreen.Bounds);


            var iRate = MTiRate.iRate.SharedInstance;
            iRate.AppStoreID = 551531422;
            
            var presenter = new SafariPresenter(this, _window);
            var setup = new Setup(this, presenter);
            setup.Initialize();

            // Mvx.RegisterSingleton<ITabBarPresenterHost>(presenter);
            // Mvx.RegisterSingleton<ISlidingMenuPresnterHost>(presenter);

            //start the app
            var startup = Mvx.Resolve<IMvxAppStart>();
            startup.Start();

            _window.MakeKeyAndVisible();

            UIApplication.SharedApplication.SetStatusBarHidden(true, false);

            return true;
        }

        public override void DidEnterBackground(UIApplication application)
        {
            var presenter = (SafariPresenter)Mvx.Resolve<IMvxTouchViewPresenter>();
            if (presenter != null)
            {
                if (presenter.NavController.TopViewController.GetType() == typeof(WelcomeView))
                {
                    ((WelcomeView)presenter.NavController.TopViewController).PauseVideoLoop();
                }
            }
        }

        public override UIInterfaceOrientationMask GetSupportedInterfaceOrientations(UIApplication application, UIWindow forWindow)
        {
            try
            {
                if (forWindow != null && forWindow.RootViewController != null && forWindow.RootViewController.PresentedViewController != null)
                {
                    if (forWindow.RootViewController.PresentedViewController is MPMoviePlayerViewController)
                    {
                        return UIInterfaceOrientationMask.AllButUpsideDown;
                    }
                }

            }
            catch (System.Exception e)
            {
            }

//            try
//            {
//                if (forWindow != null && forWindow.RootViewController != null && forWindow.RootViewController.ChildViewControllers != null)
//                {
//                    var presenter = (SafariPresenter)Mvx.Resolve<IMvxTouchViewPresenter>();
//                    if (presenter != null)
//                    {
//                        if (presenter.NavController.TopViewController.GetType() == typeof(MediaAlbumView))
//                        {
//                            return ((MediaAlbumView)presenter.NavController.TopViewController).IsFullScreen ? UIInterfaceOrientationMask.AllButUpsideDown : UIInterfaceOrientationMask.Portrait;
//                        }
//                    }
//                }
//            }
//            catch (System.Exception e)
//            {
//            }
//

            return UIInterfaceOrientationMask.Portrait;
        }


        public override void WillEnterForeground(UIApplication application)
        {
            var presenter = (SafariPresenter)Mvx.Resolve<IMvxTouchViewPresenter>();
            if (presenter != null)
            {
                if (presenter.NavController.TopViewController.GetType() == typeof(WelcomeView))
                {
                    ((WelcomeView)presenter.NavController.TopViewController).PlayVideoLoop();
                }
            }
        }
    }
}