﻿using BigTed;
using Cirrious.CrossCore;
using DriveThroughSafari.Core.Interfaces;
using MonoTouch.UIKit;

namespace DriveThroughSafari.Touch
{
    public class ErrorDisplayer
    {
        public ErrorDisplayer()
        {
            var source = Mvx.Resolve<IErrorSource>();
            source.ErrorReported += (sender, args) => ShowError(args.Message);
        }

        private void ShowError(string message)
        {
            UIApplication.SharedApplication.InvokeOnMainThread(
                () => BTProgressHUD.ShowToast(message, timeoutMs: 1500));
        }
    }
}