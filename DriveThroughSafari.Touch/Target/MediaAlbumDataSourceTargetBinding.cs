﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Cirrious.CrossCore.Platform;
using Cirrious.MvvmCross.Binding;
using Cirrious.MvvmCross.Binding.Bindings.Target;
using DriveThroughSafari.Core.Models;
using DriveThroughSafari.Touch.Views;
using MonoTouch.Foundation;

namespace DriveThroughSafari.Touch.Target
{
    public class MediaAlbumDataSourceTargetBinding : MvxTargetBinding
    {


        public override void SetValue(object value)
        {
            var view = View;
            if (view == null)
                return;
            var vmList = (List<MediaItem>)value;
            if (vmList != null)
            {
                var uiList = new List<PhotoInfo>();

                foreach (var mediaItem in vmList)
                {
                    var info = new PhotoInfo
                    {
                        ImagePath = mediaItem.ImageUrl,
                        ThumbPath = mediaItem.ThumbUrl,
                        IsVideo = mediaItem.IsVideo
                    };

                    uiList.Add(info);
                }

                View.CurrentAnimal = uiList[0];
                    
// ReSharper disable CoVariantArrayConversion
                View.PhotoDs.Data = uiList.ToArray();
// ReSharper restore CoVariantArrayConversion
                View.ThumbsDs.Data = uiList.ToArray();
// ReSharper disable CoVariantArrayConversion
                View.SingleRowThumbDs.Data = uiList.ToArray();
// ReSharper restore CoVariantArrayConversion

                View.PhotoGridView.ReloadData();
                View.ThumbsGridView.ReloadData();
            }

        }

        public override Type TargetType
        {
            get { return typeof(NSObject[]); }
        }

        public override MvxBindingMode DefaultMode
        {
            get { return MvxBindingMode.OneWay; }
        }
    }
}
