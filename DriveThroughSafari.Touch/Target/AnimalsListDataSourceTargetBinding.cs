//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using Cirrious.CrossCore.Platform;
//using Cirrious.MvvmCross.Binding;
//using Cirrious.MvvmCross.Binding.Bindings.Target;
//using DriveThroughSafari.Core.ViewModels;
//using DriveThroughSafari.Touch.Views;
//using MonoTouch.Foundation;
//using MonoTouch.UIKit;
//using DriveThroughSafari.Core.Models;
//
//namespace DriveThroughSafari.Touch.Target
//{
//    public class AnimalsListDataSourceTargetBinding : MvxTargetBinding
//    {
//        protected AnimalsView View
//        {
//            get { return base.Target as AnimalsView; }
//        }
//
//        public AnimalsListDataSourceTargetBinding(AnimalsView view)
//            : base(view)
//        {
//            if (view == null)
//            {
//                MvxBindingTrace.Trace(MvxTraceLevel.Error, "Error - GridViewSource is null in SingRowDataSourceTargetBinding");
//            }
//        }
//
//        public override void SetValue(object value)
//        {
//            var view = View;
//            if (view == null)
//                return;
//
//            var vmList = (List<Animal>) value;
//
//            if(vmList == null)
//                return;
//
//            var uiList = new List<Animal>();
//
//            foreach (var animal in vmList)
//            {
//                uiList.Add(new AnimalListItem {Category = animal.Category.ToString(), Name = animal.Name, ImageUrl = animal.ImageUrl, Animal = animal});
//            }
//
//            View.Ds.Data = uiList.ToArray();
//            View.GridView.ReloadData();
//        }
//
//        public override Type TargetType
//        {
//            get { return typeof(NSObject[]); }
//        }
//
//        public override MvxBindingMode DefaultMode
//        {
//            get { return MvxBindingMode.OneWay; }
//        }
//    }
//}