﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cirrious.MvvmCross.Binding;
using Cirrious.MvvmCross.Binding.Bindings.Target;
using DriveThroughSafari.Core.Models;
using Google.Maps;
using MonoTouch.CoreLocation;

namespace DriveThroughSafari.Touch.Target
{
    class MapCameraTargetBinding : MvxTargetBinding 
    {
        protected   MapView  View
        {
            get { return base.Target as MapView; }
        }
        public MapCameraTargetBinding(MapView target) : base(target)
        {
        }

        public override void SetValue(object value)
        {
            var loc = (GeoLocation) value;
            var camera = new CameraPosition(new CLLocationCoordinate2D(loc.Lat, loc.Lng), View.Camera.Zoom, View.Camera.Bearing, View.Camera.ViewingAngle);
            
            View.Animate(camera);
        }

        public override Type TargetType
        {
            get { return typeof(GeoLocation); }
        }

        public override MvxBindingMode DefaultMode
        {
            get { return MvxBindingMode.OneWay; }
        }
    }
}
