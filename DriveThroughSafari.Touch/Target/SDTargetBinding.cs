﻿using System;
using Cirrious.MvvmCross.Binding;
using Cirrious.MvvmCross.Binding.Bindings.Target;
using MonoTouch.UIKit;
using SDWebImage;
using MonoTouch.Foundation;
using DriveThroughSafari.Core.Models;

namespace DriveThroughSafari.Touch
{
    public class SDTargetBinding : MvxConvertingTargetBinding
    {
        protected UIImageView UIImageView
        {
            get { return (UIImageView)Target; }
        }

        public SDTargetBinding(UIImageView target)
            : base(target)
        {
        }

        protected override void SetValueImpl(object target, object value)
        {
            var view = (UIImageView)target;
            var imageUrl = (string)value;
            view.CreateImageAndPlaceholder(imageUrl);
        }

        #region implemented abstract members of MvxTargetBinding

        public override Type TargetType
        {
            get
            {
                return typeof(Animal);
            }
        }

        #endregion
    }
}