﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Cirrious.CrossCore.Platform;
using Cirrious.MvvmCross.Binding;
using Cirrious.MvvmCross.Binding.Bindings.Target;
using DriveThroughSafari.Core.ViewModels;
using DriveThroughSafari.Touch.Controls;
using MonoTouch.Foundation;
using DriveThroughSafari.Core;
using DriveThroughSafari.Core.Models;

namespace DriveThroughSafari.Touch.Target
{
    public class SingleRowDataSourceTargetBinding : MvxTargetBinding
    {
        protected SingleRowGridView View
        {
            get { return base.Target as SingleRowGridView; }
        }

        public SingleRowDataSourceTargetBinding(SingleRowGridView view)
            : base(view)
        {
            if (view == null)
            {
                MvxBindingTrace.Trace(MvxTraceLevel.Error, "Error - GridViewSource is null in SingRowDataSourceTargetBinding");
            }
        }

        public override void SetValue(object value)
        {
            var view = View;
            if (view == null)
                return;
            var animals = (List<Animal>)value;
            if (animals != null)
            {
                var list = new List<SingleRowData>();

                for (int i = 0; i < animals.Count; i++)
                {
                    animals[i].ImageUrl = string.Format("http://placepuppy.it/200/22{0}", i.ToString(CultureInfo.InvariantCulture));
                    list.Add(new SingleRowData(animals[i].ImageUrl));
                }

            }
            
        }

        public override Type TargetType
        {
            get { return typeof(NSObject[]); }
        }

        public override MvxBindingMode DefaultMode
        {
            get { return  MvxBindingMode.OneWay; }
        }
    }
}
