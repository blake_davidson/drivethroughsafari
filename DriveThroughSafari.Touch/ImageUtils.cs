﻿using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Drawing;
using MonoTouch.CoreGraphics;

namespace DriveThroughSafari.Touch
{
	public static class Image
	{
	
		/// <summary>
		/// Resize the image to be contained within a maximum width and height, keeping aspect ratio
		/// </summary>
		/// <returns>The resize image.</returns>
		/// <param name="sourceImage">Source image.</param>
		/// <param name="maxWidth">Max width.</param>
		/// <param name="maxHeight">Max height.</param>
		public static UIImage MaxResizeImage(UIImage sourceImage, float maxWidth, float maxHeight)
		{
			var sourceSize = sourceImage.Size;
			var maxResizeFactor = Math.Max(maxWidth / sourceSize.Width, maxHeight / sourceSize.Height);
			if (maxResizeFactor > 1) return sourceImage;
			var width = maxResizeFactor * sourceSize.Width;
			var height = maxResizeFactor * sourceSize.Height;
			UIGraphics.BeginImageContext(new SizeF(width, height));
			sourceImage.Draw(new RectangleF(0, 0, width, height));
			var resultImage = UIGraphics.GetImageFromCurrentImageContext();
			UIGraphics.EndImageContext();
			return resultImage;
		}

		/// <summary>
		/// Takes two images and combines them into one image with the second image placed to the right of the first.
		/// </summary>
		/// <param name="leftImage">Image that will appear on the left</param>
		/// <param name="rightImage">Image that will appear on the right</param>
		/// <returns>Merged image</returns>
		public static UIImage MergeImages(UIImage leftImage, UIImage rightImage)
		{
			var lSize = leftImage.Size;
			var rSize = rightImage.Size;
			var width = lSize.Width + rSize.Width;
			var height = Math.Max(lSize.Height, rSize.Height);
			var lRect = new RectangleF(new Point(0, 0), lSize);
			var rRect = new RectangleF(new Point((int)lSize.Width, 0), rSize);

			var image = PerformContextOperations(width, height, (context) =>
				{
					context.DrawImage(lRect, leftImage.CGImage);
					context.DrawImage(rRect, rightImage.CGImage);
				});

			return image;
		}

		/// <summary>
		/// Takes two images and draws the first image over the second, allowing the bottom image to show through transparent areas in the first
		/// </summary>
		/// <param name="topImage">Image to be drawn on top</param>
		/// <param name="bottomImage">Image to drawn on bottom</param>
		/// <returns>Image resulting from the superimpose operation</returns>
		public static UIImage SuperimposeImage(UIImage topImage, UIImage bottomImage)
		{
			var scaleFactor = ScaleFactor;
			var tSize = topImage.Size;
			var bSize = bottomImage.Size;
			var width = Math.Max(tSize.Width, bSize.Width);
			var height = Math.Max(tSize.Height, bSize.Height);
			var tRect = new RectangleF(new Point(0, 0), tSize);
			var bRect = new RectangleF(new Point(0, 0), bSize);

			var image = PerformContextOperations(width, height, (context) =>
				{
					context.DrawImage(bRect, bottomImage.CGImage);
					context.DrawImage(tRect, topImage.CGImage);
				});

			return image;
		}

		/// <summary>
		/// Repeat an image horizontally.
		/// </summary>
		/// <param name="image">Image to repeat</param>
		/// <param name="count">the number of times to perform the repeat operation (result will contain count + 1 images) </param>
		/// <returns></returns>
		public static UIImage RepeatImage(UIImage image, int count = 1)
		{
			var repeatedImage = image;
			for (int i = 0; i < count; i++)
			{
				repeatedImage = MergeImages(repeatedImage, image);
			}
			return repeatedImage;
		}
			
		/// <summary>
		/// Resizes the image.
		/// </summary>
		/// <returns>The image.</returns>
		/// <param name="sourceImage">Source image.</param>
		/// <param name="width">Width.</param>
		/// <param name="height">Height.</param>
		public static UIImage ResizeImage(UIImage sourceImage, float width, float height)
		{
			UIGraphics.BeginImageContext(new SizeF(width, height));
			sourceImage.Draw(new RectangleF(0, 0, width, height));
			var resultImage = UIGraphics.GetImageFromCurrentImageContext();
			UIGraphics.EndImageContext();
			return resultImage;
		}

		/// <summary>
		/// Gets the main screen scale factor (for Retina support).
		/// Value is cached for use on background threads, as UIScreen.MainScreen.Scale must be peformed on the main thread. If the property is accessed for the first
		/// time on the background thread, it will call on the main thread and block the background thread until the value is populated.
		/// </summary>
		public static float ScaleFactor
		{
			get
			{
				if (_scaleFactor > 0.0f)
					return _scaleFactor;

				if (NSRunLoop.Current == NSRunLoop.Main)
				{
					_scaleFactor = UIScreen.MainScreen.Scale;
					return _scaleFactor;
				}
				else
				{
					NSRunLoop.Main.InvokeOnMainThread(() =>
						{
							if (_scaleFactor == 0.0f)
							{
								var x = ScaleFactor;
							}
						});

					while (_scaleFactor == 0.0f)
						System.Threading.Thread.Sleep(50);

					return _scaleFactor;
				}
			}
		}
		/// <summary>
		/// Backing field for ScaleFactor property
		/// </summary>
		private static float _scaleFactor = 0.0f;

		/// <summary>
		/// Performs a delegate-supplied set of operations on a CGBitmapContext
		/// </summary>
		/// <param name="canvasWidth">Width of context in display-scaled units (scaling will be applied to go from display units to pixels for Retina support)</param>
		/// <param name="canvasHeight">Height of context in display-scaled units (scaling will be applied to go from display units to pixels for Retina support)</param>
		/// <param name="operations">delegate that will perform context operations</param>
		/// <returns>UIImage containing result of context operations</returns>
		public static UIImage PerformContextOperations(float canvasWidth, float canvasHeight, Action<CGBitmapContext> operations)
		{
			var scaleFactor = ScaleFactor;
			var contextWidth = (int)(canvasWidth * scaleFactor);
			var contextHeight = (int)(canvasHeight * scaleFactor);

			var colorSpace = MonoTouch.CoreGraphics.CGColorSpace.CreateDeviceRGB();
			var context = new CGBitmapContext(null, contextWidth, contextHeight, 8, 0, colorSpace, CGBitmapFlags.PremultipliedFirst);
			context.ScaleCTM(scaleFactor, scaleFactor);

			operations(context);

			var image = new UIImage(context.ToImage(), scaleFactor, UIImageOrientation.Up);

			context.Dispose();
			colorSpace.Dispose();

			return image;
		}

		public static UIImage ScaleImage(UIImage image, int maxSize)
		{

			UIImage res;

			using (CGImage imageRef = image.CGImage)
			{
				CGImageAlphaInfo alphaInfo = imageRef.AlphaInfo;
				CGColorSpace colorSpaceInfo = CGColorSpace.CreateDeviceRGB();
				if (alphaInfo == CGImageAlphaInfo.None)
				{
					alphaInfo = CGImageAlphaInfo.NoneSkipLast;
				}

				int width, height;

				width = imageRef.Width;
				height = imageRef.Height;


				if (height >= width)
				{
					width = (int)Math.Floor((double)width * ((double)maxSize / (double)height));
					height = maxSize;
				}
				else
				{
					height = (int)Math.Floor((double)height * ((double)maxSize / (double)width));
					width = maxSize;
				}


				CGBitmapContext bitmap;

				if (image.Orientation == UIImageOrientation.Up || image.Orientation == UIImageOrientation.Down)
				{
					bitmap = new CGBitmapContext (IntPtr.Zero, width, height, imageRef.BitsPerComponent, imageRef.BytesPerRow, colorSpaceInfo, alphaInfo);
				}
				else
				{
					bitmap = new CGBitmapContext (IntPtr.Zero, height, width, imageRef.BitsPerComponent, imageRef.BytesPerRow, colorSpaceInfo, alphaInfo);
				}

				switch (image.Orientation)
				{
				case UIImageOrientation.Left:
					bitmap.RotateCTM((float)Math.PI / 2);
					bitmap.TranslateCTM(0, -height);
					break;
				case UIImageOrientation.Right:
					bitmap.RotateCTM(-((float)Math.PI / 2));
					bitmap.TranslateCTM(-width, 0);
					break;
				case UIImageOrientation.Up:
					break;
				case UIImageOrientation.Down:
					bitmap.TranslateCTM(width, height);
					bitmap.RotateCTM(-(float)Math.PI);
					break;
				}

				bitmap.DrawImage(new Rectangle(0, 0, width, height), imageRef);


				res = UIImage.FromImage(bitmap.ToImage());
				bitmap = null;

			}


			return res;
		}

		/// <summary>
		/// Performs a delegate-supplied set of operations on a CGBitmapContext
		/// </summary>
		/// <param name="canvasSize">Size of context in display units (scaling will be applied to go from display units to pixels for Retina support)</param>
		/// <param name="operations">delegate that will perform context operations</param>
		/// <returns>UIImage containing result of context operations</returns>
		public static UIImage PerformContextOperations(SizeF canvasSize, Action<CGBitmapContext> operations)
		{
			return PerformContextOperations(canvasSize.Width, canvasSize.Height, operations);
		}

		/// <summary>
		/// Create a new image with another image drawn to the right of this image
		/// This is a convenience extension method to UIImage.
		/// </summary>
		/// <param name="leftImage">Left image</param>
		/// <param name="rightImage">Right image</param>
		/// <returns>Merged image</returns>
		public static UIImage AddImageOnRight(this UIImage leftImage, UIImage rightImage)
		{
			return MergeImages(leftImage, rightImage);
		}

		/// <summary>
		/// Create a new image with another image drawn on top of this image
		/// This is a convenience extention method to UIImage
		/// </summary>
		/// <param name="bottomImage">Bottom image</param>
		/// <param name="topImage">Top image</param>
		/// <returns>Merged image</returns>
		public static UIImage Superimpose(this UIImage bottomImage, UIImage topImage)
		{
			return SuperimposeImage(topImage, bottomImage);
		}

		/// <summary>
		/// Create a new image containing one or more repetitions of this image.
		/// </summary>
		/// <param name="image">Image to repeat</param>
		/// <param name="count">Number of times to perform the repeat operation (result will contain count + 1 copies)</param>
		/// <returns>Repetition result</returns>
		public static UIImage Repeat(this UIImage image, int count = 1)
		{
			return RepeatImage(image, count);
		}

		public static UIImage Resize(this UIImage sourceImage, float width, float height)
		{
			return ResizeImage (sourceImage, width, height);
		}

		public static UIImage ScaleAndRotateImage(this UIImage image, int maxSize)
		{
			return ScaleImage (image, maxSize);
		}


	}
}

