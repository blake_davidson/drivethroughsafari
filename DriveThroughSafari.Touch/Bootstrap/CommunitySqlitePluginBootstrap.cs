using Cirrious.CrossCore.Plugins;

namespace DriveThroughSafari.Touch.Bootstrap
{
    public class SqlitePluginBootstrap
        : MvxLoaderPluginBootstrapAction<Cirrious.MvvmCross.Community.Plugins.Sqlite.PluginLoader, Cirrious.MvvmCross.Community.Plugins.Sqlite.Touch.Plugin>
    {
    }
}