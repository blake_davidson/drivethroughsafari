using Cirrious.CrossCore.Plugins;

namespace DriveThroughSafari.Touch.Bootstrap
{
    public class PhoneCallPluginBootstrap
        : MvxLoaderPluginBootstrapAction<Cirrious.MvvmCross.Plugins.PhoneCall.PluginLoader, Cirrious.MvvmCross.Plugins.PhoneCall.Touch.Plugin>
    {
    }
}