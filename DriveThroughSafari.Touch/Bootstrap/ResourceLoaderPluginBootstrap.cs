using Cirrious.CrossCore.Plugins;

namespace DriveThroughSafari.Touch.Bootstrap
{
    public class ResourceLoaderPluginBootstrap
        : MvxLoaderPluginBootstrapAction<Cirrious.MvvmCross.Plugins.ResourceLoader.PluginLoader, Cirrious.MvvmCross.Plugins.ResourceLoader.Touch.Plugin>
    {
    }
}