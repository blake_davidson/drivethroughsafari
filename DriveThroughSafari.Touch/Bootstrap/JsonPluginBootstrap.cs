using Cirrious.CrossCore.Plugins;

namespace DriveThroughSafari.Touch.Bootstrap
{
    public class JsonPluginBootstrap
        : MvxPluginBootstrapAction<Cirrious.MvvmCross.Plugins.Json.PluginLoader>
    {
    }
}