using Cirrious.CrossCore.Plugins;

namespace DriveThroughSafari.Touch.Bootstrap
{
    public class NetworkPluginBootstrap
        : MvxLoaderPluginBootstrapAction<Cirrious.MvvmCross.Plugins.Network.PluginLoader, Cirrious.MvvmCross.Plugins.Network.Touch.Plugin>
    {
    }
}