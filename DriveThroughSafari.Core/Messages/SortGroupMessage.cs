﻿using System;
using Cirrious.MvvmCross.Plugins.Messenger;
using DriveThroughSafari.Core.Models;

namespace DriveThroughSafari.Core.Messages.Models
{
    public class SortGroupMessage
        : MvxMessage
    {
        public SortGroupMessage(object sender, SortType sortType, Category category)
            : base(sender)
        {
            SortType = sortType;
            Category = category;
        }

        public Category Category { get; set; }

        public SortType SortType { get; set; }
    }
}

