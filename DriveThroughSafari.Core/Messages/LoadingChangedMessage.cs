﻿using Cirrious.MvvmCross.Plugins.Messenger;

namespace DriveThroughSafari.Core.Messages
{
    public class LoadingChangedMessage : MvxMessage
    {
        public LoadingChangedMessage(object sender)
            : base(sender)
        {
        }
    }
}