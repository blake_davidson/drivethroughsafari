﻿using System;
using Cirrious.MvvmCross.Plugins.Messenger;
using System.Threading.Tasks;
using Cirrious.CrossCore;
using DriveThroughSafari.Core.Models;
using System.Net.Http;
using DriveThroughSafari.Core.Messages;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using Cirrious.CrossCore.Core;
using Cirrious.MvvmCross.Plugins.File;
using System.IO;

namespace DriveThroughSafari.Core
{
    public class ImageDownloadService
        : IImageDownloader
    {
        private Action<Dictionary<int,byte[]>> _complete;

        private Dictionary<int, string> _downloads;

        private Dictionary<int, byte[]> _completedDownloads;

        private IMvxFileStore _filestore;

        public ImageDownloadService()
        {
        }

        #region IImageDownloader implementation

        public void DownloadImages(Dictionary<int, string> downloads, Action<Dictionary<int, byte[]>> downloadComplete)
        {
            _completedDownloads = new Dictionary<int, byte[]>();
            _complete = downloadComplete;
            _downloads = downloads;

            _filestore = Mvx.Resolve<IMvxFileStore>();
            _filestore.EnsureFolderExists("Images");
            MvxAsyncDispatcher.BeginAsync(() => Load());
        }

        #endregion

        public async Task Load()
        {
            var ct = new CancellationToken();
            await AccessTheWebAsync(ct);
        }

        async Task AccessTheWebAsync(CancellationToken ct)
        {

            HttpClient client = new HttpClient();
           
            // ***Create a query that, when executed, returns a collection of tasks.
            IEnumerable<Task> downloadTasksQuery =
                _downloads.Select(download => ProcessURL(download, client, ct));

            // ***Use ToList to execute the query and start the tasks. 
            List<Task> downloadTasks = downloadTasksQuery.ToList();

            // ***Add a loop to process the tasks one at a time until none remain. 
            while (downloadTasks.Count > 0)
            {
                // Identify the first task that completes.
                Task firstFinishedTask = await Task.WhenAny(downloadTasks);

                // ***Remove the selected task from the list so that you don't 
                // process it more than once.
                downloadTasks.Remove(firstFinishedTask);

                // Await the completed task. 
                await firstFinishedTask;

            }

            _complete(_completedDownloads);

        }

        async Task ProcessURL(KeyValuePair<int,string> download, HttpClient client, CancellationToken ct)
        {
            try
            {
                var startingIndex = download.Value.LastIndexOf('/') + 1;
                var fileName = download.Value.Substring(startingIndex);

                byte[] outFile;

                if (_filestore.Exists("Images/" + fileName))
                {
                    _filestore.TryReadBinaryFile("Images/" + fileName, out outFile);
                    _completedDownloads.Add(download.Key, outFile);         
                }
                else
                {
                    // GetAsync returns a Task<HttpResponseMessage>. 
                    HttpResponseMessage response = await client.GetAsync(download.Value, ct);

                    // Retrieve the website contents from the HttpResponseMessage. 
                    byte[] urlContents = await response.Content.ReadAsByteArrayAsync();

                    _completedDownloads.Add(download.Key, urlContents);
                }

            }
            catch (Exception ex)
            {
                var str = ex.Message;
                var str2 = ex.Data;
            }

        }
    }
}

