﻿using System;
using Cirrious.MvvmCross.Community.Plugins.Sqlite;
using System.Linq;
using DriveThroughSafari.Core.Models;
using SQLiteNetExtensions.Extensions;
using System.Collections.Generic;
using System.Net;
using System.IO;
using Cirrious.CrossCore;

namespace DriveThroughSafari.Core
{
    public class SafariDataService : ISafariDataService
    {
        private ISQLiteConnection _connection;
        private string _dbPath;
        private ISQLiteConnectionFactory _factory;

        public SafariDataService(ISQLiteConnectionFactory factory)
        {
            _factory = factory;
            OpenConnection();
            _dbPath = _connection.DatabasePath;
        }

        public void OpenConnection()
        {
            _connection = _factory.Create("dts.sql");
            _connection.CreateTable<Animal>();
        }

        public void CloseConnection()
        {
            _connection.Close();
            _connection.Dispose();
        }

        #region ISafariDataService implementation


        public void InsertAnimals(List<Animal> animals)
        {
            _connection.RunInTransaction(() =>
                {
                    foreach (var animal in animals)
                    {
                        _connection.Insert(animal);
                    }
                });
        }

        public void InsertAnimal(Animal animal)
        {
            _connection.Insert(animal);
        }

        public void UpdateAnimal(Animal animal)
        {
            _connection.Update(animal);
        }

        public void DeleteAnimal(Animal animal)
        {
            _connection.Delete(animal);
        }

        public Animal GetAnimal(int id)
        {
            var animal = GetAnimals().FirstOrDefault(x => x.ID == id);
            return animal;
        }

        public List<Animal> GetAnimals()
        {
            var animals = _connection.GetAllWithChildren<Animal>(null, true);
            var outAnimals = new List<Animal>();
            foreach (var animal in animals)
            {
                var markers = new List<MapMarker>();
                var fences = new List<Fence>();
                var newAnimal = GetWithChildren<Animal>(animal.ID);

                if (animal.Markers != null)
                {
                    foreach (var marker in animal.Markers)
                    {
                        var newMarker = GetWithChildren<MapMarker>(marker.ID);
                        newMarker.Animal = animal;
                        markers.Add(newMarker);
                    }
                }

                if (animal.Fences != null)
                {
                    foreach (var fence in animal.Fences)
                    {
                        var newFence = GetWithChildren<Fence>(fence.ID);
                        newFence.Animals.Add(animal);
                        fences.Add(newFence);
                    }
                }
 
                newAnimal.Markers = markers;
                newAnimal.Fences = fences;
                outAnimals.Add(newAnimal);
            }

            return outAnimals;
        }

        public int AnimalCount { get { return _connection.Table<Animal>().Count(); } }

        public List<T> Get<T>() where T : new()
        {
            return _connection.Table<T>().ToList();
        }

        public T GetWithChildren<T>(object pk) where T : new()
        {
            return _connection.GetWithChildren<T>(pk, true);
        }

        public void GetChildren<T>(T element) where T : new()
        {
            _connection.GetChildren(element, true);
        }

        public string DatabasePath { get { return _dbPath; } }

        public ISQLiteConnection Connection { get { return _connection; } }

        #endregion


    }
}

