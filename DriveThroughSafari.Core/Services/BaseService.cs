﻿using System;
using System.IO;
using System.Net;
using Cirrious.CrossCore;
using Cirrious.CrossCore.Platform;

namespace DriveThroughSafari.Core.Services
{
    public class BaseService<T>
    {
        private readonly Action<Exception> _error;
        private readonly Action<T> _success;
        private readonly string _url;

        public BaseService(Action<T> success, Action<Exception> error, string url)
        {
            _success = success;
            _error = error;
            _url = url;
        }

        public void StartSearch()
        {
            try
            {
                // perform the search
                string uri = _url;
                WebRequest request = WebRequest.Create(new Uri(uri));
                request.BeginGetResponse(ReadCallback, request);
            }
            catch (Exception exception)
            {
                _error(exception);
            }
        }

        private void ReadCallback(IAsyncResult asynchronousResult)
        {
            try
            {
                var request = (HttpWebRequest)asynchronousResult.AsyncState;
                var response = (HttpWebResponse)request.EndGetResponse(asynchronousResult);
                using (var streamReader1 = new StreamReader(response.GetResponseStream()))
                {
                    string resultString = streamReader1.ReadToEnd();
                    HandleResponse(resultString);
                }
            }
            catch (Exception exception)
            {
                _error(exception);
            }
        }

        private void HandleResponse(string data)
        {
            var jsonConvert = Mvx.Resolve<IMvxJsonConverter>();
            var items = jsonConvert.DeserializeObject<T>(data);
            _success(items);
        }
    }
}