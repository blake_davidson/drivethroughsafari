﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cirrious.CrossCore;
using Cirrious.CrossCore.Core;
using Cirrious.CrossCore.Platform;
using Cirrious.MvvmCross.Plugins.Messenger;
using DriveThroughSafari.Core.Interfaces;
using DriveThroughSafari.Core.Messages;
using DriveThroughSafari.Core.Models;
using DriveThroughSafari.Core.ViewModels;
using System.Net;
using System.IO;
using Cirrious.MvvmCross.Plugins.File;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Xml;
using System.Threading.Tasks;

namespace DriveThroughSafari.Core.Services
{
    public enum UpdateType
    {
        All,
        Animals,
        PhotoBooth,
        MediaItems
    }

    public class SafariServices
        : ISafariService
    {
        private const string AnimalsUrl = "http://WildWildernessDriveThroughSafari.com/Mobile/Animals.json";
        private const string GeolocationUrl = "http://WildWildernessDriveThroughSafari.com/Mobile/Geolocations.json";
        private const string FenceGroupsUrl = "http://WildWildernessDriveThroughSafari.com/Mobile/FenceGroups.json";
        private const string FencesUrl = "http://WildWildernessDriveThroughSafari.com/Mobile/Fences.json";
        private const string MarkersUrl = "http://WildWildernessDriveThroughSafari.com/Mobile/Markers.json";
        private const string MediaItemUrl = "http://WildWildernessDriveThroughSafari.com/Mobile/MediaItems.json";
        private const string UpdatesUrl = "http://WildWildernessDriveThroughSafari.com/Mobile/Updates.json";
        private const string DatabaseUrl = "http://WildWildernessDriveThroughSafari.com/Mobile/dts.sql";
        private const string PhotoBoothUrl = "http://WildWildernessDriveThroughSafari.com/Mobile/PhotoBooth.json";

        private const string SettingsFileName = "Settings.xml";
        private const string SettingsResourceFileName = "Xml/Settings.xml";

        // is loading setup
        private bool _isLoading;
        private AppSettings _settings;

        public bool IsLoading
        {
            get { return _isLoading; }
            private set
            {
                _isLoading = value;
                FireLoadingChanged();
            }
        }

        private UpdateType _updateType;

        public List<Fence> Fences { get; private set; }

        public List<MapMarker> MapMarkers { get; private set; }

        private ISafariDataService _connection;

        private List<Updates> _updates;

        public AppSettings Settings
        {
            get { return _settings; }
        }

        private bool _isBackgroundTask = false;

        public void BeginAsyncLoad()
        {
            IsLoading = true;
            _updateType = UpdateType.All;
            _isBackgroundTask = true;
            MvxAsyncDispatcher.BeginAsync(Load);
        }

        public void DoSyncLoad()
        {
            Load();
        }

        public void CheckForUpdates()
        {
            IsLoading = true;
            _updateType = UpdateType.Animals;
            _isBackgroundTask = false;
            MvxAsyncDispatcher.BeginAsync(BeginCheckForUpdate);
        }

        public void BeginAsyncUpdate()
        {
            IsLoading = true;
            MvxAsyncDispatcher.BeginAsync(DownloadDatabase);
        }

        private void FireLoadingChanged()
        {
            FireMessage(new LoadingChangedMessage(this));
        }

        private void FireMessage(MvxMessage message)
        {
            var messenger = Mvx.Resolve<IMvxMessenger>();
            messenger.Publish(message);
        }

        private void Load()
        {
            _connection = Mvx.Resolve<ISafariDataService>();

            LoadSettings();

            if (_connection.GetAnimals().Count == 0)
            {
                DownloadDatabase();
            }
            else
            {	
                BeginCheckForUpdate();
            }

            IsLoading = false;
        }



        void BeginCheckForUpdate()
        {
            var svc = new BaseService<List<Updates>>(UpdatesLoaded, Error, UpdatesUrl);
            svc.StartSearch();
        }

        private void MarkersLoaded(List<MapMarker> mapMarkers)
        {
            foreach (var marker in mapMarkers)
            {
                _connection.Connection.InsertOrReplace(marker);
            }
        }

        private async Task MediaItemsLoaded(List<MediaItem> mediaItems)
        {
            _connection.Connection.DropTable<MediaItem>();
            _connection.Connection.CreateTable<MediaItem>();

            foreach (var mediaItem in mediaItems)
            {
                _connection.Connection.InsertOrReplace(mediaItem);
            }
        }

        private void AnimalsLoaded(List<Animal> animals)
        {

            _connection.Connection.DropTable<Animal>();
            _connection.Connection.CreateTable<Animal>();

            foreach (var animal in animals)
            {
                _connection.Connection.InsertOrReplace(animal);
            }
            IsLoading = false;
        }

        private void FencesLoaded(List<Fence> fences)
        {
            _connection.Connection.DropTable<Fence>();
            _connection.Connection.CreateTable<Fence>();

            foreach (var fence in fences)
            {
                _connection.Connection.InsertOrReplace(fence);
            }
        }

        void FenceGroupsLoaded(List<FenceGroups> fenceGroups)
        {
            _connection.Connection.DropTable<FenceGroups>();
            _connection.Connection.CreateTable<FenceGroups>();

            foreach (var fg in fenceGroups)
            {
                _connection.Connection.InsertOrReplace(fg);
            }
        }

        void GeolocationsLoaded(List<GeoLocation> locations)
        {
            _connection.Connection.DropTable<GeoLocation>();
            _connection.Connection.CreateTable<GeoLocation>();

            foreach (var location in locations)
            {
                _connection.Connection.InsertOrReplace(location);
            }
        }

        void PhotoBoothItemsLoaded(List<PhotoboothItem> items)
        {
            _connection.Connection.DropTable<PhotoboothItem>();
            _connection.Connection.CreateTable<PhotoboothItem>();
            foreach (var item in items)
            {
                _connection.Connection.InsertOrReplace(item);
            }
        }

        async void UpdatesLoaded(List<Updates> updates)
        {
            foreach (var update in updates)
            {
                var localUpdate = _connection.Connection.Table<Updates>().FirstOrDefault(x => x.Name == update.Name);
			
                if (localUpdate != null)
                {
                    if (update.LastUpdate > localUpdate.LastUpdate && !String.IsNullOrEmpty(localUpdate.Name) && localUpdate.LastUpdate != DateTime.MinValue)
                    {	
                        _updates = updates;

                        switch (update.Name)
                        {
                            case "Animals":
                                if (_updateType == UpdateType.All || _updateType == UpdateType.Animals)
                                {
                                    var client = new ServiceClient<List<Animal>>(AnimalsUrl);
                                    List<Animal> animals = await client.GetResult(); 
                                    AnimalsLoaded(animals);
                                }
                                                
                                break;
                            case "Fences":
                                if (_updateType == UpdateType.All || _updateType == UpdateType.Animals)
                                {
                                    var client = new ServiceClient<List<Fence>>(FencesUrl);
                                    List<Fence> fences = await client.GetResult(); 
                                    FencesLoaded(fences);
                                }            
                                break;
                            case "Markers":

                                if (_updateType == UpdateType.All || _updateType == UpdateType.Animals)
                                {
                                    var client = new ServiceClient<List<MapMarker>>(MarkersUrl);
                                    try
                                    {
                                        List<MapMarker> markers = await client.GetResult(); 
                                        MarkersLoaded(markers);
                                    }
                                    catch (Exception ex)
                                    {
                                        Error(ex);
                                    }
                                }
                                break;
                            case "Geolocations":
                                if (_updateType == UpdateType.All || _updateType == UpdateType.Animals)
                                {
                                    var client = new ServiceClient<List<GeoLocation>>(GeolocationUrl);
                                    List<GeoLocation> geolocations = await client.GetResult(); 
                                    GeolocationsLoaded(geolocations);
                                }
                                break;
                            case "FenceGroups":
                                if (_updateType == UpdateType.All || _updateType == UpdateType.Animals)
                                {
                                    var client = new ServiceClient<List<FenceGroups>>(FenceGroupsUrl);
                                    List<FenceGroups> fenceGroups = await client.GetResult(); 
                                    FenceGroupsLoaded(fenceGroups);
                                }
                                break;
                            case "MediaItems":
                                if (_updateType == UpdateType.All || _updateType == UpdateType.MediaItems)
                                {
                                    var client = new ServiceClient<List<MediaItem>>(MediaItemUrl);

                                    try
                                    {
                                        List<MediaItem> mediaItems = await client.GetResult(); 
                                        await MediaItemsLoaded(mediaItems);
                                    }
                                    catch (Exception ex)
                                    {
                                        Error(ex);
                                    }
                                }
                                break;

                            case "PhotoBooth":
                                if (_updateType == UpdateType.All || _updateType == UpdateType.PhotoBooth)
                                {
                                    var client = new ServiceClient<List<PhotoboothItem>>(PhotoBoothUrl);
                                    try
                                    {
                                        List<PhotoboothItem> photoBoothItems = await client.GetResult();
                                        PhotoBoothItemsLoaded(photoBoothItems);
                                    }
                                    catch (Exception e)
                                    {
                                        Error(e);
                                    }
                                   
                                }
                                break;
                        }
                    }
                }
            }

            IsLoading = false;
        }

        private void Error(Exception ex)
        {
            if (!_isBackgroundTask)
            {
                // Mvx.Resolve<IErrorReporter>().ReportError(ex.Message);
                Mvx.Resolve<IErrorReporter>().ReportError("A network error occured. Please verify you have a connection, and try again.");
            }

            IsLoading = false;
        }

        void DownloadDatabase()
        {
            WebRequest request = WebRequest.Create(new Uri(DatabaseUrl));
            request.BeginGetResponse(ReadCallback, request);
//            _connection.Connection.CreateTable<Animal>();
//            var aclient = new ServiceClient<List<Animal>>(AnimalsUrl);
//            List<Animal> animals = await aclient.GetResult(); 
//            AnimalsLoaded(animals);
//
//            _connection.Connection.CreateTable<Fence>();
//            var fclient = new ServiceClient<List<Fence>>(FencesUrl);
//            List<Fence> fences = await fclient.GetResult(); 
//            FencesLoaded(fences);
//
//            _connection.Connection.CreateTable<MapMarker>();
//            var mclient = new ServiceClient<List<MapMarker>>(MarkersUrl);
//            try
//            {
//                List<MapMarker> markers = await mclient.GetResult(); 
//                MarkersLoaded(markers);
//            }
//            catch (Exception ex)
//            {
//                Error(ex);
//            }
//
//            _connection.Connection.CreateTable<GeoLocation>();
//            var gclient = new ServiceClient<List<GeoLocation>>(GeolocationUrl);
//            List<GeoLocation> geolocations = await gclient.GetResult(); 
//            GeolocationsLoaded(geolocations);
//
//            _connection.Connection.CreateTable<FenceGroups>();
//            var fgclient = new ServiceClient<List<FenceGroups>>(FenceGroupsUrl);
//            List<FenceGroups> fenceGroups = await fgclient.GetResult(); 
//            FenceGroupsLoaded(fenceGroups);
//
//            _connection.Connection.CreateTable<MediaItem>();
//            var miclient = new ServiceClient<List<MediaItem>>(MediaItemUrl);
//
//            try
//            {
//                _connection.Connection.CreateTable<MediaItem>();
//                List<MediaItem> mediaItems = await miclient.GetResult(); 
//                await MediaItemsLoaded(mediaItems);
//            }
//            catch (Exception ex)
//            {
//                Error(ex);
//            }
//
//            _connection.Connection.CreateTable<Updates>();
//            var uClient = new ServiceClient<List<Updates>>(UpdatesUrl);
//            List<Updates> updates = await uClient.GetResult();
//            InsertUpdateItems(updates);

        }

        private void ReadCallback(IAsyncResult asynchronousResult)
        {
            try
            {
                var request = (HttpWebRequest)asynchronousResult.AsyncState;

                var response = (HttpWebResponse)request.EndGetResponse(asynchronousResult);
                _connection.CloseConnection();
                var fileStore = Mvx.Resolve<IMvxFileStore>();
                MemoryStream ms = new MemoryStream();
                response.GetResponseStream().CopyTo(ms);
                // If you need it...
                byte[] data = ms.ToArray();
                fileStore.WriteFile(_connection.DatabasePath, data);
                _connection.OpenConnection();

                if (_updates != null)
                {
                    try
                    {
                        _connection.Connection.DropTable<Updates>();
                        _connection.Connection.CreateTable<Updates>();
                        foreach (var update in _updates)
                        {
                            _connection.Connection.Insert(update);
                        }
                    }
                    catch
                    {
                    }
                }

                // UpdateImages();


            }
            catch (Exception exception)
            {
                Error(exception);
            }
        }

        void UpdateImages()
        {
            var animals = _connection.GetAnimals();
            var downloads = new Dictionary<int, string>();
            foreach (var animal in animals)
            {
                downloads.Add(animal.ID, animal.ImageUrl);
            }

            Mvx.Resolve<IImageDownloader>().DownloadImages(downloads, (d) =>
                {
                    foreach (var animal in animals)
                    {
                        if (d.ContainsKey(animal.ID))
                        {
                            animal.ImageData = d[animal.ID];
                            _connection.Connection.Update(animal);
                        }
                    }
                });

//            var mediaItems = _connection.Get<MediaItem>();
//            mediaItems = mediaItems.GroupBy(x => x.Id).Select(x => x.First()).ToList();
//            downloads = new Dictionary<int, string>();
//            foreach (var mediaItem in mediaItems)
//            {
//                downloads.Add(mediaItem.Id, mediaItem.ImageUrl);
//            }
//            Mvx.Resolve<IImageDownloader>().DownloadImages(downloads, (d) =>
//                {
//                    foreach (var mediaItem in mediaItems)
//                    {
//                        if (d.ContainsKey(mediaItem.Id))
//                        {
//                            mediaItem.ImageData = d[mediaItem.Id];
//                            _connection.Connection.Update(mediaItem);
//                        }
//                    }
//                });
        }

        void InsertUpdateItems(List<Updates> updates)
        {
            foreach (var update in updates)
            {
                _connection.Connection.Insert(update);
            }
        }

        private void LoadSettings()
        {
            var fileService = Mvx.Resolve<IMvxFileStore>();
            if (!fileService.TryReadBinaryFile(SettingsFileName, LoadSettingsFrom))
            {
                var resourceLoader = Mvx.Resolve<IMvxResourceLoader>();
                try
                {
                    resourceLoader.GetResourceStream(SettingsResourceFileName,
                        (inputStream) => LoadSettingsFrom(inputStream));
                }
                catch (Exception e)
                {
                    _settings = new AppSettings();
                }

            }
        }

        private bool LoadSettingsFrom(Stream inputStream)
        {
            try
            {
                XDocument loadedData = XDocument.Load(inputStream);
                if (loadedData.Root == null)
                    return false;

                using (XmlReader reader = loadedData.Root.CreateReader())
                {
                    var settings = (AppSettings)new XmlSerializer(typeof(AppSettings)).Deserialize(reader);
                    _settings = settings;
                    return true;
                }
            }
            catch (Exception exception)
            {
                Trace.Error("Problem loading settings {0}", exception.ToString());
                return false;
            }
        }

        public void SaveSettings()
        {
            var fileService = Mvx.Resolve<IMvxFileStore>();

            fileService.WriteFile(SettingsFileName, (stream) =>
                {
                    var serializer = new XmlSerializer(typeof(AppSettings));
                    serializer.Serialize(stream, _settings);
                });
        }
    }
}