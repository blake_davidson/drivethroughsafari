﻿using System;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.Location;
using Cirrious.MvvmCross.Plugins.Messenger;
using DriveThroughSafari.Core.Interfaces;
using DriveThroughSafari.Core.Messages;

namespace DriveThroughSafari.Core.Services
{
    internal class LocationService
        : ILocationService
    {
        private readonly IMvxMessenger _messenger;
        private readonly IMvxLocationWatcher _watcher;

        public LocationService()
        {
            _watcher = Mvx.Resolve<IMvxLocationWatcher>();
            _messenger = Mvx.Resolve<IMvxMessenger>();
        }

        private void OnLocation(MvxGeoLocation location)
        {
            var message = new LocationMessage(this,
                                              location.Coordinates.Latitude,
                                              location.Coordinates.Longitude
                );

            _messenger.Publish(message);
        }

        private void OnError(MvxLocationError error)
        {
            Mvx.Error("Seen location error {0}", error.Code);
        }

        public void Start()
        {
            if (!_watcher.Started)
            {
                var options = new MvxLocationOptions
                    {
                        Accuracy = MvxLocationAccuracy.Fine,
                        MovementThresholdInM = 1,
                        TimeBetweenUpdates = new TimeSpan(0, 0, 0, 0, 250)
                    };

                _watcher.Start(options, OnLocation, OnError);
            }
        }

        public void Stop()
        {
            if(_watcher.Started)
                _watcher.Stop();
        }
    }
}