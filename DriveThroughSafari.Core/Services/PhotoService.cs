﻿using System;

namespace DriveThroughSafari.Core
{
	public class PhotoService
		: IPhotoService
	{
		public PhotoService ()
		{
		}

		private byte[] _image;
		public byte[] Image 
		{
			get { return _image;}
			set { _image = value;}
		}
	}
		
}

