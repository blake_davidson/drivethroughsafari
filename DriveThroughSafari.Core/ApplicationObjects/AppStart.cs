﻿using Cirrious.CrossCore;
using Cirrious.MvvmCross.ViewModels;
using DriveThroughSafari.Core.Interfaces;
using DriveThroughSafari.Core.ViewModels;

namespace DriveThroughSafari.Core.ApplicationObjects
{
    public class AppStart
        : MvxNavigatingObject
          , IMvxAppStart
    {
        private readonly bool _showSplashScreen;

        public AppStart(bool showSplashScreen)
        {
            _showSplashScreen = showSplashScreen;
        }

        public void Start(object hint = null)
        {
            var safariSerivce = Mvx.Resolve<ISafariService>();
            if (_showSplashScreen)
            {
				safariSerivce.BeginAsyncLoad();	
                ShowViewModel<SplashScreenViewModel>();
            }
            else
            {
				safariSerivce.BeginAsyncLoad();
                ShowViewModel<WelcomeViewModel>();
            }
        }
    }
}