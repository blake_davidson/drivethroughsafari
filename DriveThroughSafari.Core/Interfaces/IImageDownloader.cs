﻿using System;
using System.Collections.Generic;

namespace DriveThroughSafari.Core
{
    public interface IImageDownloader
    {
        void DownloadImages(Dictionary<int,string> downloads, Action<Dictionary<int, byte[]>> downloadComplete);
    }
}