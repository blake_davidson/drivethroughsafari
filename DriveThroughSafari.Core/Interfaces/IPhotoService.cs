﻿using System;

namespace DriveThroughSafari.Core
{
	public interface IPhotoService
	{
		Byte[] Image {get; set;}
	}
}

