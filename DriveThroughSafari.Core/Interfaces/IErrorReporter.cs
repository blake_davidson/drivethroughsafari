﻿namespace DriveThroughSafari.Core.Interfaces
{
    public interface IErrorReporter
    {
        void ReportError(string error);
    }
}