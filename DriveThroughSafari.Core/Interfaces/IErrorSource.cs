﻿using System;

namespace DriveThroughSafari.Core.Interfaces
{
    public interface IErrorSource
    {
        event EventHandler<ErrorEventArgs> ErrorReported;
    }
}