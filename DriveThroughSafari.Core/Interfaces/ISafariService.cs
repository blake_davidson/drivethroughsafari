﻿using System.Collections.Generic;
using DriveThroughSafari.Core.Models;
using DriveThroughSafari.Core.ViewModels;
using System;

namespace DriveThroughSafari.Core.Interfaces
{
    public interface ISafariService
    {
        bool IsLoading { get; }

        void BeginAsyncLoad();

        void DoSyncLoad();

        void CheckForUpdates();

        AppSettings Settings { get; }

        void SaveSettings();
    }
}