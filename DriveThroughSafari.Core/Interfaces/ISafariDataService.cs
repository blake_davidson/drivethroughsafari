﻿using System;
using System.Collections.Generic;
using DriveThroughSafari.Core.Models;
using Cirrious.MvvmCross.Community.Plugins.Sqlite;

namespace DriveThroughSafari.Core
{
    public interface ISafariDataService
    {
        Animal GetAnimal(int id);

        String DatabasePath { get; }

        ISQLiteConnection Connection { get; }

        List<T> Get<T>() where T : new();

        T GetWithChildren<T>(object pk) where T : new();

        void GetChildren<T>(T element) where T : new();

        List<Animal> GetAnimals();

        void InsertAnimals(List<Animal> animals);

        void InsertAnimal(Animal animal);

        void UpdateAnimal(Animal animal);

        void DeleteAnimal(Animal animal);

        void OpenConnection();

        void CloseConnection();

        Int32 AnimalCount { get; }

    }
}

