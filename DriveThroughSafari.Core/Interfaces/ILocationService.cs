﻿namespace DriveThroughSafari.Core.Interfaces
{
    public interface ILocationService
    {
        void Start();
        void Stop();
    }
}