﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cirrious.CrossCore;
using DriveThroughSafari.Core.Interfaces;
using DriveThroughSafari.Core.Models;
using DriveThroughSafari.Core.ViewModels;

namespace DriveThroughSafari.Core
{
    public static class Extensions
    {

        public static bool HasAttribute<TAttribute>(this object caller)
            where TAttribute : Attribute
        {
            return caller is TAttribute;
        }

        public static List<Animal> AnimalsInsideFence(this IEnumerable<Fence> fences)
        {
            var animals = Mvx.Resolve<ISafariDataService>().GetAnimals();
            var returnAnimals = new List<Animal>();

            var enumerable = fences as IList<Fence> ?? fences.ToList();
            if (animals != null)
            {
                foreach (var fence in enumerable)
                {
                    foreach (var animal in animals)
                    {
                        if (animal.Fences != null)
                        {
                            foreach (var aFence in animal.Fences)
                            {
                                if (aFence.ID == fence.ID)
                                    returnAnimals.Add(animal);
                            }
                        }
                    }
                }
            }
            return returnAnimals;
        }

        public static IEnumerable<T> GetEnumValues<T>(this object e)
        {
            return Enum.GetValues(typeof(T)).Cast<T>();
        }

        public static bool ScrambledEquals<T>(this IEnumerable<T> list1, IEnumerable<T> list2)
        {
            if (list1 == null || list2 == null)
                return false;

            var cnt = new Dictionary<T, int>();
            foreach (T s in list1)
            {
                if (cnt.ContainsKey(s))
                {
                    cnt[s]++;
                }
                else
                {
                    cnt.Add(s, 1);
                }
            }
            foreach (T s in list2)
            {
                if (cnt.ContainsKey(s))
                {
                    cnt[s]--;
                }
                else
                {
                    return false;
                }
            }
            return cnt.Values.All(c => c == 0);
        }

    }
}