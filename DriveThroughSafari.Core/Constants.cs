﻿namespace DriveThroughSafari.Core
{
    public static class Constants
    {
        public const string GeneralNamespace = "DriveThroughSafari";
        public const string Shared = "Shared";
        public const string RootFolderForResources = "SafariResources/Text";
    }
}