﻿using System;
using Cirrious.MvvmCross.Community.Plugins.Sqlite;

namespace DriveThroughSafari.Core
{
    public class PhotoboothItem
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public String ImageUrl { get; set; }
    }
}

