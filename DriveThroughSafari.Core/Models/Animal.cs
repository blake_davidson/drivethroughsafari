﻿using System;
using DriveThroughSafari.Core.Models;
using System.Collections.Generic;
using Cirrious.MvvmCross.Community.Plugins.Sqlite;
using SQLiteNetExtensions.Attributes;

namespace DriveThroughSafari.Core.Models
{
    public class Animal
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string ImageUrl { get; set; }

        public byte[] ImageData { get; set; }

        public Category Category { get; set; }

        public string Origin { get; set; }

        [OneToMany]
        public List<MapMarker> Markers { get; set; }

        [ManyToMany(typeof(FenceGroups))]
        public List<Fence> Fences { get; set; }

        public string Weight { get; set; }

        public string Length { get; set; }
    }
}