﻿using System;
using Cirrious.MvvmCross.Community.Plugins.Sqlite;

namespace DriveThroughSafari.Core
{
	public class Updates
	{
		[AutoIncrement, PrimaryKey]
		public int ID { get; set; }
		public String Name { get; set; }
		public DateTime LastUpdate {get; set; }
	}
}

