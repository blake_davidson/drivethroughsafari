﻿using System.Collections.Generic;
using Cirrious.MvvmCross.Community.Plugins.Sqlite;
using SQLiteNetExtensions.Attributes;

namespace DriveThroughSafari.Core.Models
{
    public class Fence
    {
        public Fence()
        {
            Points = new List<GeoLocation>();
        }

		[PrimaryKey, AutoIncrement]
		public int ID { get; set; }

		public string Name { get; set; }

		[OneToMany]
        public List<GeoLocation> Points { get; set; }

		[ManyToMany(typeof(FenceGroups))]
		public List<Animal> Animals { get; set;}
        
        public void Add(GeoLocation p)
        {
            Points.Add(p);
        }

        private int Count()
        {
            return Points.Count;
        }

        public bool InsideFence(double x, double y)
        {
            int sides = Count() - 1;
            int j = sides - 1;
            bool pointStatus = false;
            for (int i = 0; i < sides; i++)
            {
                if (Points[i].Lng < y && Points[j].Lng >= y || Points[j].Lng < y && Points[i].Lng >= y)
                {
                    if (Points[i].Lat + (y - Points[i].Lng)/(Points[j].Lng - Points[i].Lng)*(Points[j].Lat - Points[i].Lat) < x)
                    {
                        pointStatus = !pointStatus;
                    }
                }
                j = i;
            }
            return pointStatus;
        }
    } 
}