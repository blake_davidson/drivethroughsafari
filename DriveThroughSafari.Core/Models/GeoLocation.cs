﻿using SQLiteNetExtensions.Attributes;
using Cirrious.MvvmCross.Community.Plugins.Sqlite;

namespace DriveThroughSafari.Core.Models
{
    public class GeoLocation
    {
		[PrimaryKey, AutoIncrement]
		public int ID { get; set; }

		public double Lat { get; set;}
		public double Lng { get; set;}

		public int FenceID { get; set; }

		[ForeignKey(typeof(MapMarker))]
		public int MarkerID { get; set;}

		[ManyToOne("FenceID")]
		public Fence Fence{ get; set;}

        public GeoLocation()
        {
        }

        public GeoLocation(double lat, double lng)
        {
            Lat = lat;
            Lng = lng;
        }
    }
}