﻿using Cirrious.MvvmCross.Community.Plugins.Sqlite;
using SQLiteNetExtensions.Attributes;


namespace DriveThroughSafari.Core.Models
{
    public class MapMarker
    {
		[PrimaryKey, AutoIncrement]
		public int ID { get; set;}

		[OneToOne]
        public GeoLocation Location { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public bool IsAnimal { get; set; }

		public int AnimalID { get; set;}

		[ManyToOne("AnimalID")]
		public Animal Animal {get; set;}

    }
}
