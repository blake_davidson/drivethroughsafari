﻿using Cirrious.MvvmCross.Community.Plugins.Sqlite;

namespace DriveThroughSafari.Core.Models
{
    public class MediaItem
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public string ImageUrl { get; set; }

        public byte[] ImageData { get; set; }

        public bool IsVideo { get; set; }

        public string VideoUrl { get; set; }
    }
}
