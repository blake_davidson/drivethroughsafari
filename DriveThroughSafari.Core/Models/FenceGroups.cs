﻿using System;
using SQLiteNetExtensions.Attributes;
using DriveThroughSafari.Core.Models;

namespace DriveThroughSafari.Core.Models
{
	public class FenceGroups
	{
		[ForeignKey(typeof(Animal))]
		public int AnimalId { get; set; }

		[ForeignKey(typeof(Fence))]
		public int FenceId { get; set; }
	}
}

