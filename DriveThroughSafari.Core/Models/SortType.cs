﻿using System;

namespace DriveThroughSafari.Core.Models
{
    public enum SortType
    {
        None,
        AtoZ,
        ZtoA,
        Category,
        Range
    }
}

