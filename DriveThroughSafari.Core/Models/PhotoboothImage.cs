﻿using System;
using Cirrious.MvvmCross.Community.Plugins.Sqlite;

namespace DriveThroughSafari.Core
{
	public class PhotoboothImage
	{
		[PrimaryKey, AutoIncrement]
		public int ID { get; set; }
		public Byte[] ImageData { get; set;}
		public String ImageUrl {get; set;}
	}
}

