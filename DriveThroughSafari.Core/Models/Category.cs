﻿namespace DriveThroughSafari.Core.Models
{
    public enum Category
    {
        Bird,
        Cat,
        DeerAndAntelope,
        FarmAnimal,
        Hoofstock,
        K9,
        OtherMammal,
        Primates,
        Reptile,
        Rodent,
        All
    }
}