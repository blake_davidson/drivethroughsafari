﻿using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.Messenger;
using DriveThroughSafari.Core.Interfaces;
using Cirrious.MvvmCross.Plugins.File;

namespace DriveThroughSafari.Core.ViewModels
{
    public class HomeViewModel
        : BaseSafariViewModel
    {
        public HomeViewModel()
        {
            Welcome = new WelcomeViewModel();
            Tour = new TourViewModel();
            Animals = new AnimalsViewModel(Mvx.Resolve<IMvxMessenger>());
        }

        public AnimalsViewModel Animals { get; private set; }

        public WelcomeViewModel Welcome { get; private set; }

        public TourViewModel Tour { get; private set; }
    }
}