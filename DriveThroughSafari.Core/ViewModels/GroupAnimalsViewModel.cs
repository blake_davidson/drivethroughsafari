﻿using System;
using System.Windows.Input;
using Cirrious.MvvmCross.ViewModels;
using Cirrious.MvvmCross.Plugins.Messenger;
using DriveThroughSafari.Core.Messages.Models;
using DriveThroughSafari.Core.Models;

namespace DriveThroughSafari.Core.ViewModels
{
    public class GroupAnimalsViewModel
        :BaseSafariViewModel
    {
        private IMvxMessenger _messenger;

        public GroupAnimalsViewModel(IMvxMessenger messenger)
        {
            _messenger = messenger;
        }

        public ICommand AtoZSort
        {
            get { return new MvxCommand(SortAtoZ); }
        }

        public ICommand ZtoASort
        {
            get { return new MvxCommand(SortZtoA); }
        }

        public ICommand CategorySort
        {
            get { return new MvxCommand(SortByCategory); }
        }

        public ICommand FilterByCategory
        {
            get
            {
                return new MvxCommand<Category>(category =>
                    DoFilterByCategory(category));
            }
        }

        void DoFilterByCategory(Category category)
        {
            var message = new SortGroupMessage(this, SortType.None, category);
            _messenger.Publish(message);
        }

        public ICommand RangeSort
        {
            get { return new MvxCommand(SortByRange); }
        }

        void SortByCategory()
        {
            var message = new SortGroupMessage(this, SortType.Category, Category.All);
            _messenger.Publish(message);
        }

        private void SortAtoZ()
        {
            var message = new SortGroupMessage(this, SortType.AtoZ, Category.All);
            _messenger.Publish(message);
        }

        void SortZtoA()
        {
            var message = new SortGroupMessage(this, SortType.ZtoA, Category.All);
            _messenger.Publish(message);
        }

        void SortByRange()
        {
            var message = new SortGroupMessage(this, SortType.Range, Category.All);
            _messenger.Publish(message);
        }
    }
}