﻿using DriveThroughSafari.Core.ViewModels;

namespace DriveThroughSafari.Core
{
    public class PreviewViewModel
		:BaseSafariViewModel
    {
        public PreviewViewModel(IPhotoService photoService)
        {
            Image = photoService.Image;
        }

        private byte[] _image;

        public byte[] Image
        {
            get { return _image; }
            set
            {
                _image = value;
                RaisePropertyChanged(() => Image);
            }
        }


			
    }
}

