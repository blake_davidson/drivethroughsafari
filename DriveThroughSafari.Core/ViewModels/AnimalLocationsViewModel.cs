using System;
using System.Collections.Generic;
using System.Linq;
using DriveThroughSafari.Core.Models;

namespace DriveThroughSafari.Core.ViewModels
{
    public partial class AnimalLocationsViewModel : BaseSafariViewModel
    {

        public AnimalLocationsViewModel() 
        {
            _markers = new List<MapMarker>();
        }

		public void Init(Int32 id)
        {
			Animal = DataService.GetAnimal (id);

        }

		private Animal _animal;
		protected Animal Animal
        {
            get { return _animal; }
            set { _animal = value; RaisePropertyChanged(() => Animal); }
        }

        private List<MapMarker> _markers;

        public List<MapMarker> Markers
        {
            get { return _markers; }
            set
            {
                _markers = value;
                RaisePropertyChanged(() => Markers);
            }
        }
    }
}