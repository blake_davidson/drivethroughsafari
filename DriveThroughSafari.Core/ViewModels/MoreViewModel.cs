﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Cirrious.MvvmCross.ViewModels;

namespace DriveThroughSafari.Core.ViewModels
{
    public class MoreViewModel
        :BaseSafariViewModel
    {
        public ICommand ShowPhotoBooth
        {
            get { return new MvxCommand(() =>  ShowViewModel<PhotoBoothViewModel>()); }
        }

        public ICommand ShowMediaAlbum
        {
            get { return new MvxCommand(() => ShowViewModel<MediaAlbumViewModel>()); }
        }
    }
}
