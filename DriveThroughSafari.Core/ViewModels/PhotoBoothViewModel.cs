﻿using System.Collections.Generic;
using System.Windows.Input;
using Cirrious.MvvmCross.ViewModels;
using DriveThroughSafari.Core.Models;

namespace DriveThroughSafari.Core.ViewModels
{
    public class PhotoBoothViewModel
        : BaseSafariViewModel
    {
        private List<PhotoboothItem> _items;
        private IPhotoService _photoService;

        public PhotoBoothViewModel(IPhotoService photoservice)
        {
            _photoService = photoservice;
            PhotoBoothItems = DataService.Get<PhotoboothItem>();
        }

        public List<PhotoboothItem> PhotoBoothItems
        {
            get { return _items; }
            private set
            {
                _items = value;
                RaisePropertyChanged(() => PhotoBoothItems);
            }
        }

        private byte[] _image;

        public byte[] Image
        {
            get{ return _image; }
            set
            {
                _image = value;
                RaisePropertyChanged(() => Image);
            }
        }

        public ICommand ShowPreview
        {
            get { return new MvxCommand(DoShowPreview); }
        }

        private void DoShowPreview()
        {
            _photoService.Image = Image;
            ShowViewModel<PreviewViewModel>();
        }
    }
}