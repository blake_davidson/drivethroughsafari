﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.Messenger;
using Cirrious.MvvmCross.ViewModels;
using DriveThroughSafari.Core.Interfaces;
using DriveThroughSafari.Core.Messages;
using DriveThroughSafari.Core.Models;

namespace DriveThroughSafari.Core.ViewModels
{
    public class TourViewModel
        : BaseSafariViewModel
    {
        private readonly MvxSubscriptionToken _token;
        private List<Animal> _animalsAtLocation;
        private readonly ILocationService _service;
        private List<Fence> _fences;

        public TourViewModel()
        {
            _token = Mvx.Resolve<IMvxMessenger>().Subscribe<LocationMessage>(OnLocationMessage);
            _service = Mvx.Resolve<ILocationService>();
            _location = new GeoLocation();
            _fences = GetFences();
            _hideAnimalsView = true;
        }

        public ICommand StartService
        {
            get { return new MvxCommand(() => _service.Start()); }
        }

        public ICommand StopService
        {
            get { return new MvxCommand(() => _service.Stop()); }
        }

        public List<Animal> AnimalsAtLocation
        {
            get { return _animalsAtLocation; }
            set
            {
                _animalsAtLocation = value;
                RaisePropertyChanged(() => AnimalsAtLocation);
            }
        }

        public List<Fence> GetFences()
        {

            var fences = DataService.Get<Fence>().ToList();

            foreach (var fence in fences)
            {
                DataService.GetChildren(fence);
            }

            return fences;

        }

        public List<MapMarker> Markers
        {
            get
            { 
                var markers = DataService.Get<MapMarker>(); 
                foreach (var marker in markers)
                {
                    DataService.GetChildren(marker);
                }

                return markers;
            }
        }

        private GeoLocation _location;

        public GeoLocation Location
        {
            get { return _location; }
            set
            {
                _location = value;
                RaisePropertyChanged(() => Location);
                CheckLocation();
            }
        }

        private MvxCommand<Animal> _itemSelectedCommand;

        public ICommand ItemSelectedCommand
        {
            get
            {
                _itemSelectedCommand = _itemSelectedCommand ?? new MvxCommand<Animal>(DoSelectItem);
                return _itemSelectedCommand;
            }
        }

        private void DoSelectItem(Animal item)
        {
            ShowViewModel<AnimalViewModel>(item);
        }

        private bool _hideAnimalsView;

        public bool HideAnimalsView
        {
            get { return _hideAnimalsView; }
            set
            {
                _hideAnimalsView = value;
                RaisePropertyChanged(() => HideAnimalsView);
            }
        }

        private bool _hideWarning;

        public bool HideWarning
        {
            get { return _hideWarning; }
            set
            {
                _hideWarning = value;
                RaisePropertyChanged(() => HideWarning);
            }
        }


        private void OnLocationMessage(LocationMessage locationMessage)
        {
            var loc = new GeoLocation(locationMessage.Lat, locationMessage.Lng);
            Location = loc;
        }

        private void CheckLocation()
        {
            var fences = InsideFences(Location.Lat, Location.Lng);
            
            if (fences != null && fences.Count > 0)
            {
                var animals = fences.AnimalsInsideFence();
                if (!animals.ScrambledEquals(AnimalsAtLocation))
                    AnimalsAtLocation = animals;

                HideWarning = true;
                HideAnimalsView = false;
            }
            else
            {
                HideWarning = false;
                HideAnimalsView = true;
            }
        }

        public List<Fence> InsideFences(double lat, double lng)
        {
            return _fences != null ? _fences.Where(fence => fence.InsideFence(lat, lng)).ToList() : null;
        }

    }
}