﻿using System.Collections.Generic;
using DriveThroughSafari.Core.Models;
using System.Threading.Tasks;
using System.Net.Http;
using System.Threading;
using System.Linq;
using System;
using Cirrious.MvvmCross.ViewModels;
using System.Windows.Input;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.Messenger;
using Cirrious.MvvmCross.Plugins.DownloadCache;
using DriveThroughSafari.Core.Messages;
using Cirrious.MvvmCross.Plugins.File;
using Cirrious.CrossCore.Platform;

namespace DriveThroughSafari.Core.ViewModels
{
    public class MediaAlbumViewModel
        :BaseSafariViewModel
    {
        private readonly MvxSubscriptionToken _token;

        public MediaAlbumViewModel(IMvxMessenger messenger)
        {
            _token = messenger.Subscribe<LoadingChangedMessage>(OnLoaded);
            MediaItems = DataService.Get<MediaItem>();
        }

        private List<MediaItem> _mediaItems;

        public List<MediaItem> MediaItems
        {
            get { return _mediaItems; }
            set
            {
                _mediaItems = value;
                RaisePropertyChanged(() => MediaItems);
            }
        }


        public ICommand ReloadCommand
        {
            get { return new MvxCommand(Service.CheckForUpdates); }
        }

        void OnLoaded(LoadingChangedMessage obj)
        {
            IsBusy = Service.IsLoading;

            if (!IsBusy)
            {
                MediaItems = DataService.Get<MediaItem>();
            }
        }
    }
}
