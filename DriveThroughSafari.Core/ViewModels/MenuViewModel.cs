﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Cirrious.MvvmCross.ViewModels;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.Email;
using Cirrious.MvvmCross.Plugins.PhoneCall;
using Cirrious.MvvmCross.Plugins.WebBrowser;

namespace DriveThroughSafari.Core.ViewModels
{
    public class MenuViewModel : BaseSafariViewModel
    {
        public ICommand ShowAnimals
        {
            get { return new MvxCommand(ClearStackAndShow<AnimalsViewModel>); }
        }

        public ICommand ShowTour
        {
            get { return new MvxCommand(ClearStackAndShow<TourViewModel>); }
        }

        public ICommand ShowPhotoBooth
        {
            get { return new MvxCommand(ClearStackAndShow<PhotoBoothViewModel>); }
        }

        public ICommand ShowPhotoGallery
        {
            get { return new MvxCommand(ClearStackAndShow<MediaAlbumViewModel>); }
        }

        private void ClearStackAndShow<TViewModel>()
            where TViewModel : BaseViewModel
        {
            var presentationBundle = new MvxBundle(new Dictionary<string, string> { { PresentationBundleFlagKeys.ClearStack, "" } });

            ShowViewModel<TViewModel>(presentationBundle: presentationBundle);
        }

		public ICommand SendEmail
		{
			get {return new MvxCommand (DoSendEmail);}
		}

		public ICommand MakePhoneCall
		{
			get {return new MvxCommand (DoMakeCall);}
		}

		public ICommand GoToFacebook
		{
			get {return new MvxCommand (DoGoToFacebook);}
		}

		public ICommand GoToTripAdvisor
		{
			get { return new MvxCommand (DoGoToTripAdvisor);}
		}

		void DoGoToFacebook ()
		{
			var task = Mvx.Resolve<IMvxWebBrowserTask> ();
			task.ShowWebPage ("https://www.facebook.com/pages/Wild-Wilderness-Drive-Through-Safari/347874149318");
		}

		void DoGoToTripAdvisor ()
		{
			var task = Mvx.Resolve<IMvxWebBrowserTask> ();
			task.ShowWebPage ("http://www.tripadvisor.com/Attraction_Review-g31612-d1379999-Reviews-Wild_Wilderness_Drive_through_Safari-Gentry_Arkansas.html");
		}

		private void DoSendEmail()
		{
			var task = Mvx.Resolve<IMvxComposeEmailTask>();
			task.ComposeEmail ("info@wildwildernessdrivethroughsafari.com", null, "Safari App Question", "", false);
		}

		private void DoMakeCall()
		{
			var task = Mvx.Resolve<IMvxPhoneCallTask> ();
			task.MakePhoneCall ("WWDTS","4797368383");
		}
        
    }
}
