﻿namespace DriveThroughSafari.Core.ViewModels
{
    public class SplashScreenViewModel
        : BaseSafariViewModel
    {
        private bool _splashScreenComplete;

        public SplashScreenViewModel()
        {
            SplashScreenComplete = false;
            PropertyChanged += (sender, args) =>
                {
                    if (args.PropertyName == "IsSearching")
                    {
                        MoveForwardsIfPossible();
                    }
                };
        }

        public bool SplashScreenComplete
        {
            get { return _splashScreenComplete; }
            set
            {
                _splashScreenComplete = value;
                MoveForwardsIfPossible();
            }
        }

        private void MoveForwardsIfPossible()
        {
            if (!SplashScreenComplete)
                return;

            ShowViewModel<HomeViewModel>(true);
        }
    }
}