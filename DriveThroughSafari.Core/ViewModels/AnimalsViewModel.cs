﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using Cirrious.MvvmCross.ViewModels;
using DriveThroughSafari.Core.Models;
using Cirrious.MvvmCross.Plugins.Messenger;
using DriveThroughSafari.Core.Messages.Models;
using DriveThroughSafari.Core.Messages;
using System.Linq;
using System.Linq.Expressions;

namespace DriveThroughSafari.Core.ViewModels
{
    public class AnimalsViewModel
        : BaseSafariViewModel
    {

        private readonly MvxSubscriptionToken _token;
        private readonly MvxSubscriptionToken _token2;
        private SortType _sortType;
        private Category _category;

        public AnimalsViewModel(IMvxMessenger messenger)
        {
            _token = messenger.Subscribe<SortGroupMessage>(OnSortGroupMessage);
            _token2 = messenger.Subscribe<LoadingChangedMessage>(OnLoaded);
            GroupedList = new Dictionary<string, List<Animal>>();
            _sortType = SortType.AtoZ;
            _category = Category.All;
            AtoZ();
        }

        private Dictionary<string, List<Animal>> _groupedList;

        public Dictionary<string, List<Animal>> GroupedList
        {
            get { return _groupedList; }
            protected set
            {
                _groupedList = value;
                RaisePropertyChanged(() => GroupedList);
            }
        }

        private MvxCommand<Animal> _itemSelectedCommand;

        public ICommand ItemSelectedCommand
        {
            get
            {
                _itemSelectedCommand = _itemSelectedCommand ?? new MvxCommand<Animal>(DoSelectItem);
                return _itemSelectedCommand;
            }
        }

        public ICommand ReloadCommand
        {
            get { return new MvxCommand(Service.CheckForUpdates); }
        }

        private void DoSelectItem(Animal item)
        {
            ShowViewModel<AnimalViewModel>(new {id = item.ID});
        }

        void OnLoaded(LoadingChangedMessage obj)
        {
            IsBusy = Service.IsLoading;

            if (!IsBusy)
            {
                SortAnimals();
            }
        }

        void OnSortGroupMessage(SortGroupMessage message)
        {
            _sortType = message.SortType;
            _category = message.Category;

            SortAnimals();
        }

        private void SortAnimals()
        {
            switch (_sortType)
            {
                case SortType.AtoZ:
                    AtoZ();
                    break;

                case SortType.ZtoA:
                    ZtoA();
                    break;

                case SortType.Category:
                    CategorySort();
                    break;

                case SortType.Range:
                    RangeSort();
                    break;

                case SortType.None:
                    FilterByCategory(_category);
                    break;
            }
        }

        private void AtoZ()
        {
            GroupedList.Clear();
            var list = new Dictionary<string, List<Animal>>();
            var animals = DataService.GetAnimals().OrderBy(x => x.Name);
            foreach (var animal in animals)
            {
                if (list.ContainsKey(animal.Name.Substring(0, 1).ToUpper()))
                {
                    list[animal.Name.Substring(0, 1).ToUpper()].Add(animal);
                }
                else
                {
                    list.Add(animal.Name.Substring(0, 1).ToUpper(), new List<Animal> { animal });
                }
            }

            GroupedList = list;
        }

        private void ZtoA()
        {
            GroupedList.Clear();
            var list = new Dictionary<string, List<Animal>>();
            var animals = DataService.GetAnimals().OrderByDescending(x => x.Name);
            foreach (var animal in animals)
            {
                if (list.ContainsKey(animal.Name.Substring(0, 1).ToUpper()))
                {
                    list[animal.Name.Substring(0, 1).ToUpper()].Add(animal);
                }
                else
                {
                    list.Add(animal.Name.Substring(0, 1).ToUpper(), new List<Animal> { animal });
                }
            }

            GroupedList = list;
        }


        private void CategorySort()
        {
            GroupedList.Clear();
            var list = new Dictionary<string, List<Animal>>();
            var animals = DataService.GetAnimals().OrderBy(x => x.Category);
            foreach (var animal in animals)
            {
                if (list.ContainsKey(animal.Category.ToString()))
                {
                    list[animal.Category.ToString()].Add(animal);
                }
                else
                {
                    list.Add(animal.Category.ToString(), new List<Animal> { animal });
                }
            }

            GroupedList = list;
        }

        void RangeSort()
        {
            GroupedList.Clear();
            var list = new Dictionary<string, List<Animal>>();
            var animals = DataService.GetAnimals().OrderBy(x => x.Category);
            foreach (var animal in animals)
            {
                if (list.ContainsKey(animal.Origin))
                {
                    list[animal.Origin].Add(animal);
                }
                else
                {
                    list.Add(animal.Origin, new List<Animal> { animal });
                }
            }

            GroupedList = list;
        }

        void FilterByCategory(Category category)
        {
            GroupedList.Clear();
            var list = new Dictionary<string, List<Animal>>();
            var animals = DataService.GetAnimals().OrderBy(x => x.Category);
            list.Add(category.ToString(), new List<Animal>());
            foreach (var animal in animals)
            {
                if (animal.Category == category)
                {
                    list[category.ToString()].Add(animal);
                }
            }

            GroupedList = list;
        }
    }
}