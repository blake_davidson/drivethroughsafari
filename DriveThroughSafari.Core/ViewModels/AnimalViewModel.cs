﻿using System.Linq;
using System.Windows.Input;
using Cirrious.MvvmCross.ViewModels;
using System;
using DriveThroughSafari.Core.Models;

namespace DriveThroughSafari.Core.ViewModels
{
    public class AnimalViewModel
        : BaseSafariViewModel
    {
        private Animal _animal;

        public Animal Animal
        {
            get { return _animal; }
            set
            {
                _animal = value;
                RaisePropertyChanged(() => Animal);
            }
        }


        private bool _hideMarkerButton;

        public bool HideMarkerButton
        {
            get { return _hideMarkerButton; }
            set
            {
                _hideMarkerButton = value;
                RaisePropertyChanged(() => HideMarkerButton);
            }
        }

        public ICommand ShowAnimalLocations
        {
            get { return new MvxCommand(() => ShowViewModel<AnimalLocationsViewModel>(Animal)); }
        }

        public void Init(int id)
        {
            HideMarkerButton = true;

            Animal = DataService.GetAnimal(id);
    
            if (Animal != null && (Animal.Markers != null && Animal.Markers.Count > 0))
                HideMarkerButton = false;
        }
    }
}