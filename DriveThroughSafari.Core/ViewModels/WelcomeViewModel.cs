﻿using System.Windows.Input;
using Cirrious.MvvmCross.ViewModels;

namespace DriveThroughSafari.Core.ViewModels
{
    public class WelcomeViewModel
        : BaseSafariViewModel
    {
        public ICommand AnimalsCommand
        {
            get { return new MvxCommand(() => ShowViewModel<AnimalsViewModel>()); }
        }

        public ICommand ShowTourCommand
        {
            get { return new MvxCommand(() => ShowViewModel<TourViewModel>()); }
        }

        public ICommand ShowPhotosCommand
        {
            get { return new MvxCommand(() => ShowViewModel<PhotoBoothViewModel>()); }
        }

        //public ICommand ShowAboutCommand
        //{
        //    get { return new MvxCommand(() => ShowViewModel<AboutViewModel>()); }
        //}

        public ICommand DismissTutorial
        {
            get { return new MvxCommand(SetTutorialSeen); }
        }

        private void SetTutorialSeen()
        {
            Settings.HasSeenTutorial = true;
            UpdateSettings();
        }
    }
}