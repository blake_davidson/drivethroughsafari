﻿using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.Messenger;
using DriveThroughSafari.Core.Interfaces;
using DriveThroughSafari.Core.Messages;
using System.Windows.Input;
using Cirrious.MvvmCross.ViewModels;

namespace DriveThroughSafari.Core.ViewModels
{
    public class BaseSafariViewModel
        : BaseViewModel
    {

        public BaseSafariViewModel()
        {
        }

        public ISafariService Service
        {
            get { return Mvx.Resolve<ISafariService>(); }
        }

        public AppSettings Settings
        {
            get
            {
                return Service.Settings;
            }
        }

        public void UpdateSettings()
        {
            Service.SaveSettings();
        }

        public ISafariDataService DataService
        {
            get { return Mvx.Resolve<ISafariDataService>(); }
        }

        public ICommand Home()
        {
            return new MvxCommand(() => ShowViewModel<WelcomeViewModel>());
        }

        public ICommand GoHome
        {
            get { return new MvxCommand(() => ShowViewModel<WelcomeViewModel>()); }
        }

        public ICommand GoBack
        {
            get { return new MvxCommand(() => Close(this)); }
        }

        private bool _isBusy;

        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                RaisePropertyChanged(() => IsBusy);
            }
        }

        private string m_BusyMessage = string.Empty;

        public string BusyMessage
        {
            get { return m_BusyMessage; }
            set
            {
                m_BusyMessage = value;
                RaisePropertyChanged(() => BusyMessage);
            }
        }
    }
}