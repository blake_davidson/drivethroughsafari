﻿using System;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Localization;
using Cirrious.MvvmCross.Plugins.Email;
using Cirrious.MvvmCross.Plugins.Messenger;
using Cirrious.MvvmCross.ViewModels;
using DriveThroughSafari.Core.Interfaces;

namespace DriveThroughSafari.Core.ViewModels
{

    public static class PresentationBundleFlagKeys
    {
        public const string ClearStack = "__CLEAR_STACK__";
    }

    public class BaseViewModel
        : MvxViewModel
    {

        protected void ReportError(string text)
        {
            Mvx.Resolve<IErrorReporter>().ReportError(text);
        }
			
        protected void ComposeEmail(string to, string subject, string body)
        {
            var task = Mvx.Resolve<IMvxComposeEmailTask>();
            task.ComposeEmail(to, null, subject, body, false);
        }
			
    }
}