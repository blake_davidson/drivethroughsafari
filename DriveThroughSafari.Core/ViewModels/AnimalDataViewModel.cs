﻿using System.Collections.Generic;
using DriveThroughSafari.Core.Models;

namespace DriveThroughSafari.Core.ViewModels
{
    public class AnimalDataViewModel
        : BaseSafariViewModel
    {
		public Animal Animal { get; set; }

        public void ShowAnimal()
        {
            ShowViewModel<AnimalViewModel>(new {animal = this});
        }

    }
}