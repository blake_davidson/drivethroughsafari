﻿using Cirrious.CrossCore;
using Cirrious.MvvmCross.ViewModels;
using DriveThroughSafari.Core.ApplicationObjects;
using DriveThroughSafari.Core.Interfaces;
using DriveThroughSafari.Core.Services;
using PluginLoader = Cirrious.MvvmCross.Plugins.Json.PluginLoader;
using Cirrious.CrossCore.IoC;
using Cirrious.MvvmCross.Community.Plugins.Sqlite;

namespace DriveThroughSafari.Core
{
    public abstract class BaseSafariApp
        : MvxApplication
    {
        protected BaseSafariApp()
        {
            InitializePlugins();
            InitializeErrorSystem();
            InitializeServices();
        }

        private void InitializePlugins()
        {
            PluginLoader.Instance.EnsureLoaded();
          
            Cirrious.MvvmCross.Plugins.Location.PluginLoader.Instance.EnsureLoaded();
            Cirrious.MvvmCross.Plugins.Messenger.PluginLoader.Instance.EnsureLoaded();

            // these don't really need to be loaded on startup, but it's convenient for now
            Cirrious.MvvmCross.Plugins.Email.PluginLoader.Instance.EnsureLoaded();
        }

        private void InitializeErrorSystem()
        {
            var errorHub = new ErrorApplicationObject();
            Mvx.RegisterSingleton<IErrorReporter>(errorHub);
            Mvx.RegisterSingleton<IErrorSource>(errorHub);
        }

        private void InitializeServices()
        {
            CreatableTypes()
				.EndingWith("Service")
				.AsInterfaces()
				.RegisterAsLazySingleton();

            var safari = new SafariServices();
            var location = new LocationService();
            var photoService = new PhotoService();
            var imageDownloadService = new ImageDownloadService();

            var dataService = new SafariDataService(Mvx.Resolve<ISQLiteConnectionFactory>());
            Mvx.RegisterSingleton<ILocationService>(location);
            Mvx.RegisterSingleton<ISafariService>(safari);
            Mvx.RegisterSingleton<IPhotoService>(photoService);
            Mvx.RegisterSingleton<ISafariDataService>(dataService);
            Mvx.RegisterSingleton<IImageDownloader>(imageDownloadService);

        }

        protected abstract void InitializeStartNavigation();
    }

    public class DriveThroughSafariApp
        : BaseSafariApp
    {
        public DriveThroughSafariApp()
        {
            InitializeStartNavigation();
        }

        protected override sealed void InitializeStartNavigation()
        {
            var startApplicationObject = new AppStart(true);
            Mvx.RegisterSingleton<IMvxAppStart>(startApplicationObject);
        }
    }

    public class NoSplashScreenSafariApp
        : BaseSafariApp
    {
        public NoSplashScreenSafariApp()
        {
            InitializeStartNavigation();
        }

        protected override sealed void InitializeStartNavigation()
        {
            var startApplicationObject = new AppStart(false);
            Mvx.RegisterSingleton<IMvxAppStart>(startApplicationObject);
        }
    }
}